<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Eigerlab Technologies | Blog</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{url('public/img/eigerlab-32x32.png')}}" rel="icon">
  <link href="{{URL::to('public/flex-start-blog/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{URL::to('public/flex-start-blog/assets/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: FlexStart - v1.0.0
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

  <link href="{{URL::to('public/css/navigationbarcss/home-page.css')}}" rel="stylesheet" type="text/css">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="{{URL::to('/')}}" class="logo d-flex align-items-center">
        <img src="{{URL::to('public/flex-start-blog/assets/img/logo.png')}}" alt="">
        <span>Eigerlab Technologies</span>
      </a>
  <?php //dd($blog_data);?>
      <nav id="navbar" class="navbar">
        <ul></ul>
          {{-- </li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="getstarted scrollto" href="#about">Get Started</a></li> --}}
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        {{-- <ol>
          <li><a href="{{URL::to('/')}}">Home</a></li>
          <li>Blog</li>
        </ol> --}}
        <h2>Blog</h2>

      </div>
    </section>
    <!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">
  
          <div class="col-lg-8 entries all-blogs">
         
            <article class="entry ">
              <div class="entry-img">
                <img src="{{$blog_data['blog_image']}}" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                {{$blog_data['blog_title']}}
              </h2>

              <div class="entry-meta">
                <ul>
                  <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-single.html">{{$blog_data['blogger_name']}}</a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-single.html"><time datetime="2020-01-01">{{$blog_data['created_at']}}</time></a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-bookmark-fill"></i> <a href="blog-single.html">{{$blog_data['category_name']}}</a></li>
                </ul>
              </div>

              <div class="entry-content">
                <p>
                  {{$blog_data['blog_description']}}
                </p>
              </div>

            </article><!-- End blog entry -->

           
          </div>

          <div class="col-lg-4">

            <div class="sidebar">

              <h3 class="sidebar-title">Search</h3>
              <div class="sidebar-item search-form">
                <form action="">
                  <input type="text">
                  <button type="submit"><i class="bi bi-search"></i></button>
                </form>
              </div><!-- End sidebar search formn-->

              <h3 class="sidebar-title">Categories</h3>
              <div class="sidebar-item categories blog-category">
                <ul>
                  <?php for($i=0;$i< count($category);$i++){?>
                  <li><a href="#">{{$category[$i]['category_name']}}<span></span></a></li>
                  <?php }?>
                </ul>
              </div><!-- End sidebar categories-->

              <h3 class="sidebar-title">Our most popular blogs</h3>
              <div class="sidebar-item recent-posts">
                <?php for($k=1;$k < count($randomBlog);$k++){?>
                <div class="post-item clearfix">
                  <img src="{{$randomBlog[$k]['blog_image']}}" alt="">
                  <h4><a href="#">{{$randomBlog[$k]['blog_title']}}</a></h4>
                  <time datetime="2020-01-01">{{$randomBlog[$k]['created_at']}}</time>
                </div>
               <?php }?>

              </div><!-- End sidebar recent posts-->

            </div><!-- End sidebar -->

          </div><!-- End blog sidebar -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->


  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/aos/aos.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/purecounter/purecounter.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{URL::to('public/flex-start-blog/assets/js/main.js')}}"></script>



  <script src="{{URL::to('public/js/jquery-2.1.1.js')}}"></script>
 
</body>

</html>