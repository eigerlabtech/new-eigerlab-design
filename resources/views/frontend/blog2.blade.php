@extends('frontend.master')
@section('content')
<!-- <link rel="stylesheet" id="style-main" href="{{url('public/css/styles-main.css?v=3')}}">
  <link rel="stylesheet" id="swiper-main" href="{{url('public/vendors/swiper/swiper.min.css')}}">
  <link rel="stylesheet" id="lity-main" href="{{url('public/vendors/lity/lity.min.css')}}">
  <link rel="stylesheet" id="mediaelementplayer" href="{{url('public/vendors/mediaelement/mediaelementplayer.min.css')}}">
  <link rel="stylesheet" id="range" href="{{URL::to('public/vendors/range/jquery.range.css')}}">
  <link rel="stylesheet" id="lightgallery" href="{{url('public/vendors/lightgallery/lightgallery.min.css')}}">
  <link rel="stylesheet" id="style-link" href="{{url('public/css/styles-9.css?v=35')}}"> -->
<section class="aheto-banner aheto-banner--height-450 aheto-banner--height-mob-200" style="background-image: url('public/img/SAAS/feature_bg.png'); background-position: center">
    <div class="container d-flex h-100 justify-content-center align-items-center">
      <div class="row padding-lg-60t padding-sm-0t">
        <div class="aheto-heading t-center aheto-heading--t-white">
          <h1 class="aheto-heading__title          t-light ">Blog</h1>
        </div>
      </div>
    </div>
  </section>
  <div class="aht-page container margin-lg-100b margin-lg-120t margin-md-80t margin-md-80b margin-sm-50t margin-sm-50b
    aht-page--no-sb
    
    
    
    ">
    <div class="aht-page__inner">
      <div class="aht-page__content">
        <div class="aht-page__content-inner">
          <div class="row">
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item ">
              <article class="post format-image border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Notarization</a>
                    <a href="#" rel="category tag">Tips</a>
                  </div>
                  <div class="image-wrapper border-radius-5t">
                    <img src="{{URL::to('public/img/inner-pages/blog/Image-one.jpg')}}" alt="blog img" class="js-bg border-radius-5t">
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">How We`re Transforming Yet Untouched Market</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item ">
              <article class="post format-image border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Tips</a>
                  </div>
                  <div class="image-wrapper border-radius-5t">
                    <img src="{{('public/img/inner-pages/blog/Image-two.jpg')}}" alt="blog img" class="js-bg border-radius-5t">
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">Online Notarization Bill Passes in Tennessee</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item ">
              <article class="post format-quote border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Notarization</a>
                  </div>
                  <div class="image-wrapper ">
                  </div>
                </div>
                <div class="content-quote-wrapper">
                  <p>Follow your passion, be prepared to work hard and sacrifice, don’t let anyone limit your dreams.</p>
                  <cite>- Michale vouge</cite>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item ">
              <article class="post format-image border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Notarization</a>
                    <a href="#" rel="category tag">Tips</a>
                  </div>
                  <div class="image-wrapper border-radius-5t">
                    <img src="../img/inner-pages/blog/image-9.jpg" alt="blog img" class="js-bg border-radius-5t">
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">Online Notarization Bill Passes in Tennessee</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item ">
              <article class="post format-standard border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Tips</a>
                  </div>
                  <div class="image-wrapper ">
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">What Does a Notary Public Do?</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item ">
              <article class="post format-slider border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Tips</a>
                  </div>
                  <div class="image-wrapper border-radius-5t">
                    <div class="swiper swiper--blog-gallery">
                      <div class="swiper-container" data-speed="600" data-spacebetween="0" data-slidesPerView="1">
                        <div class="swiper-wrapper ">
                          <div class="swiper-slide border-radius-5t">
                            <img class="js-bg border-radius-5t" src="public/img/inner-pages/blog/image-4.jpg" alt="Image">
                          </div>
                          <div class="swiper-slide border-radius-5t">
                            <img class="js-bg border-radius-5t" src="public/img/inner-pages/blog/Image-one.jpg" alt="Image">
                          </div>
                          <div class="swiper-slide border-radius-5t">
                            <img class="js-bg border-radius-5t" src="public/img/inner-pages/blog/Image-three.png" alt="Image">
                          </div>
                        </div>
                      </div>
                      <div class="swiper-button-prev "><i class="fa fa-angle-left"></i></div>
                      <div class="swiper-button-next "><i class="fa fa-angle-right"></i></div>
                    </div>
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">How We`re Transforming Yet Untouched Market</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item ">
              <article class="post format-video border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Notarization</a>
                    <a href="#" rel="category tag">Tips</a>
                  </div>
                  <div class="image-wrapper video-wrapper ">
                    <a href="https://www.youtube.com/watch?v=668nUCeBHyY" class="w-100 aheto-video-link js-mfp-video border-radius-5t">
                      <img width="1280" height="720" src="../img/inner-pages/blog/image-5.jpg" class="js-bg border-radius-5t" alt="">
                    </a>
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">What Does a Notary Public Do?</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item ">
              <article class="post format-image border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Web design</a>
                    <a href="#" rel="category tag">Tips</a>
                  </div>
                  <div class="image-wrapper border-radius-5t">
                    <img src="../img/inner-pages/blog/image-6.jpg" alt="blog img" class="js-bg border-radius-5t">
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">How We`re Transforming Yet Untouched Market</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item ">
              <article class="post format-image border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Tips</a>
                  </div>
                  <div class="image-wrapper border-radius-5t">
                    <img src="../img/inner-pages/blog/image-7.jpg" alt="blog img" class="js-bg border-radius-5t">
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">Online Notarization Bill Passes in Tennessee</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item hide">
              <article class="post format-audio border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Market</a>
                  </div>
                  <div class="image-wrapper ">
                    <div class="audio-wrapper">
                      <audio controls>
                        <source src="../audio/audio-sample.mp3" type="audio/mpeg">
                      </audio>
                    </div>
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">How We`re Transforming Yet Untouched Market</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item hide">
              <article class="post format-quote border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Notarization</a>
                  </div>
                  <div class="image-wrapper ">
                  </div>
                </div>
                <div class="content-quote-wrapper">
                  <p>Follow your passion, be prepared to work hard and sacrifice, don’t let anyone limit your dreams.</p>
                  <cite>- Michale vouge</cite>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4  margin-lg-30b blog_item hide">
              <article class="post format-image border-radius-5  ">
                <div class="content-top-wrapper">
                  <div class="post-cats">
                    <a href="#" rel="category tag">Notarization</a>
                  </div>
                  <div class="image-wrapper border-radius-5t">
                    <img src="../img/inner-pages/blog/image-8.jpg" alt="blog img" class="js-bg border-radius-5t">
                  </div>
                </div>
                <div class="content-wrapper">
                  <p class="post-date">25 Jul, 2019</p>
                  <h1 class="post-title"><a href="#">What Does a Notary Public Do?</a></h1>
                  <p>Design adds clarity. Using colour, typo graphy, hierarchy, contrast, and all the other tools at their disposal, designers can take an unordered jumble of...</p>
                  <a href="#" class="aheto-btn aheto-btn--underline">Read full post</a>
                </div>
              </article>
            </div>
          </div>
        </div>
        <div class="aht-page__content-inner">
          <div class="blog-pagination-wrapper blog-pagination-wrapper--without-numbers">
            <div class="pagination">
              <a href="javascript:void(0)" class="pagn-16 pagination__learn-more ">Load more
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="site-search" id="search-box">
    <button class="close-btn js-close-search"><i class="fa fa-times" aria-hidden="true"></i></button>
    <div class="form-container">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <form role="search" method="get" class="search-form" action="http://ahetopro/" autocomplete="off">
              <div class="input-group">
                <input type="search" value="" name="s" class="search-field" placeholder="Enter Keyword" required="">
              </div>
            </form>
            <p class="search-description">Input your search keywords and press Enter.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- SCRIPTS -->
  <!-- jQuery -->
  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-migrate-1.3.0.js" integrity="sha256-/Gj+NlY1u/J2UGzM/B2QrWR01trK8ZZqrD5BdqQUsac=" crossorigin="anonymous"></script>
  <!-- Swiper -->
  <script src="{{('public/vendors/swiper/swiper.min.js')}}"></script>
  <!-- Isotope -->
  <script src="{{('public/vendors/isotope/isotope.pkgd.min.js')}}"></script>
  <!-- Images loaded library -->
  <script src="{{('public/vendors/lazyload/imagesloaded.pkgd.min.js')}}"></script>
  <!-- MediaElement js library (only for Aheto HTML) -->
  <script src="{{('public/vendors/mediaelement/mediaelement.min.js')}}"></script>
  <script src="{{('public/vendors/mediaelement/mediaelement-and-player.min.js')}}"></script>
  <!-- Typed animation text -->
  <script src="{{('public/vendors/typed/typed.min.js')}}"></script>
  <!-- Lity Lightbox -->
  <script src="{{('public/vendors/lity/lity.min.js')}}"></script>



  <!-- Magnific popup -->
  <script src="{{('public/vendors/magnific/jquery.magnific-popup.min.js')}}"></script>
  <!-- anm -->
  <script src="{{('public/vendors/animation/anm.min.js')}}"></script>
  <!-- Google maps -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARwCmK-LlGIH8Mv1ac4VyceMYUgg9vStM&amp;#038;&language=en"></script>
  <script src="{{('public/vendors/googlemap/google-maps.js?v=1')}}"></script>
  <!-- FullCalendar -->
  <!-- Parallax -->
  <script src="{{('public/vendors/parallax/parallax.min.js')}}"></script>
  <!-- asRange -->
  <script src="{{('public/vendors/range/jquery.range-min.js')}}"></script>
  <!-- lightgallery -->
  <script src="../vendors/lightgallery/lightgallery.min.js')}}"></script>
  <!-- Main script -->
  <script src="{{('public/vendors/script.js?v=1')}}"></script>
  <script src="{{('public/vendors/spectragram/spectragram.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      jQuery.fn.spectragram.accessData = {
        accessToken: '4058508404.1677ed0.f87c0182df0d4512a9e01def0c53adb7'
      }

      $('.instafeed').spectragram('getUserFeed', {
        size: 'big',
        max: 6
      });
    });
  </script>
@stop