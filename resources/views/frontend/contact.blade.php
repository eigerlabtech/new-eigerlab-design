@extends('frontend.master')

<!-- @section('css')
<style type="text/css">
  .form .wpcf7-form{
    width: 100%;
    background: red;
  }
</style>
@stop -->

@section('content')


<!-- @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif -->

<!-- @if(session()->get("success") != "") 
<div class="alert alert-success col-md-12"><a style="text-decoration:none;" href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>{{ session()->get("success")}} </div>
{{session()->put("success","")}}
@elseif(session()->get("error") != "") 
<div class="alert alert-danger col-md-12"><a style="text-decoration:none;" href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>{{session()->get("error")}} </div>
{{ session()->put("error","")}}
@endif -->
  <div class="contact-single-wrap">
    <section class="aheto-banner aheto-banner--height-600 aheto-banner--height-mob-300 " style="background-image: url('public/img/SAAS/banner_bg.png'); background-position: bottom;">
      <div class="container d-flex h-100 justify-content-center align-items-center">
        <div class="row margin-lg-50b margin-md-110b">
          <div class="aheto-heading t-center aheto-heading--t-white">
            <h1 class="aheto-heading__title          t-light ">Contact Us</h1>
          </div>
        </div>
      </div>
    </section>
    <div class="container">
      <div class="contact-single-wrap__contacts contact-single-wrap__contacts--saas">
        <div class="container">
          <div class="row padding-lg-85t padding-lg-70b padding-sm-45t padding-sm-40b">
            <div class="col-sm-4 margin-sm-50b">
              <div class="aheto-contact aheto-contact--simple aheto-contact--dvder aheto-contact--saas t-center">
                <i class="aheto-contact__icon icon ti-mobile"></i>
                <h6 class="aheto-contact__type t-uppercase t-medium">Phone</h6>
                <a class="aheto-contact__link aheto-contact__info" href="tel:(102)66668888">(102) 6666 8888</a>
              </div>
            </div>
            <div class="col-sm-4 margin-sm-50b">
              <div class="aheto-contact aheto-contact--simple aheto-contact--dvder aheto-contact--saas t-center">
                <i class="aheto-contact__icon icon ti-map-alt"></i>
                <h6 class="aheto-contact__type t-uppercase t-medium">Address</h6>
                <p class="aheto-contact__info">3rd floor, 81, Block A, Sector 4, Noida.</p>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="aheto-contact aheto-contact--simple aheto-contact--saas t-center">
                <i class="aheto-contact__icon icon ti-lock"></i>
                <h6 class="aheto-contact__type t-uppercase t-medium">Email</h6>
                <a class="aheto-contact__link aheto-contact__info" href="eigerlabtech@gmail.com">eigerlabtech@gmail.com</a>
                <a class="aheto-contact__link aheto-contact__info" href="mailto:support@aheto.com">support@aheto.com</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 margin-lg-100t margin-md-80t margin-sm-50t">
        <div class="aheto-heading t-center ">
          <h2 class="aheto-heading__title          t-light ">Get In Touch</h2>
          <p class="aheto-heading__desc   ">Created from either wood or recycled materials, it can be moulded into just about </p>
        </div>
      </div>
    </div>
    <div class="row margin-lg-50t margin-lg-100b margin-md-80b margin-sm-50b margin-sm-30t">
      <div class="col-md-12">
        <!-- @if(Session::has('message'))
        <div class="col-sm-12">
          <div class="alert alert-success alert-dismissable">
            {{ session::get('message')}}
            <a class="close" data-dismiss="alert">&times;</a>
          </div>
        </div>
        @endif -->

        <div class="aheto-form aheto-form--default aheto-form--saas">
          <p id="head"></p>
          <form name="qform" class="wpcf7-form" method="post" action="{{URL::to('/submit-form')}}" enctype="multipart/form-data">
            @csrf
            <p>
              <span class="wpcf7-form-control-wrap Name">
                <input id="name"  type="text" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"aria-required="true" aria-invalid="false" placeholder="Enter Your Name*" name="name">
                <span id="span1"></span>
              </span>
              <span class="wpcf7-form-control-wrap Email">
                <input id="email" name="email"  type="text" required="email" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your E-Mail Address*">
                <span id="span2"></span>
              </span>
              <span class="wpcf7-form-control-wrap Phone">
                <input id="mobile" name="mobile"  type="text" required="mobile" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Your Mobile Number*">
                <span id="span3"></span>
              </span>
            </p>
            <p>
            </p>
            <p>
              <span class="wpcf7-form-control-wrap Message">
                <textarea id="message" name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-required="true" aria-invalid="false" placeholder="Message*"></textarea>
                <span id="span4"></span>
              </span>
            </p>
            <p class="form-bth-holder">
              <input id="submit" type="submit" value="SEND MESSAGE" class="wpcf7-form-control wpcf7-submit ">
            </p>
          </form>
          <!-- <div id="succ_message"></div> -->
        </div>
      </div>
    </div>
  </div>
  <div id="aheto-map" class="aheto-map  " data-zoom="13" data-style="" data-center-lat="40.678223" data-center-lng="-73.920703" data-marker-img="" data-active-marker-img="" data-lat-0="40.678223" data-lng-0="-73.920703" data-img-0="public/img/inner-pages/contact/marker-1.png"
    data-active-0="" data-title-0="" data-desc-0="" data-tell-0="" data-photo-0="">
  </div>
  <div class="site-search" id="search-box">
    <button class="close-btn js-close-search"><i class="fa fa-times" aria-hidden="true"></i></button>
    <div class="form-container">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <form role="search" method="get" class="search-form" action="http://ahetopro/" autocomplete="off">
              <div class="input-group">
                <input type="search" value="" name="s" class="search-field" placeholder="Enter Keyword" required="">
              </div>
            </form>
            <p class="search-description">Input your search keywords and press Enter.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Magnific popup -->
  <script src="{{url('public/vendors/magnific/jquery.magnific-popup.min.js')}}"></script>
  <!-- anm -->
  <script src="{{url('public/vendors/animation/anm.min.js')}}"></script>
  <!-- Google maps -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARwCmK-LlGIH8Mv1ac4VyceMYUgg9vStM&amp;#038;&language=en"></script>
  <script src="{{url('public/vendors/googlemap/google-maps.js?v=1')}}"></script>
  <!-- FullCalendar -->
  <!-- Parallax -->
  <script src="{{url('public/vendors/parallax/parallax.min.js')}}"></script>
  <!-- asRange -->
  <script src="{{url('public/vendors/range/jquery.range-min.js')}}"></script>
  <!-- lightgallery -->
  <script src="{{url('public/vendors/lightgallery/lightgallery.min.js')}}"></script>
  <!-- Main script -->
  <script src="{{url('public/vendors/script.js?v=1')}}"></script>
  <script src="{{url('public/vendors/spectragram/spectragram.min.js')}}"></script>


  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
  <script>
    $(document).ready(function() {
      jQuery.fn.spectragram.accessData = {
        accessToken: '4058508404.1677ed0.f87c0182df0d4512a9e01def0c53adb7'
      }

      $('.instafeed').spectragram('getUserFeed', {
        size: 'big',
        max: 6
      });
    });
  </script>

  <script>
    $(document).ready(function() {
      $('#submit').click(function(e){
        // Initializing Variables With Form Element Values

        var name = $('#name').val();
        var email = $('#email').val();
        var mobile = $('#mobile').val();
        var message = $('#message').val();

        var name_regex = /^[a-zA-Z ]+$/;
        var email_regex =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var mobile_regex = /^\d{10}$/;

        // To Check Empty Form Fields.
        // if (name.length == 0) {
        // $('#head').html('<span class="somethingwrong" style="color:red; font-size:12px;">* All fields are mandatory *</span>').show(); // This Segment Displays The Validation Rule For All Fields
        // setTimeout(function(){ $('#head').fadeOut() }, 5000);
        // $("#name").focus();
        // return false;
        // }

        // Validating Name Field.
        if (name.length == 0) {
        $('#span1').html('<span class="namewrong" style="color:red; font-size:12px;">* Name is required *</span>').show(); // This Segment Displays The Validation Rule For Name
        setTimeout(function(){ $('#span1').fadeOut() }, 5000);
        $("#name").focus();
        return false;
        }

        else if(!name.match(name_regex)){
          $('#span1').html('<span class="namewrong" style="color:red; font-size:12px;">* Please enter a valid name *</span>').show(); // This Segment Displays The Validation Rule For Name
        setTimeout(function(){ $('#span1').fadeOut() }, 5000);
        $("#name").focus();
        return false;
        }

        // Validating Email Field.
        if (email.length == 0) {
        $('#span2').html('<span class="blankemail" style="color:red; font-size:12px;">* Email is required *</span>').show(); // This Segment Displays The Validation Rule For Name
        setTimeout(function(){ $('#span2').fadeOut() }, 5000);
        $("#email").focus();
        return false;
        }

        else if (!email.match(email_regex)) {
        $('#span2').html('<span class="emailwrong" style="color:red; font-size:12px;">* Please enter a valid email address *</span>').show(); // This Segment Displays The Validation Rule For Email
        setTimeout(function(){ $('#span2').fadeOut() }, 5000);
        $("#email").focus();
        return false;
        }

        // Validating Mobile Field.
        if (mobile.length == 0) {
        $('#span3').html('<span class="mobilewrong" style="color:red; font-size:12px;">* Mobile number is required *</span>').show(); // This Segment Displays The Validation Rule For Email
        setTimeout(function(){ $('#span3').fadeOut() }, 5000);
        $("#mobile").focus();
        return false;
        }

        else if (!mobile.match(mobile_regex)) {
        $('#span3').html('<span class="mobilewrong" style="color:red; font-size:12px;">* Please enter a valid mobile number *</span>').show(); // This Segment Displays The Validation Rule For Email
        setTimeout(function(){ $('#span3').fadeOut() }, 5000);
        $("#mobile").focus();
        return false;
        }

        // Validating Message Field.
        else if (message.length == 0) {
        $('#span4').html('<span class="mesageblank" style="color:red; font-size:12px;">* Please enter you query *</span>').show(); // This Segment Displays The Validation Rule For Email
        setTimeout(function(){ $('#span4').fadeOut() }, 5000);
        $("#message").focus();
        return false;
        }

        else {
          // $("#succ_message").html('<div class="thankstext"><a>Thanks For Contacting!!!</div>').show();
          // setTimeout(function(){ $('#succ_message').fadeOut() }, 5000);
        alert("Thanks For Contacting with us!!!");
        return true;
        }

        });
      // body...
    });
  </script>
  <!-- Testing script for form validation -->
  <script>
    // $(document).ready(function() {
    //   $('#submit').click(function(e){
        // Initializing Variables With Form Element Values

        // var name = $('#name').val();
        // var email = $('#email').val();
        // var mobile = $('#mobile').val();
        // var message = $('#message').val();

        // var name_regex = /^[a-zA-Z ]+$/;
        // var email_regex =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        // var mobile_regex = /^\d{10}$/;

        // To Check Empty Form Fields.
        // if (name.length == 0) {
        // $('#head').html('<span class="somethingwrong" style="color:red; font-size:12px;">* All fields are mandatory *</span>').show(); // This Segment Displays The Validation Rule For All Fields
        // setTimeout(function(){ $('#head').fadeOut() }, 5000);
        // $("#name").focus();
        // return false;
        // }

        // Validating Name Field.
        // else if (!name.match(name_regex) || name.length == 0) {
        // $('#span1').html('<span class="namewrong" style="color:red; font-size:12px;">* Please enter a valid name *</span>').show(); // This Segment Displays The Validation Rule For Name
        // setTimeout(function(){ $('#span1').fadeOut() }, 5000);
        // $("#name").focus();
        // return false;
        // }

        // Validating Email Field.
        // else if (!email.match(email_regex) || email.length == 0) {
        // $('#span2').html('<span class="emailwrong" style="color:red; font-size:12px;">* Please enter a valid email address *</span>').show(); // This Segment Displays The Validation Rule For Email
        // setTimeout(function(){ $('#span2').fadeOut() }, 5000);
        // $("#email").focus();
        // return false;
        // }

        // Validating Mobile Field.
        // else if (!mobile.match(mobile_regex) || mobile.length == 0) {
        // $('#span3').html('<span class="mobilewrong" style="color:red; font-size:12px;">* Please enter a valid mobile number *</span>').show(); // This Segment Displays The Validation Rule For Email
        // setTimeout(function(){ $('#span3').fadeOut() }, 5000);
        // $("#mobile").focus();
        // return false;
        // }

        // Validating Message Field.
        // else if (message.length == 0) {
        // $('#span4').html('<span class="mesageblank" style="color:red; font-size:12px;">* Please enter you query *</span>').show(); // This Segment Displays The Validation Rule For Email
        // setTimeout(function(){ $('#span4').fadeOut() }, 5000);
        // $("#message").focus();
        // return false;
        // }
        // else {
        // alert("Form Submitted Successfuly!");
        // return true;
      //   }
      // });
      // body...
    // });
  </script>
  
@stop