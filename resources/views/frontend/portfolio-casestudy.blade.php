@extends('frontend.master')
@section('content')

<!-- Animate -->
	<link rel="stylesheet" href="{{URL::to('public/css/epic/animate.css')}}">
	<!-- Icomoon -->
	<link rel="stylesheet" href="{{URL::to('public/css/epic/icomoon.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{URL::to('public/css/epic/bootstrap.css')}}">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{URL::to('public/css/epic/magnific-popup.css')}}">

	<link rel="stylesheet" href="{{URL::to('public/css/epic/style.css')}}">

<!-- Loader -->
	<div class="fh5co-loader"></div>
	
	<!-- <div id="fh5co-logo">
		<a href="index.html" class="transition"><i class="icon-camera"></i><em><span class="icon-home"></span></em></a>	
	</div> -->

	<div id="fh5co-main" role="main"  style="margin-top: 64px;">
		
		<div class="container">
			
			

			<div class="row">
				<div class="col-md-6 col-md-push-4 fh5co-heading">
					<h1>Single Showcase Photography</h1>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
					<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
				</div>
			</div>

			
			<div class="fh5co-grid">
				
				<div class="fh5co-col-1">
					<div class="fh5co-heading padding-right">
						<h2>Outdoor</h2>
						<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
					</div>
					
					<div class="fh5co-item">
						<a href="{{URL::to('public/img/epic/pic1.png')}}" class="image-popup animate-box">
							<img src="{{URL::to('public/img/epic/pic1.png')}}" alt="Free HTML5 Website Template by FreeHTML5.co" title="Nice one">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2><i class="icon-search2"></i></h2>
								</div>
							</div>
						</a>
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('public/img/epic/pic2.png')}}" class="image-popup animate-box">
							<img src="{{URL::to('public/img/epic/pic2.png')}}" alt="Free HTML5 Website Template by FreeHTML5.co">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2><i class="icon-search2"></i></h2>
								</div>
							</div>
						</a>
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('public/img/epic/pic3.png')}}" class="image-popup animate-box">
							<img src="{{URL::to('public/img/epic/pic3.png')}}" alt="Free HTML5 Website Template by FreeHTML5.co">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2><i class="icon-search2"></i></h2>
								</div>
							</div>
						</a>
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('public/img/epic/pic4.png')}}" class="image-popup animate-box">
							<img src="{{URL::to('public/img/epic/pic4.png')}}" alt="Free HTML5 Website Template by FreeHTML5.co">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2><i class="icon-search2"></i></h2>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="fh5co-col-2">

					<div class="fh5co-item">
						<a href="{{URL::to('public/img/epic/Nature.jpg')}}" class="image-popup animate-box">
							<img src="{{URL::to('public/img/epic/Nature.jpg')}}" alt="Free HTML5 Website Template by FreeHTML5.co">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2><i class="icon-search2"></i></h2>	
								</div>
							</div>
						</a>
					</div>
					
					<div class="fh5co-item">
						<a href="{{URL::to('public/img/epic/Wildlife.jpg')}}" class="image-popup animate-box">
							<img src="{{URL::to('public/img/epic/Wildlife.jpg')}}" alt="Free HTML5 Website Template by FreeHTML5.co">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2><i class="icon-search2"></i></h2>	
								</div>
							</div>
						</a>
						
					</div>

					<div class="fh5co-item">
						<a href="{{URL::to('public/img/epic/Food.jpg')}}" class="image-popup animate-box">
							<img src="{{URL::to('public/img/epic/Food.jpg')}}" alt="Free HTML5 Website Template by FreeHTML5.co">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2><i class="icon-search2"></i></h2>	
								</div>
							</div>
						</a>
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('public/img/epic/Cities.jpg')}}" class="image-popup animate-box">
							<img src="{{URL::to('public/img/epic/Cities.jpg')}}" alt="Free HTML5 Website Template by FreeHTML5.co">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2><i class="icon-search2"></i></h2>	
								</div>
							</div>
						</a>
					</div>

					<!-- <div id="fh5co-footer" class="padding-left">
						<p><small>&copy; 2016 Epic. All Rights Reserved. <br> Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://unsplash.com/" target="_blank">Unsplash</a></small></p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-twitter-with-circle"></i></a></li>
							<li><a href="#"><i class="icon-facebook-with-circle"></i></a></li>
							<li><a href="#"><i class="icon-instagram-with-circle"></i></a></li>
							<li><a href="#"><i class="icon-dribbble-with-circle"></i></a></li>
						</ul>
					</div> -->
				</div>
			</div>
		</div>

	</div>

	<!-- jQuery -->
	<script src="{{URL::to('public/js/epic/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{URL::to('public/js/epic/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{URL::to('public/js/epic/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{URL::to('public/js/epic/jquery.waypoints.min.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{URL::to('public/js/epic/jquery.magnific-popup.min.js')}}"></script>
	
	<!-- Main JS -->
	<script src="{{URL::to('public/js/epic/main.js')}}"></script>



	<!-- Modernizr JS -->
	<script src="{{URL::to('public/js/epic/modernizr-2.6.2.min.js')}}"></script>
@stop