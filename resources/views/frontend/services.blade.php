@extends('frontend.master')
<style type="text/css">
  .section-animation{
    width: 100%;
    height: 150px;
    background-color: yellow;
  }
  .ball-scale-ripple-multiple > div {
    position: absolute;
    width: 130px;
    height: 130px;
    border-radius: 100%;
    border: 1px solid dodgerblue;
    -webkit-animation: ball-scale-ripple-multiple 1.25s 0s infinite cubic-bezier(0.21, 0.53, 0.56, 0.8);
    animation: ball-scale-ripple-multiple 1.25s 0s infinite cubic-bezier(0.21, 0.53, 0.56, 0.8);
}
/*.basketwrap.chatbots .basketinfobox.num2 .ball-scale-ripple-multiple > div {
    border: 1px solid #8686ff;
}
.basketwrap.chatbots .basketinfobox.num3 .ball-scale-ripple-multiple > div {
    border: 1px solid #ff8e2d;
}
.basketwrap.chatbots .basketinfobox.num4 .ball-scale-ripple-multiple > div {
    border: 1px solid #41c3d1;
}
.basketwrap.chatbots .basketinfobox.num5 .ball-scale-ripple-multiple > div {
    border: 1px solid #59b3ff;
}*/
.ball-scale-ripple-multiple {
    position: relative;
}
.ball-scale-ripple-multiple > div:nth-child(0) {
    -webkit-animation-delay: -0.8s;
    animation-delay: -0.8s;
}
.ball-scale-ripple-multiple > div:nth-child(1) {
    -webkit-animation-delay: -0.6s;
    animation-delay: -0.6s;
}
.ball-scale-ripple-multiple > div:nth-child(2) {
    -webkit-animation-delay: -0.4s;
    animation-delay: -0.4s;
}
.ball-scale-ripple-multiple > div:nth-child(3) {
    -webkit-animation-delay: -0.2s;
    animation-delay: -0.2s;
}
@-webkit-keyframes ball-scale-ripple-multiple {
    0% {
        -webkit-transform: scale(0.1);
        transform: scale(0.1);
        opacity: 1;
    }
    70% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 0.7;
    }
    100% {
        opacity: 0;
    }
}
@keyframes ball-scale-ripple-multiple {
    0% {
        -webkit-transform: scale(0.1);
        transform: scale(0.1);
        opacity: 1;
    }
    70% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 0.7;
    }
    100% {
        opacity: 0;
    }
}

/*.whyiosrtbox .first_circle {
    position: relative;
    width: 800px;
    height: 800px;
    background: #afeef3;
    background: -webkit-linear-gradient(left, #afeef3 0, #c4f8f8 100%);
    background: linear-gradient(to right, #afeef3 0, #c4f8f8 100%);
    border-radius: 400px 0 0 400px;
}
.whyiosrtbox.testing .first_circle {
    background: #eaf6ff;
}
.whyiosrtbox.testing .second_circle {
    background: #def2ff;
}
.whyiosrtbox .second_circle {
    position: absolute;
    left: 60px;
    top: 60px;
    right: 0;
    bottom: 60px;
    background: #7be2ec;
    background: -webkit-linear-gradient(left, #7be2ec 0, #9cf2f2 100%);
    background: linear-gradient(to right, #7be2ec 0, #9cf2f2 100%);
    border-radius: 340px 0 0 340px;
    z-index: 1;
}
.whyiosrtbox .second_circle .imgbxx {
    position: absolute;
    left: 60px;
    top: 60px;
    right: 0;
    bottom: 60px;
    background: url('public/img/saas/slide_1') no-repeat;
    background-size: cover;
    border-radius: 280px 0 0 280px;
    overflow: hidden;
}
.whyiosrtbox.testing .second_circle .imgbxx {
    background: url('public/img/saas/slide_1.png') no-repeat;
    background-size: cover;
}
.whyiosrtbox .second_circle .imgbxx.nodeimg {
    background: url('public/img/saas/slide_1.png') no-repeat;
    background-size: cover;
}

.whyiossection {
    position: relative;
    padding: 150px 0 150px 100px;
}
.whyiossection.nodepg {
    padding: 100px 0 40px 100px;
}
.whyiossection .whysiOSContentbx {
    position: absolute;
    left: 100px;
    right: 900px;
    top: 150px;
}

.whyiossection .whysiOSContentbx .iosshapeCon {
    position: absolute;
    width: 563px;
    height: 800px;
    left: 50%;
    margin-left: -282px;
    background: url('public/img/saas/slide_1.png') no-repeat;
    background-size: 100% 100%;
    -webkit-animation: beat 0.25s infinite alternate;
    animation: beat 0.25s infinite alternate;
    -webkit-transform-origin: center;
    transform-origin: center;
}
.whyiossection .whysiOSContentbx .iosshapeCon.android {
    width: 100%;
    height: 800px;
    margin-left: -340px;
    background: url(../images/android_big_icon.png) no-repeat;
    background-size: 100% 100%;
    -webkit-animation: none;
    animation: none;
}
.whyiossection .iosdetailsbx {
    position: relative;
    padding-top: 230px;
}
.whyiossection .iosdetailsbx.nodejsdetails,
.whyiossection.androidApp .iosdetailsbx {
    padding-top: 130px;
}

.whyiossection.androidApp .iosdetailsbx h2 {
    position: relative;
    color: #08c5dc;
    text-align: left;
    margin-top: 0;
    padding-bottom: 30px;
}
.whyiossection.androidApp .iosdetailsbx h2:after {
    position: absolute;
    content: "";
    width: 150px;
    height: 2px;
    left: 0;
    bottom: 10px;
    background-color: #50e8e9;
}

.whyiossection.androidApp .iosdetailsbx h3 {
    padding-bottom: 10px;
}
.iosdetailsbx h3:after {
    position: absolute;
    content: "";
    width: 150px;
    height: 2px;
    left: 50%;
    bottom: 0;
    margin-left: -75px;
    background-color: #5cb5ff;
}
.whyiossection.androidApp .iosdetailsbx h3 {
    text-align: left;
}
.whyiossection.androidApp .iosdetailsbx h3:after {
    display: none;
}
.whyiossection.testing {
    padding: 80px 0 80px 100px;
}
@-webkit-keyframes beat {
    to {
        -webkit-transform: scale(1.03);
        transform: scale(1.03);
    }
}
@keyframes beat {
    to {
        -webkit-transform: scale(1.03);
        transform: scale(1.03);
    }
}*/
</style>
@section('content')

<?php
          foreach($service_data as $service){
          
            ?>
  <section  class="aheto-banner aheto-banner aheto-banner--height-450 aheto-banner--height-mob-200" style="background-image: url('{{$service['service_banner']}}'); background-position: center; border: 1px solid red;">
    <?php }?>
    <div class="container d-flex h-100 justify-content-center align-items-center">
      <div class="row padding-lg-60t padding-sm-0t">
        <div class="aheto-heading t-center aheto-heading--t-white">
          <h1 class="aheto-heading__title          t-light ">Service Details</h1>
        </div>
      </div>
    </div>
  </section>
  <!-- <hr> -->
  <section >
    <div class="container margin-lg-45t margin-lg-55b margin-md-50t margin-sm-50t margin-sm-20b ">
      <div class="row row--flex row--v-center ">
        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
          <div>
            <?php
          foreach($service_data as $service){
          
            ?>
            <div class="aheto-heading t-left sm-t-center aheto-heading">
              <h2 class="aheto-heading__title          t-light ">{{$service['service_title']}}</h2>
            </div>
            <?php }?>
          </div>
          <div class="margin-lg-25t">
            <?php
          foreach($service_data as $service){
          
            ?>
            <div class="aheto-heading t-left sm-t-center aheto-heading">
              <!-- <p class="aheto-heading__title       l-height--163    ">Sed orci mauris, vestibulum fringilla contsequat amet, convallis sed quam. Etiam non velit tincidunt, mollis felisotiro elementum, commodo metus. Pellentesque a risus volutpat,
                egestas metus ut, tristique odio. Curabitur sit amet sapien ut neque interdroum iaculis a a mi. Vivamus quis nibh orci. Donec scelerisque doloros vitae molestie consequat. Proin sagittis eleifend dui esollicitudin. Curabitur viverra
                suscipit ipsum lacinia accumsan.<br><br>Mauris congue luctus tincidunt. Etiam non velit tincidunt, mollis felisotiro elementum, commodo metus.</p> -->
                <p class="aheto-heading__title       l-height--163    ">{{$service['service_description']}}</p>
            </div>
            <?php }?>
          </div>
        </div>
        <div class="col-sm-12 offset-lg-1 col-md-6 col-xs-12 sm-t-center margin-sm-30t margin-sm-30b">
          <?php
          foreach($service_data as $service){
          
            ?>
          
          <img src="{{$service['service_image']}}" alt="" class="w-100" style="border-radius: 100%">
          <?php }?>
        </div>
      </div>
    </div>
  </section>
  <section style="background-color: #f3f9ff;">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 offset-sm-3 margin-lg-120t margin-md-80t margin-sm-50t retreat">
          <div class="aheto-heading t-center aheto-heading">
            <h6 class="aheto-heading__subtitle t-bold text-color--grey f-14 text-uppercase let-spasing">HOW IT WORKS</h6>
            <h2 class="aheto-heading__title          t-light ">Nostrud exercitation ullamco laboris nisi ut aliquip</h2>
          </div>
        </div>
      </div>
      <div class="row margin-lg-65t margin-md-40t margin-sm-20t padding-lg-90b padding-md-60b padding-sm-30b">
        <div class="col-sm-4">
          <div class="aheto-content-block aheto-content-block--saas t-center  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-bag">
                </i>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">Nostrud exercitation ullamco<br> laboris nisi ut aliquip</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="aheto-content-block aheto-content-block--saas t-center  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ion-ios-monitor-outline">
                </i>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">Exercitation ullamco laboris<br> nisi ut aliquip</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="aheto-content-block aheto-content-block--saas t-center  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ion-levels">
                </i>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">Ullamco laboris nisi ut aliquip<br> nostrud exercitation</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
          foreach($service_data as $service){
            // dd($service);
          ?>
  <section>
    <div class="container margin-lg-45t margin-lg-60b margin-md-70t margin-sm-50t margin-sm-20b">
      <div class="row row--flex row--v-center ">
        <div class="col-sm-12 col-md-6 col-xs-12 sm-t-center margin-xs-20b">
          <img src="{{$service['section3_image']}}" alt="" class="w-100">
        </div>
        <div class="col-lg-5 offset-lg-1 col-md-6 col-sm-12 col-xs-12 margin-lg-30t">
          <div>
            <div class="aheto-heading t-left sm-t-center aheto-heading">
              <h2 class="aheto-heading__title          t-light ">{{$service['section3_title']}}</h2>
            </div>
          </div>
          <div class="margin-lg-30t">
            <div class="aheto-heading t-left sm-t-center aheto-heading">
              <p class="aheto-heading__title       l-height--163    ">{{$service['section3_description']}}</p>
            </div>
          </div>
          
          <ul class="ul--dotted ul--alter retreat--0 margin-lg-30t margin-md-20t">
            <?php
          for($i=0;$i<count($service['section3_points']);$i++){
            // dd($service['section3_points']);
            // die();
          ?>
            <li>{{$service['section3_points'][$i]}}</li>
            <?php }?>
            <!-- <li>Transaction Details</li>
            <li>Email Notifications</li> -->
          </ul>
           
        </div>
      </div>
    </div>
  </section>
  <?php }?>

  <hr>

  
   <section>
    <div class="container margin-lg-55t margin-lg-25b margin-md-10t margin-sm-50t margin-sm-20b ">
      <div class="row row--flex row--v-center ">
        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
          <div>
            <div class="aheto-heading t-left sm-t-center aheto-heading">
              <?php
    foreach($service_data as $service){
    // dd($service);
    ?>
              <h2 class="aheto-heading__title          t-light ">{{$service['section4_title']}}</h2>
              <?php }?>
            </div>
          </div>
          <div class="margin-lg-25t">
            <div class="aheto-heading t-left sm-t-center aheto-heading">
            </div>
          </div>
          <ul class="ul--dotted ul--alter retreat--0 margin-lg-30t margin-md-20t">
            <?php
          for($i=0;$i<count($service['section4_points']);$i++){
            // dd($service['section3_points']);
            // die();
          ?>
            <li>{{$service['section4_points'][$i]}}</li>
            <!-- <li>Transaction Details</li>
            <li>Email Notifications</li> -->
            <?php }?>
          </ul>
        </div>
        <div class="col-sm-12 offset-lg-1 col-md-6 col-xs-12 sm-t-center">
          <img src="{{$service['section4_image']}}" alt="" class="w-100">
        </div>
      </div>
      <!-- <div class="row row--flex row--v-center ">
        <div class="col-sm-12 col-md-6 col-xs-12 sm-t-center order-last order-md-first order-lg-first">
          <img src="{{URL::to('public/img/saas/features_img-2.png')}}" alt="" class="w-100">
        </div>
        <div class="col-lg-5 offset-lg-1 col-md-6 col-sm-12 col-xs-12 order-first order-md-last order-lg-last  margin-sm-30t">
          <div>
            <div class="aheto-heading t-left sm-t-center aheto-heading">
              <h2 class="aheto-heading__title          t-light ">Give your customers access from <span>anywhere in the world.</span></h2>
            </div>
          </div>
          <div class="margin-lg-25t">
            <div class="aheto-heading t-left sm-t-center aheto-heading">
            </div>
          </div>
          <ul class="ul--dotted ul--alter retreat--0 margin-lg-30t margin-md-20t">
            <li>iPhone</li>
            <li>iPad</li>
            <li>Android devices</li>
          </ul>
        </div>
      </div> -->
    </div>
  </section>
  
  <hr>
  <section>
    <div class="container margin-lg-115t margin-lg-85b margin-md-75t margin-md-40b margin-sm-50t">
      <div class="row">
        <div class="col-md-6 offset-md-3">
          <div class="aheto-heading t-center aheto-heading">
            <h2 class="aheto-heading__title          t-light ">See more <span>features</span></h2>
          </div>
        </div>
      </div>
      <div class="row margin-lg-65t margin-md-30t margin-sm-0t no-scroll ">
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-mobile">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Responsive</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">Your landing page displays smoothly on any device: desktop, tablet or mobile.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-settings">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Customizable</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">You can easily read, edit, and write your own code, or change everything.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-package">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">UI Elements</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">There is a bunch of useful and necessary elements for developing your website.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-bolt">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Clean Code</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">You can find our code well organized, commented and readable.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-layout-tab">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Documented</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">As you can see in the source code, we provided a comprehensive documentation.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-harddrive">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Free Updates</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">When you purchase this template, you'll freely receive future updates.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-cloud-down">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Storage and Deletion</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">Karma does not store client documents or retain any Non-Public Information (NPI).</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-loop">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">API Integration</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">Designed well as enterprises or other industry solution providers.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-folder">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Digital Recording</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">Platform captures and stores a digital audio/video recording to sign the document.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

 

  <!-- <div style="background-color: #f7f7f7;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="padding-lg-25t padding-lg-20b">
          <div class="aheto-clients aheto-clients--5-in-row aheto-clients--low-opacity">
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{URL::to('public/img/home/client1.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{URL::to('public/img/home/client2.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{URL::to('public/img/home/client3.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{URL::to('public/img/home/client4.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{URL::to('public/img/home/client6.png')}}" alt="Clients logo image">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->

  <div style="background-color: #f7f7f7;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="padding-lg-25t padding-lg-20b">
          <div class="aheto-clients aheto-clients--5-in-row aheto-clients--low-opacity">
            <?php
              foreach($clients_info as $client){
            // dd($client);
                
            ?>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{$client['logo']}}" alt="Clients logo image">
              </a>
            </div>
            <?php }?>

            <!-- <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client2.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client3.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client4.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client6.png')}}" alt="Clients logo image">
              </a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- <section class="section-animation">
  <div class="ball-scale-ripple-multiple">
    <div></div>
    <div></div>
    <div></div>
  </div>
</section> -->

<!-- <div class="whyiossection nodepg clearfix">
   <div class="whyiosrtbox">
      <div class="first_circle" data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="600" data-aos-delay="300">
         <div class="second_circle" data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="600" data-aos-delay="400">
            <div class="imgbxx nodeimg" data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="600" data-aos-delay="500"></div>
         </div>
      </div>
   </div>
   <div class="whysiOSContentbx">
      <div class="iosdetailsbx nodejsdetails">
         <h2 data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="600" data-aos-offset="0">Why Your business must<br> pick it?</h2>
         <div class="smtinfo">
            <p data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="600" data-aos-offset="0">With Node.js your business experiences the exceptional power to build faster and scalable real-time applications. Also it helps your business to streamline the back-end and front-end development, because it works on the Google V8 JavaScript engine to execute the code, and the maximum modules are written in JavaScript. </p>
            <p data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="600" data-aos-offset="0">It further contains the built-in library, letting the applications to act as a Web server without using the software such as Apache HTTP Server or IIS. </p>
         </div>
      </div>
   </div>
</div> -->

  <!-- <section class="aheto-banner aheto-banner--height-500 d-flex aheto-banner--promo" style="background-image: url('public/img/saas/banner_bg-3.png')">
    <div class="container margin-lg-35t padding-lg-60b d-flex">
      <div class="row w-100 align-items-center">
        <div class="col-md-5 col-xs-12 md-t-center">
          <div class="aheto-heading t-left md-t-center aheto-heading--t-white">
            <h2 class="aheto-heading__title          t-light ">Download the Inbox app to get started.</h2>
          </div>
          <div class="aheto-banner__rating margin-lg-50t margin-sm-15t">
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
          </div>
          <div class="aheto-heading t-left md-t-center aheto-heading--t-white">
            <p class="aheto-heading__title          t-light ">Base on <b>2,000+ reviews</b></p>
          </div>
        </div>
        <div class="col-md-6 offset-md-1 col-xs-12 t-right md-t-center margin-lg-35t margin-sm-15t">
          <div class="aheto-banner__store margin-lg-25b">
            <a href="https://www.apple.com/lae/ios/app-store/" target="_blank"><img alt="" src="{{URL::to('public/img/saas/app-store.png')}}"></a>
            <a href="https://play.google.com/" target="_blank"><img src="{{URL::to('public/img/saas/google-play.png')}}" alt=""></a>
          </div>
          <div class="aheto-heading t-right md-t-center aheto-heading--t-white">
            <p class="aheto-heading__title          t-light ">Also available on the web at <b>inbox.google.com.</b></p>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  
  <!--<div class="site-search" id="search-box">-->
  <!--  <button class="close-btn js-close-search"><i class="fa fa-times" aria-hidden="true"></i></button>-->
  <!--  <div class="form-container">-->
  <!--    <div class="container">-->
  <!--      <div class="row">-->
  <!--        <div class="col-lg-12">-->
  <!--          <form role="search" method="get" class="search-form" action="http://ahetopro/" autocomplete="off">-->
  <!--            <div class="input-group">-->
  <!--              <input type="search" value="" name="s" class="search-field" placeholder="Enter Keyword" required="">-->
  <!--            </div>-->
  <!--          </form>-->
  <!--          <p class="search-description">Input your search keywords and press Enter.</p>-->
  <!--        </div>-->
  <!--      </div>-->
  <!--    </div>-->
  <!--  </div>-->
  <!--</div>-->
  <!-- Magnific popup -->
  <script src="{{URL::to('public/vendors/magnific/jquery.magnific-popup.min.js')}}"></script>
  <!-- anm -->
  <script src="{{URL::to('public/vendors/animation/anm.min.js')}}"></script>
  <!-- Google maps -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARwCmK-LlGIH8Mv1ac4VyceMYUgg9vStM&amp;#038;&language=en"></script>
  <script src="{{URL::to('public/vendors/googlemap/google-maps.js?v=1')}}"></script>
  <!-- FullCalendar -->
  <!-- Parallax -->
  <script src="{{URL::to('public/vendors/parallax/parallax.min.js')}}"></script>
  <!-- asRange -->
  <script src="{{URL::to('public/vendors/range/jquery.range-min.js')}}"></script>
  <!-- lightgallery -->
  <script src="{{URL::to('public/vendors/lightgallery/lightgallery.min.js')}}"></script>
  <!-- Main script -->
  <script src="{{URL::to('public/vendors/script.js?v=1')}}"></script>
  <script src="{{URL::to('public/vendors/spectragram/spectragram.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      jQuery.fn.spectragram.accessData = {
        accessToken: '4058508404.1677ed0.f87c0182df0d4512a9e01def0c53adb7'
      }

      $('.instafeed').spectragram('getUserFeed', {
        size: 'big',
        max: 6
      });
    });
  </script>
@stop