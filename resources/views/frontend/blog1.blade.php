<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Blog - FlexStart Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{URL::to('public/flex-start-blog/assets/img/favicon.png')}}" rel="icon">
  <link href="{{URL::to('public/flex-start-blog/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{URL::to('public/flex-start-blog/assets/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: FlexStart - v1.0.0
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

  <!-- <link href="{{URL::to('public/css/navigationbarcss/home-page.css')}}" rel="stylesheet" type="text/css"> -->
  <style>
    /*#loading {
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  position: fixed;
  display: block;
  opacity: 0.7;
  background-color: #fff;
  z-index: 99;
  text-align: center;
}

#loading-image {
  position: absolute;
  top: 100px;
  left: 240px;
  z-index: 100;
}*/

.loader,
        .loader:after {
            border-radius: 50%;
            width: 10em;
            height: 10em;
        }
        .loader {            
            margin: 60px auto;
            font-size: 10px;
            position: relative;
            text-indent: -9999em;
            border-top: 1.1em solid rgba(255, 255, 255, 0.2);
            border-right: 1.1em solid rgba(255, 255, 255, 0.2);
            border-bottom: 1.1em solid rgba(255, 255, 255, 0.2);
            border-left: 1.1em solid #ffffff;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
            -webkit-animation: load8 1.1s infinite linear;
            animation: load8 1.1s infinite linear;
        }
        @-webkit-keyframes load8 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes load8 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        #loadingDiv {
            position:absolute;;
            padding-top: 200px;
            top:0;
            left:0;
            width:100%;
            height:100%;
            background-color:#000;
        }
  </style>
</head>

<body>

  <!-- ======= Header ======= -->
  <!-- <div id="loading">
  <img id="loading-image" src= "{{URL::to('public/img/loader/ajax-loader.gif')}}" alt="Loading..." />
</div> -->
  <header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="{{URL::to('/')}}" class="logo d-flex align-items-center">
        <img src="{{URL::to('public/flex-start-blog/assets/img/logo.png')}}" alt="">
        <span>Eigerlab Technologies</span>
      </a>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link " href="{{URL::to('/')}}">Home</a></li>
          <li><a class="nav-link scrollto" href="{{URL::to('/about')}}">About</a></li>
          <li><a class="nav-link scrollto " href="#services">Services</a></li>
          <li><a class="nav-link scrollto" href="#portfolio">Portfolio</a></li>
          <li><a class="nav-link scrollto" href="#team">Team</a></li>
          <li><a class="active" href="{{URL::to('/blog')}}">Blog</a></li>
          <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="getstarted scrollto" href="#about">Get Started</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{URL::to('/')}}">Home</a></li>
          <li>Blog</li>
        </ol>
        <h2>Blog</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-8 entries all-blogs">

            <article class="entry ">

              <div class="entry-img">
                <img src="{{URL::to('public/flex-start-blog/assets/img/blog/blog-1.jpg')}}" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a href="blog-single.html">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
              </h2>

              <div class="entry-meta">
                <ul>
                  <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-single.html">John Doe</a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-single.html"><time datetime="2020-01-01">Jan 1, 2020</time></a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> <a href="blog-single.html">12 Comments</a></li>
                </ul>
              </div>

              <div class="entry-content">
                <p>
                  Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta.
                  Et eveniet enim. Qui velit est ea dolorem doloremque deleniti aperiam unde soluta. Est cum et quod quos aut ut et sit sunt. Voluptate porro consequatur assumenda perferendis dolore.
                </p>
                <div class="read-more">
                  <a href="blog-single.html">Read More</a>
                </div>
              </div>

            </article><!-- End blog entry -->

            <article class="entry">

              <div class="entry-img">
                <img src="{{URL::to('public/flex-start-blog/assets/img/blog/blog-2.jpg')}}" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a href="blog-single.html">Nisi magni odit consequatur autem nulla dolorem</a>
              </h2>

              <div class="entry-meta">
                <ul>
                  <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-single.html">John Doe</a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-single.html"><time datetime="2020-01-01">Jan 1, 2020</time></a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> <a href="blog-single.html">12 Comments</a></li>
                </ul>
              </div>

              <div class="entry-content">
                <p>
                  Incidunt voluptate sit temporibus aperiam. Quia vitae aut sint ullam quis illum voluptatum et. Quo libero rerum voluptatem pariatur nam.
                  Ad impedit qui officiis est in non aliquid veniam laborum. Id ipsum qui aut. Sit aliquam et quia molestias laboriosam. Tempora nam odit omnis eum corrupti qui aliquid excepturi molestiae. Facilis et sint quos sed voluptas. Maxime sed tempore enim omnis non alias odio quos distinctio.
                </p>
                <div class="read-more">
                  <a href="blog-single.html">Read More</a>
                </div>
              </div>

            </article><!-- End blog entry -->

            <article class="entry">

              <div class="entry-img">
                <img src="{{URL::to('public/flex-start-blog/assets/img/blog/blog-3.jpg')}}" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a href="blog-single.html">Possimus soluta ut id suscipit ea ut. In quo quia et soluta libero sit sint.</a>
              </h2>

              <div class="entry-meta">
                <ul>
                  <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-single.html">John Doe</a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-single.html"><time datetime="2020-01-01">Jan 1, 2020</time></a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> <a href="blog-single.html">12 Comments</a></li>
                </ul>
              </div>

              <div class="entry-content">
                <p>
                  Aut iste neque ut illum qui perspiciatis similique recusandae non. Fugit autem dolorem labore omnis et. Eum temporibus fugiat voluptate enim tenetur sunt omnis.
                  Doloremque est saepe laborum aut. Ipsa cupiditate ex harum at recusandae nesciunt. Ut dolores velit.
                </p>
                <div class="read-more">
                  <a href="blog-single.html">Read More</a>
                </div>
              </div>

            </article><!-- End blog entry -->

            <article class="entry">

              <div class="entry-img">
                <img src="{{URL::to('public/flex-start-blog/assets/img/blog/blog-4.jpg')}}" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a href="blog-single.html">Non rem rerum nam cum quo minus. Dolor distinctio deleniti explicabo eius exercitationem.</a>
              </h2>

              <div class="entry-meta">
                <ul>
                  <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-single.html">John Doe</a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-single.html"><time datetime="2020-01-01">Jan 1, 2020</time></a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> <a href="blog-single.html">12 Comments</a></li>
                </ul>
              </div>

              <div class="entry-content">
                <p>
                  Aspernatur rerum perferendis et sint. Voluptates cupiditate voluptas atque quae. Rem veritatis rerum enim et autem. Saepe atque cum eligendi eaque iste omnis a qui.
                  Quia sed sunt. Ea asperiores expedita et et delectus voluptates rerum. Id saepe ut itaque quod qui voluptas nobis porro rerum. Quam quia nesciunt qui aut est non omnis. Inventore occaecati et quaerat magni itaque nam voluptas. Voluptatem ducimus sint id earum ut nesciunt sed corrupti nemo.
                </p>
                <div class="read-more">
                  <a href="blog-single.html">Read More</a>
                </div>
              </div>

            </article><!-- End blog entry -->

            <div class="blog-pagination">
              <ul class="justify-content-center">
                <li><a href="#">1</a></li>
                <li class="active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
              </ul>
            </div>

          </div><!-- End blog entries list -->

          <div class="col-lg-4">

            <div class="sidebar">

              <h3 class="sidebar-title">Search</h3>
              <div class="sidebar-item search-form">
                <form action="">
                  <input type="text">
                  <button type="submit"><i class="bi bi-search"></i></button>
                </form>
              </div><!-- End sidebar search formn-->

              <h3 class="sidebar-title">Categories</h3>
              <div class="sidebar-item categories blog-category">
                <ul>
                  <li><a href="#">General <span>(25)</span></a></li>
                  <li><a href="#">Lifestyle <span>(12)</span></a></li>
                  <li><a href="#">Travel <span>(5)</span></a></li>
                  <li><a href="#">Design <span>(22)</span></a></li>
                  <li><a href="#">Creative <span>(8)</span></a></li>
                  <li><a href="#">Educaion <span>(14)</span></a></li>
                </ul>
              </div><!-- End sidebar categories-->

              <h3 class="sidebar-title">Recent Posts</h3>
              <div class="sidebar-item recent-posts">
                <div class="post-item clearfix">
                  <img src="{{URL::to('public/flex-start-blog/assets/img/blog/blog-recent-1.jpg')}}" alt="">
                  <h4><a href="blog-single.html">Nihil blanditiis at in nihil autem</a></h4>
                  <time datetime="2020-01-01">Jan 1, 2020</time>
                </div>

                <div class="post-item clearfix">
                  <img src="{{URL::to('public/flex-start-blog/assets/img/blog/blog-recent-2.jpg')}}" alt="">
                  <h4><a href="blog-single.html">Quidem autem et impedit</a></h4>
                  <time datetime="2020-01-01">Jan 1, 2020</time>
                </div>

                <div class="post-item clearfix">
                  <img src="{{URL::to('public/flex-start-blog/assets/img/blog/blog-recent-3.jpg')}}" alt="">
                  <h4><a href="blog-single.html">Id quia et et ut maxime similique occaecati ut</a></h4>
                  <time datetime="2020-01-01">Jan 1, 2020</time>
                </div>

                <div class="post-item clearfix">
                  <img src="{{URL::to('public/flex-start-blog/assets/img/blog/blog-recent-4.jpg')}}" alt="">
                  <h4><a href="blog-single.html">Laborum corporis quo dara net para</a></h4>
                  <time datetime="2020-01-01">Jan 1, 2020</time>
                </div>

                <div class="post-item clearfix">
                  <img src="{{URL::to('public/flex-start-blog/assets/img/blog/blog-recent-5.jpg')}}" alt="">
                  <h4><a href="blog-single.html">Et dolores corrupti quae illo quod dolor</a></h4>
                  <time datetime="2020-01-01">Jan 1, 2020</time>
                </div>

              </div><!-- End sidebar recent posts-->

              <h3 class="sidebar-title">Tags</h3>
              <div class="sidebar-item tags">
                <ul>
                  <li><a href="#">App</a></li>
                  <li><a href="#">IT</a></li>
                  <li><a href="#">Business</a></li>
                  <li><a href="#">Mac</a></li>
                  <li><a href="#">Design</a></li>
                  <li><a href="#">Office</a></li>
                  <li><a href="#">Creative</a></li>
                  <li><a href="#">Studio</a></li>
                  <li><a href="#">Smart</a></li>
                  <li><a href="#">Tips</a></li>
                  <li><a href="#">Marketing</a></li>
                </ul>
              </div><!-- End sidebar tags-->

            </div><!-- End sidebar -->

          </div><!-- End blog sidebar -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-12 text-center">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
          </div>
          <div class="col-lg-6">
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row gy-4">
          <div class="col-lg-5 col-md-12 footer-info">
            <a href="{{URL::to('/')}}" class="logo d-flex align-items-center">
              <img src="{{URL::to('public/flex-start-blog/assets/img/logo.png')}}" alt="">
              <span>Eigerlab Technologies</span>
            </a>
            <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus.</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
              <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
              <a href="#" class="instagram"><i class="bi bi-instagram bx bxl-instagram"></i></a>
              <a href="#" class="linkedin"><i class="bi bi-linkedin bx bxl-linkedin"></i></a>
            </div>
          </div>

          <div class="col-lg-2 col-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <!-- <li><i class="bi bi-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Privacy policy</a></li> -->
              <li><i class="bi bi-chevron-right"></i> <a href="{{URL::to('/')}}">Home</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="{{URL::to('/about')}}">About us</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="{{URL::to('/contact')}}">Contact Us</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="{{URL::to('/career')}}">Career</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Privacy</a></li>
            </ul>
          </div>

          <div class="col-lg-2 col-6 footer-links">
            <h4>Our Services</h4>
            <ul class="blog-footer-services">
              <li><i class="bi bi-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
            <h4>Contact Us</h4>
            <p>
              A108 Adam Street <br>
              New York, NY 535022<br>
              United States <br><br>
              <strong>Phone:</strong> +1 5589 55488 55<br>
              <strong>Email:</strong> info@example.com<br>
            </p>

          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>FlexStart</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexstart-bootstrap-startup-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/aos/aos.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/purecounter/purecounter.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{URL::to('public/flex-start-blog/assets/js/main.js')}}"></script>



  <script src="{{URL::to('public/js/jquery-2.1.1.js')}}"></script>
  <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script> -->

  <script>
    // get blog-categories data 

        var blog_category = [];

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/blog-category',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                var str = res.category_name;
                var category_name = str.replace(/\s+/g, '-').toLowerCase();
                var url = "{{URL::to('/blog-category')}}" +'/' + category_name;
                blog_category.push('<ul><li><a href="'+ url +'">'+ res.category_name +'<span>'+(25)+'</span></a></li></ul>');
                // <ul>
                //   <li><a href="#">General <span>(25)</span></a></li>
                })
                $('.blog-category').html(blog_category);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });

        // get all blogs 

        var blogs = [];

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/blogs',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                var url = "{{URL::to('/blogs')}}" +'/' + res.id;
                // console.log(url);
                // blog_category.push('<ul><li><a href="'+ url +'">'+ res.category_name +'<span>'+(25)+'</span></a></li></ul>');

                blogs.push('<article class="entry"><div class="entry-img"><img src="'+ res.blog_image +'" alt="" class="img-fluid" style="max-width: 100%;"></div><h2 class="entry-title"><a href="#">'+ res.blog_title +'</a></h2><div class="entry-meta"><ul><li class="d-flex align-items-center"><i class="bi bi-person"></i><a href="#">"'+ res.blogger_name +'"</a></li><li class="d-flex align-items-center"><i class="bi bi-clock"></i><a href="#"><time datetime="2020-01-01">"'+ res.created_at +'"</time></a></li></ul></div><div class="entry-content"><p>"'+ res.blog_description +'"</p></div></article>');
                })
                $('.all-blogs').html(blogs);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });

  </script>

  <script>
       // get services data for footer

        var blogfooterServices = [];
        // var footerTechnologyArray = [];

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/services',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                var url = "{{URL::to('/services')}}" + res.url;
                // console.log(url);
                blogfooterServices.push('<li><i class="bi bi-chevron-right"></i><a href="'+ url +'">'+ res.title +'</a></li>');
                })
                $('.blog-footer-services').html(blogfooterServices);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });

        //get technologies data

        // $.ajax({
        //     url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/technologies',
        //     method: 'GET',
        //     dataType: 'json',
        //     success: function (response) {
                
        //         response.map(res => {
        //             var url = "{{URL::to('/technology')}}" + res.url;
        //             footerTechnologyArray.push('<ul class="menu"><li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="'+ url +'">'+ res.title +'</a></li><li></li></ul>');
        //         })
        //         $('.footerTechnologies').html(footerTechnologyArray);
        //     },
        //     error : function(request, status, err){
        //         console.log(err)
        //     }
        // });
  </script>
  
  <!-- <script>
  $(window).load(function() {
    $('#loading').hide();
  });
</script> -->

<!-- <script>
  $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
$(window).on('load', function(){
  wait for page load PLUS two seconds.
  setTimeout(removeLoader, 2000); 
});
function removeLoader(){
    $( "#loadingDiv" ).fadeOut(500, function() {
      fadeOut complete. Remove the loading div
      makes page more lightweight
      $( "#loadingDiv" ).remove(); 
  });  
}
</script> -->

</body>

</html>