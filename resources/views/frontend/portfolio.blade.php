@extends('frontend.master')
@section('content')
<!-- Animate -->
	<link rel="stylesheet" href="{{URL::to('public/css/epic/animate.css')}}">
	<!-- Icomoon -->
	<link rel="stylesheet" href="{{URL::to('public/css/epic/icomoon.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{URL::to('public/css/epic/bootstrap.css')}}">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{URL::to('public/css/epic/magnific-popup.css')}}">

	<link rel="stylesheet" href="{{URL::to('public/css/epic/style.css')}}">
<!-- Loader -->
	<div class="fh5co-loader"></div>

	<!-- <div id="fh5co-logo" style="margin-top: 64px;">
		<a href="index.html" class="transition"><i class="icon-camera"></i><em><span class="icon-home"></span></em></a>	
	</div> -->

	<div id="fh5co-main" role="main" style="margin-top: 64px;">
		

		<div class="container">
			
			<div class="fh5co-grid">
				
				<div class="fh5co-col-1">
					<div class="fh5co-intro padding-right">
						<h1>More of our work <em>&amp;</em> We build identities and experiences to elevate and empower organizations with our alien computing technology.<a href="javascript:void(0);" class="transition"><em></em></a></h1>
						<!-- <ul class="fh5co-social">
							<li><a href="#"><i class="icon-twitter-with-circle"></i></a></li>
							<li><a href="#"><i class="icon-facebook-with-circle"></i></a></li>
							<li><a href="#"><i class="icon-instagram-with-circle"></i></a></li>
							<li><a href="#"><i class="icon-dribbble-with-circle"></i></a></li>
						</ul> -->
					</div>
					<div class="fh5co-item">
						<!-- style="background: #39b7fc;" -->
						<a href="{{URL::to('/portfolio-casestudy')}}" class="transition animate-box">
							<img src="{{URL::to('public/img/epic/pic1.png')}}" alt="outdoor">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2>Modello</h2>
								</div>
							</div>
						</a>
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('/portfolio-casestudy')}}" class="transition animate-box">
							<img src="{{URL::to('public/img/epic/pic2.png')}}" alt="Food">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2>Theekher</h2>
								</div>
							</div>
						</a>
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('/portfolio-casestudy')}}" class="transition animate-box">
							<img src="{{URL::to('public/img/epic/pic3.png')}}" alt="Nature">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2>Bonpiti</h2>
								</div>
							</div>
						</a>
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('/portfolio-casestudy')}}" class="transition animate-box">
							<img src="{{URL::to('public/img/epic/pic4.png')}}" alt="Travel">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2>VMS Console</h2>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="fh5co-col-2">
					<div class="fh5co-item">
						<a href="{{URL::to('/portfolio-casestudy')}}" class="transition animate-box">
							<img src="{{URL::to('public/img/epic/pic5.png')}}" alt="Nature">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2>Taxytheapp</h2>
								</div>
							</div>
						</a>
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('/portfolio-casestudy')}}" class="transition animate-box">
							<img src="{{URL::to('public/img/epic/Wildlife.jpg')}}" alt="Wildlife">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2>Wildlife</h2>
								</div>
							</div>
						</a>
						
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('/portfolio-casestudy')}}" class="transition animate-box">
							<img src="{{URL::to('public/img/epic/Food.jpg')}}" alt="Faces">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2>Food</h2>
								</div>
							</div>
						</a>
					</div>
					<div class="fh5co-item">
						<a href="{{URL::to('/portfolio-casestudy')}}" class="transition animate-box">
							<img src="{{URL::to('public/img/epic/Cities.jpg')}}" alt="Cities">
							<div class="fh5co-item-text-wrap">
								<div class="fh5co-item-text">
									<h2>Cities</h2>
								</div>
							</div>
						</a>
					</div>

					<!-- <div id="fh5co-footer" class="padding-left">
						<p><small>&copy; 2016 Epic. All Rights Reserved. <br> Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://unsplash.com/" target="_blank">Unsplash</a></small></p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-twitter-with-circle"></i></a></li>
							<li><a href="#"><i class="icon-facebook-with-circle"></i></a></li>
							<li><a href="#"><i class="icon-instagram-with-circle"></i></a></li>
							<li><a href="#"><i class="icon-dribbble-with-circle"></i></a></li>
						</ul>
					</div> -->
				</div>
			</div>
		</div>

	</div>
	<!-- jQuery -->
	<script src="{{URL::to('public/js/epic/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{URL::to('public/js/epic/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{URL::to('public/js/epic/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{URL::to('public/js/epic/jquery.waypoints.min.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{URL::to('public/js/epic/jquery.magnific-popup.min.js')}}"></script>
	
	<!-- Main JS -->
	<script src="{{URL::to('public/js/epic/main.js')}}"></script>

	<!-- Modernizr JS -->
	<script src="{{URL::to('public/js/epic/modernizr-2.6.2.min.js')}}"></script>

@stop