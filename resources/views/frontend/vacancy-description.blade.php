@extends('frontend.master')
@section('content')
<style>
	/*@media only screen and (max-width: 767px){
				.section-1{
		    padding: 40px 20px;
		}
	}*/
	.heading{
		font-size: 20px;
		color: black;

	}
	.points-padding{
		padding-top: 450px;
		background-color: red;
	}
	.tag-line{
		font-family: "Times New Roman", Times, serif;
                font-size:      40px;
                font-weight:    bold;
	}
</style>

<section class="section-1" style="background-color: #fbfbfb; width: 100%; height: auto; margin-top: 64px; padding: 50px; border-bottom: 1px solid #000000;">
	<div class="container">
		<h2 style="text-align: center;">{{$details['job_title']}} ({{$details['job_experience']}})</h2>
	</div>
	<!-- <div class="container" style="background-color: #FFFFFF;">
		<div>
			<h2 style="margin: 80px 20px 20px 10px; text-align: center; color: black;">Together At Eigerlab Technologies</h2>
		<p class="para-1">We are a team that practices design-led engineering, innovates iteratively, and builds delightful<br>
products with the potential to create a positive impact. Sounds like a team you want to join?</p>
		</div>
	</div> -->
	
</section>
	
           <div class="container career_description" style="padding-top: 45px;">
					<div class="career_descriptionCon">
					<div class="row clearfix" style="margin-left: -15px; margin-right: -15px;">
					 <div class="col-sm-6"><h3>Description</h3></div>
					 <!-- <div class="col-sm-6">
						<div class="socialmediashare" style="text-align: right; font-size: 30px;">
						 <span class="share">Share on</span>
							<a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=/career/job_openings?type%3D1" target="_blank"><i class="fa fa-linkedin"></i></a>
							<a class="twitter" href="http://www.twitter.com/share?url=/career/job_openings?type=1" target="_blank"><i class="fa fa-twitter"></i></a>
							<a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=/career/job_openings?type=1" target="_blank"><i class="fa fa-facebook"></i></a>
						 </div>
					 </div> -->
					 <div class="col-sm-6" style="text-align: right; font-size: 20px;">
					 	<span class="share">Share on</span>
            <ul class="social-network social-circle">
              <li><a href="https://in.linkedin.com/company/eigerlabtech" class="icoLinkedin" title="LinkedIn" target="_blank" data-pjax-state=""><i class="fa fa-linkedin"></i></a></li>
              <!-- <li><a href="https://twitter.com/eigerlabtech" class="icoTwitter" title="Twitter" target="_blank" data-pjax-state=""><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://www.facebook.com/Eigerlabtech/" class="icoFacebook" title="Facebook" target="_blank" data-pjax-state=""><i class="fa fa-facebook"></i></a></li> -->
            </ul>
          </div>
					</div>
					<p>{{ $details['description']}}</p>					
					<h5 class="heading">Skills</h5>
					<div style="padding-left: 30px;">
						<ul class="ul--dotted ul--alter retreat--0 margin-lg-30t margin-md-20t">
							<?php
                        for($i=0;$i<count($details['skills']);$i++){
                          ?>
				   <li>{{$details['skills'][$i]}}.</li>
				   <?php }?>
               <!--  <li>Experience with iOS frameworks such as Core Data, Core Animation, etc.</li>
                <li>Experience with offline storage, threading, and performance tuning</li>
                <li>Familiarity with RESTful APIs to connect iOS applications to back-end services</li>
                <li>Knowledge of other web technologies and UI/UX standards</li>
                <li>Understanding of Apple’s design principles and interface guidelines</li>
                <li>Knowledge of low-level C-based libraries is preferred</li>
                <li>Experience with performance and memory tuning with tools</li>
                <li>Familiarity with cloud message APIs and push notifications</li>
                <li>Knack for benchmarking and optimization</li>
                <li>Proficient understanding of code versioning tools</li>
                <li>Familiarity with continuous integration</li> -->
				   </ul>
					</div>
				   
				   <h5 class="heading">Responsibilities</h5>
				   <div style="padding-left: 30px;">
				   	<ul class="ul--dotted ul--alter retreat--0 margin-lg-30t margin-md-20t" style="padding-left: 25px;">
				   <?php
                        for($i=0;$i<count($details['responsibilities']);$i++){
                          ?>
				   <li>{{$details['responsibilities'][$i]}}.</li>
				   <?php }?>
                  <!--  <li>Ensure the performance, quality, and responsiveness of applications</li>
                   <li>Collaborate with a team to define, design, and ship new features</li>
                   <li>Identify and correct bottlenecks and fix bugs</li>
                   <li>Help maintain code quality, organization, and automatization</li> -->
				   </ul>
				   </div>
					</div>
				  </div>

				  <!-- <hr style="border-bottom: 1px solid #000000;"> -->
				  <hr style="border-bottom-style: inset;border-bottom-width: 7px;">

				  <div class="container" style="background-color: yellow; width: 100%; height: auto; position: relative; margin-top: 100px; margin-bottom: 100px;">
				  	<!-- #20a8f2 -->
              <div class="tag-line col-md-12" style="position: relative; text-align: center;">Think you fit in?</div>
              <div class="row margin-lg-50t margin-lg-100b margin-md-80b margin-sm-50b margin-sm-30t">
      <div class="col-md-12">
        <!-- @if(Session::has('message'))
        <div class="col-sm-12">
          <div class="alert alert-success alert-dismissable">
            {{ session::get('message')}}
            <a class="close" data-dismiss="alert">&times;</a>
          </div>
        </div>
        @endif -->

        <div class="aheto-form aheto-form--default aheto-form--saas">
          <p id="head"></p>
          <form name="qform" class="wpcf7-form" method="post" action="{{URL::to('/submit-resume')}}" enctype="multipart/form-data">
            @csrf
            <p>
              <span class="wpcf7-form-control-wrap Email">
                <input id="email" name="email"  type="text" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your E-Mail Address*">
                <span id="spanEmail"></span>
              </span>
              <span class="wpcf7-form-control-wrap Name" style="background-color: white;">
                <input id="resume"  type="file" size="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"aria-required="true" aria-invalid="false" placeholder="Attach Your Resume*" name="resume">
                <span id="spanResume"></span>
              </span>
              <!-- <span class="wpcf7-form-control-wrap Phone">
                <input id="mobile" name="mobile"  type="text" required="mobile" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Your Mobile Number*">
                <span id="span3"></span>
              </span> -->
            </p>
            <p class="form-bth-holder">
              <input id="submit" type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit">
            </p>
          </form>
          <!-- <div id="succ_message"></div> -->
        </div>
      </div>
    </div>
</div>

<hr style="border-bottom-style: inset;border-bottom-width: 7px;">

<script>
    $(document).ready(function() {
      $('#submit').click(function(e){
        // Initializing Variables With Form Element Values

        // var name = $('#name').val();
        var email = $('#email').val();

        var email_regex =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        // To Check Empty Form Fields.
        // if (name.length == 0) {
        // $('#head').html('<span class="somethingwrong" style="color:red; font-size:12px;">* All fields are mandatory *</span>').show(); // This Segment Displays The Validation Rule For All Fields
        // setTimeout(function(){ $('#head').fadeOut() }, 5000);
        // $("#name").focus();
        // return false;
        // }

        // Validating Email Field.
        if (email.length == 0) {
        $('#spanEmail').html('<span class="blankemail" style="color:red; font-size:12px;">* Email is required *</span>').show(); // This Segment Displays The Validation Rule For Name
        setTimeout(function(){ $('#spanEmail').fadeOut() }, 5000);
        $("#email").focus();
        return false;
        }

        else if (!email.match(email_regex)) {
        $('#spanEmail').html('<span class="emailwrong" style="color:red; font-size:12px;">* Please enter a valid email address *</span>').show(); // This Segment Displays The Validation Rule For Email
        setTimeout(function(){ $('#spanEmail').fadeOut() }, 5000);
        $("#email").focus();
        return false;
        }

        else {
          // $("#succ_message").html('<div class="thankstext"><a>Thanks For Contacting!!!</div>').show();
          // setTimeout(function(){ $('#succ_message').fadeOut() }, 5000);
        alert("Thanks For Contacting with us!!!");
        return true;
        }

        });
      // body...
    });
  </script>
@stop