@extends('frontend.master')
@section('content')
<!-- <!DOCTYPE html>
<html> -->
<head>
    <meta charset="utf-8">
    <!-- <meta name="apple-mobile-web-app-capable" content="yes"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <!-- <link href="{{URL::to('public/css/portfolio/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css"> -->
   <!--  <script src="{{URL::to('public/js/portfolio/bootstrap.min.js')}}"></script>
    <script src="{{URL::to('public/js/portfolio/jquery.min.js')}}"></script> -->
    <style type="text/css">
        .how-section1{
    margin-top:-15%;
    padding: 10%;
}
.how-section1 h4{
    color: #ffa500;
    font-weight: bold;
    font-size: 30px;
}
.how-section1 .subheading{
    color: #3931af;
    font-size: 20px;
}
.how-section1 .row
{
    margin-top: 10%;
}
.how-img 
{
    /*position: relative;*/
    animation: 2s infinite ;
    text-align: right;
    animation-name: slidein;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    position: relative;
}

@keyframes slidein {
  /*from {
    margin-left: 50%;
    width: 300%;
    left: 200px;
  }

  to {
    margin-left: 80%;
    width: 100%;
    right: 0px;
  }*/
  /*0% { transform: translateX(0); }
    100% { transform: translateX(-400px); }*/
    0% { transform: translateX(0); }
    100% { transform: translateX(-365px); }
}
/* Responsive layout - makes a two column-layout instead of four columns */
@media screen and (max-width: 800px) {
  .how-img {
    -ms-flex: 50%;
    flex: 50%;
    max-width: 50%;
  }
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .how-img {
    -ms-flex: 100%;
    flex: 100%;
    max-width: 100%;
  }
}
.how-img img{
    width: 40%;
}
    </style>
</head>
<!------ Include the above in your HEAD tag ---------->
<!-- <body> -->
    <!-- <br>
    <br> -->
    <!-- <div class="container"> -->
<div class="how-section1" style="background-image: url('public/img/sedna/hero.jpg');">
                    <div class="row" >
                        <div class="col-md-6 how-img">
                            <img src="{{URL::to('public/img/saas/mobile.png')}}" class="rounded-circle img-fluid" style=" border-radius: 0%; z-index: 4;  left: 50%; " alt=""/>
                        </div>
                        <div class="col-md-6">
                            <h4>Find rewarding projects</h4>
                                        <h4 class="subheading">GetLance is a great place to find more clients, and to run and grow your own freelance business.</h4>
                        <p class="text-muted">Freedom to work on ideal projects. On GetLance, you run your own business and choose your own clients and projects. Just complete your profile and we’ll highlight ideal jobs. Also search projects, and respond to client invitations.
                                            Wide variety and high pay. Clients are now posting jobs in hundreds of skill categories, paying top price for great work.
                                            More and more success. The greater the success you have on projects, the more likely you are to get hired by clients that use GetLance.</p>
                                            <!-- <p>Freedom to work on ideal projects. On GetLance, you run your own business and choose your own clients and projects. Just complete your profile and we’ll highlight ideal jobs. Also search projects, and respond to client invitations. Wide variety and high pay. Clients are now posting jobs in hundreds of skill categories, paying top price for great work. More and more success. The greater the success you have on projects, the more likely you are to get hired by clients that use GetLance.</p> -->
                                            <!-- <p>Freedom to work on ideal projects. On GetLance, you run your own business and choose your own clients and projects. Just complete your profile and we’ll highlight ideal jobs. Also search projects, and respond to client invitations. Wide variety and high pay. Clients are now posting jobs in hundreds of skill categories, paying top price for great work. More and more success. The greater the success you have on projects, the more likely you are to get hired by clients that use GetLance.</p>
                                            <p>Freedom to work on ideal projects. On GetLance, you run your own business and choose your own clients and projects. Just complete your profile and we’ll highlight ideal jobs. Also search projects, and respond to client invitations. Wide variety and high pay. Clients are now posting jobs in hundreds of skill categories, paying top price for great work. More and more success. The greater the success you have on projects, the more likely you are to get hired by clients that use GetLance.</p> -->
                        </div>
                    </div>
    <hr class="d-lg-block">
                    <div class="row container-fluid">
                        <div class="col-md-6">
                            <h4>Get hired quickly</h4>
                                        <h4 class="subheading">GetLance makes it easy to connect with clients and begin doing great work.</h4>
                                        <p class="text-muted">Streamlined hiring. GetLance’s sophisticated algorithms highlight projects you’re a great fit for.
                                            Top Rated and Rising Talent programs. Enjoy higher visibility with the added status of prestigious programs.
                                            Do substantial work with top clients. GetLance pricing encourages freelancers to use GetLance for repeat relationships with their clients.</p>
                        </div>
                        <div class="col-md-6 how-img">
                            <img src="{{URL::to('public/img/saas/mobile.png')}}" class="rounded-circle img-fluid" alt=""/>
                        </div>
                    </div>
    <hr class="d-lg-block">
                    <div class="row">
                        <div class="col-md-6 how-img">
                             <img src="{{URL::to('public/img/saas/mobile.png')}}" class="rounded-circle img-fluid" alt=""/>
                        </div>
                        <div class="col-md-6">
                            <h4>Work efficiently, effectively.</h4>
                                        <h4 class="subheading">With GetLance, you have the freedom and flexibility to control when, where, and how you work. Each project includes an online workspace shared by you and your client, allowing you to:</h4>
                                        <p class="text-muted">Send and receive files. Deliver digital assets in a secure environment.
                                            Share feedback in real time. Use GetLance Messages to communicate via text, chat, or video.
                                            Use our mobile app. Many features can be accessed on your mobile phone when on the go.</p>
                        </div>
                    </div>
    <hr class="d-lg-block">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Get paid on time</h4>
                                        <h4 class="subheading">All projects include GetLance Payment Protection — helping ensure that you get paid for all work successfully completed through the freelancing website.</h4>
                                        <p class="text-muted">All invoices and payments happen through GetLance. Count on a simple and streamlined process.
                                            Hourly and fixed-price projects. For hourly work, submit timesheets through GetLance. For fixed-price jobs, set milestones and funds are released via GetLance escrow features.
                                            Multiple payment options. Choose a payment method that works best for you, from direct deposit or PayPal to wire transfer and more.</p>
                        </div>
                        <div class="col-md-6 how-img">
                            <img src="{{URL::to('public/img/saas/mobile.png')}}" class="rounded-circle img-fluid" alt=""/>
                        </div>
                    </div>
                <!--     <hr> -->
    <!-- <hr class="d-lg-block"> -->

                </div>
            <!-- </div> -->
                <!-- </body>
                </html> -->
                @stop