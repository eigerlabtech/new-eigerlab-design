<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Eigerlab Technologies | Blog</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{url('public/img/eigerlab-32x32.png')}}" rel="icon">
  <link href="{{URL::to('public/flex-start-blog/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">
  <link href="{{URL::to('public/flex-start-blog/assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{URL::to('public/flex-start-blog/assets/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: FlexStart - v1.0.0
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

  <link href="{{URL::to('public/css/navigationbarcss/home-page.css')}}" rel="stylesheet" type="text/css">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="{{URL::to('/')}}" class="logo d-flex align-items-center">
        <img src="{{URL::to('public/flex-start-blog/assets/img/logo.png')}}" alt="">
        <span>Eigerlab Technologies</span>
      </a>

      <nav id="navbar" class="navbar">
        <ul></ul>
          {{-- </li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="getstarted scrollto" href="#about">Get Started</a></li> --}}
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        {{-- <ol>
          <li><a href="{{URL::to('/')}}">Home</a></li>
          <li>Blog</li>
        </ol> --}}
        <h2>Blog</h2>

      </div>
    </section>
    <!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-8 entries">
          <div class="all-blogs">
         {{-- all blogs place here by ajax call --}}
          </div>
          {{-- pagination --}}
            <div class="blog-pagination">
              <ul class="justify-content-center">
                <li><a href="javascript:void(0);" onclick="Pagination(2)" id="prev">1</a></li>
                <li class="active"><a href="javascript:void(0);" id="curr">2</a></li>
                <li><a href="javascript:void(0);" id="next">3</a></li>
              </ul>
            </div>

          </div><!-- End blog entries list -->

          <div class="col-lg-4">

            <div class="sidebar">

              <h3 class="sidebar-title">Search</h3>
              <div class="sidebar-item search-form">
                <form action="">
                  <input type="text">
                  <button type="submit"><i class="bi bi-search"></i></button>
                </form>
              </div><!-- End sidebar search formn-->

              <h3 class="sidebar-title">Categories</h3>
              <div class="sidebar-item categories blog-category">
                <ul>
                  <?php for($i=0;$i< count($category);$i++){?>
                  <li><a href="#">{{$category[$i]['category_name']}}<span></span></a></li>
                  <?php }?>
                </ul>
              </div><!-- End sidebar categories-->

              <h3 class="sidebar-title">Our most popular blogs</h3>
              <div class="sidebar-item recent-posts">
                <?php for($k=1;$k < count($randomBlog);$k++){?>
                <div class="post-item clearfix">
                  <img src="{{$randomBlog[$k]['blog_image']}}" alt="">
                  <h4><a href="blog-single.html">{{$randomBlog[$k]['blog_title']}}</a></h4>
                  <time datetime="2020-01-01">{{$randomBlog[$k]['created_at']}}</time>
                </div>
               <?php }?>

              </div><!-- End sidebar recent posts-->

              <h3 class="sidebar-title">Tags</h3>
              <div class="sidebar-item tags">
                <ul>
                  <?php for($i=0;$i< count($category);$i++){?>
                  <li><a href="#">{{$category[$i]['category_name']}}</a></li>
                  <?php }?>
                </ul>
              </div><!-- End sidebar tags-->

            </div><!-- End sidebar -->

          </div><!-- End blog sidebar -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->


  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/aos/aos.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/purecounter/purecounter.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{URL::to('public/flex-start-blog/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{URL::to('public/flex-start-blog/assets/js/main.js')}}"></script>



  <script src="{{URL::to('public/js/jquery-2.1.1.js')}}"></script>
  <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script> -->
  <script>
    // get blog-categories data 

        var blog_category = [];

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/blog-category',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                // var str = res.category_name;
                // var category_name = str.replace(/\s+/g, '-').toLowerCase();
                var url = "{{URL::to('/blog-category')}}" +'/' + res.id;
                
                blog_category.push('<ul><li><a href="javascript:void(0);" onclick = "categoryData(this.id)" id="'+res.id+'">'+ res.category_name +'<span></span></a></li></ul>');
                // <ul>
                //   <li><a href="#">General <span>(25)</span></a></li>
                })
                $('.blog-category').html(blog_category);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });

        // get all blogs 

        var blogs = [];
        $('#curr').text(0);
        $('#next').text(1);
        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")."/blogs/0"}}',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
            //   console.log(response);
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                let title = res.blog_title;
                title = title.replace(/[^a-zA-Z ]/g, "")
                var url = "{{URL::to('/blog')}}" +"/"+ title+"_"+res.id;
                // console.log(url);
                // blog_category.push('<ul><li><a href="'+ url +'">'+ res.category_name +'<span>'+(25)+'</span></a></li></ul>');

                blogs.push('<article class="entry"><div class="entry-img"><img src="'+ res.blog_image +'" alt="" class="img-fluid" style="max-width: 100%;"></div><h2 class="entry-title"><a href="'+url+'">'+ res.blog_title +'</a></h2><div class="entry-meta"><ul><li class="d-flex align-items-center"><i class="bi bi-person"></i><a href="#">'+ res.blogger_name +'</a></li><li class="d-flex align-items-center"><i class="bi bi-clock"></i><a href="#"><time datetime="2020-01-01">'+ res.created_at +'</time></a></li></ul></div><div class="entry-content"><p>"'+ res.blog_description +'"</p><div class="read-more"><a href="'+url+'">Read More</a></div></div></article>');
                })
                $('.all-blogs').html(blogs);
                 $('#prev').css('display',"none");
                $('#curr').attr('onclick',"Pagination(0)");
        
                $('#next').attr('onclick',"Pagination(1)");
            },
            error : function(request, status, err){
                console.log(err)
            }
        });

        function Pagination(pageNo){
          $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")."/blogs/"}}'+pageNo,
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              console.log(response);
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                var url = "{{URL::to('/blogs')}}" +'/' + res.id;
                // console.log(url);
                // blog_category.push('<ul><li><a href="'+ url +'">'+ res.category_name +'<span>'+(25)+'</span></a></li></ul>');

                blogs.push('<article class="entry"><div class="entry-img"><img src="'+ res.blog_image +'" alt="" class="img-fluid" style="max-width: 100%;"></div><h2 class="entry-title"><a href="#">'+ res.blog_title +'</a></h2><div class="entry-meta"><ul><li class="d-flex align-items-center"><i class="bi bi-person"></i><a href="#">'+ res.blogger_name +'</a></li><li class="d-flex align-items-center"><i class="bi bi-clock"></i><a href="#"><time datetime="2020-01-01">'+ res.created_at +'</time></a></li></ul></div><div class="entry-content"><p>"'+ res.blog_description +'"</p></div></article>');
                })
                $('#prev').css('display',"none");
                $('.all-blogs').html(blogs);
                $('#curr').text(pageNo);
                $('#next').text(pageNo+1);
                $('#prev').text(pageNo-1);

                $('#prev').attr('onclick',"Pagination("+(pageNo-1)+")");
                $('#curr').attr('onclick',"Pagination("+pageNo+")");
                $('#next').attr('onclick',"Pagination("+(pageNo+1)+")");
            },
            error : function(request, status, err){
                console.log(err)
            }
        });
        }

        function categoryData(categoryId){
            // console.log(categoryId)
            var url = '{{env("LARAVEL_API_BASE_PATH")."/blog/"}}'+categoryId ;
            
            var blogs_of_categoryId = [];
          $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")."/blog/"}}'+categoryId,
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              console.log(response);
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                let title = res.blog_title;
                title = title.replace(/[^a-zA-Z ]/g, "")
                var url = "{{URL::to('/blog')}}" +"/"+ title+"_"+res.id;
                //   console.log(response);
                // var url = "{{URL::to('/blogs')}}" +'/' + res.id;
                // console.log(url);
                // blog_category.push('<ul><li><a href="'+ url +'">'+ res.category_name +'<span>'+(25)+'</span></a></li></ul>');
                // $('.all-blogs').remove();
                // $('#all-blog').addClass('all-blogs');

                // blogs_of_categoryId.push('<article class="entry"><div class="entry-img"><img src="'+ res.blog_image +'" alt="" class="img-fluid" style="max-width: 100%;"></div><h2 class="entry-title"><a href="#">'+ res.blog_title +'</a></h2><div class="entry-meta"><ul><li class="d-flex align-items-center"><i class="bi bi-person"></i><a href="#">'+ res.blogger_name +'</a></li><li class="d-flex align-items-center"><i class="bi bi-clock"></i><a href="#"><time datetime="2020-01-01">'+ res.created_at +'</time></a></li></ul></div><div class="entry-content"><p>"'+ res.blog_description +'"</p></div></article>');

                blogs_of_categoryId.push('<article class="entry"><div class="entry-img"><img src="'+ res.blog_image +'" alt="" class="img-fluid" style="max-width: 100%;"></div><h2 class="entry-title"><a href="'+url+'">'+ res.blog_title +'</a></h2><div class="entry-meta"><ul><li class="d-flex align-items-center"><i class="bi bi-person"></i><a href="#">'+ res.blogger_name +'</a></li><li class="d-flex align-items-center"><i class="bi bi-clock"></i><a href="#"><time datetime="2020-01-01">'+ res.created_at +'</time></a></li></ul></div><div class="entry-content"><p>"'+ res.blog_description +'"</p><div class="read-more"><a href="'+url+'">Read More</a></div></div></article>');

                })
                $('#prev').css('display',"none");
                $('.all-blogs').html(blogs_of_categoryId);
                $('#curr').text(pageNo);
                $('#next').text(pageNo+1);
                $('#prev').text(pageNo-1);

                $('#prev').attr('onclick',"Pagination("+(pageNo-1)+")");
                $('#curr').attr('onclick',"Pagination("+pageNo+")");
                $('#next').attr('onclick',"Pagination("+(pageNo+1)+")");
            },
            error : function(request, status, err){
                console.log(err);
            }
        });
        }
  </script>
</body>

</html>