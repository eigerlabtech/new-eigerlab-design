<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Blog - FlexStart Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <link  rel="icon" href="{{url('public/img/eigerlab-32x32.png')}}" sizes="32x32" />

  <link rel="stylesheet" id="style-main" href="{{url('public/css/styles-main.css?v=3')}}">
  <link rel="stylesheet" id="swiper-main" href="{{url('public/vendors/swiper/swiper.min.css')}}">
  <link rel="stylesheet" id="lity-main" href="{{url('public/vendors/lity/lity.min.css')}}">
  <link rel="stylesheet" id="mediaelementplayer" href="{{url('public/vendors/mediaelement/mediaelementplayer.min.css')}}">
  <link rel="stylesheet" id="range" href="{{url('public/vendors/range/jquery.range.css')}}">
  <link rel="stylesheet" id="lightgallery" href="{{url('public/vendors/lightgallery/lightgallery.min.css')}}">
  <link rel="stylesheet" id="style-link" href="{{url('public/css/styles-9.css?v=35')}}">

  <link rel="stylesheet" id="style-main-1" href="{{url('public/css/new-style.css')}}">
  <link rel="stylesheet" id="style-main" href="{{url('public/css/new-style-1.css')}}">
  <link href="{{URL::to('public/css/navigationbarcss/home-page.css')}}" rel="stylesheet" type="text/css">

  <!-- PagePiling -->
  <link rel="stylesheet" type="text/css" href="{{URL::to('public/css/pagepiling/jquery.pagepiling.css')}}">

  <!-- Swiper -->
  <script src="{{url('public/vendors/swiper/swiper.min.js')}}"></script>
  <!-- Isotope -->
  <script src="{{url('public/vendors/isotope/isotope.pkgd.min.js')}}"></script>
  <!-- Images loaded library -->
  <script src="{{url('public/vendors/lazyload/imagesloaded.pkgd.min.js')}}"></script>
  <!-- MediaElement js library (only for Aheto HTML) -->
  <script src="{{url('public/vendors/mediaelement/mediaelement.min.js')}}"></script>
  <script src="{{url('public/vendors/mediaelement/mediaelement-and-player.min.js')}}"></script>
  <!-- Typed animation text -->
  <script src="{{url('public/vendors/typed/typed.min.js')}}"></script>
  <!-- Lity Lightbox -->
  <script src="{{url('public/vendors/lity/lity.min.js')}}"></script>

  <!--   Techugo links-->
  <link rel="icon" href="{{url('public/img/favicon-180x180.png')}}"  />
  <link rel="shortcut icon" href="{{url('public/img/favicon-180x180.png')}}"  />
  <!-- <link href="https:///assets/css/main.css" rel="stylesheet" type="text/css"> -->
  <link rel="stylesheet" id="style-main" href="{{url('public/css/new-style-1.css')}}">

  <link href="{{URL::to('public/css/navigationbarcss/home-page.css')}}" rel="stylesheet" type="text/css">

  <!-- <script src="https://www.techugo.com/assets/js/jquery-2.1.1.js"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script> -->
  <script src="{{URL::to('public/js/jquery-2.1.1.js')}}"></script>
  <script src="{{URL::to('public/js/owl.carousel.min.js')}}"></script>
  <script src="{{URL::to('public/js/script.js')}}"></script>

  <style type="text/css">
    /*.sec { 
  background: url('public/img/car.jpg') no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  
}*/
  .section-heading{

    text-align: center;
    font-size: 40px;
    font-style: italic;
  }
  .navigation-bar{
    /*color: dodgerblue !important;*/
    background-color: transparent;
  }
  .colum{
    position: absolute; 
    bottom: 0px;
  }

  /*.applogorw{
    
    background:#063;
    padding-top: 30px;
  }*/

  /*@media (min-width:768px) {
  .container {
    width: 750px
  }
}

@media (min-width:992px) {
  .container {
    width: 970px
  }
}

@media (min-width:1200px) {
  .container {
    width: 1170px
  }
}*/
  </style>
  <style>
* {
  box-sizing: border-box;
}

/* Create three equal columns that floats next to each other */
.column {
  float: left;
  /*width: 33.33%;*/
  padding: 10px;
  height: 100px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>

</head>
	<body>
    <nav class="navbar navbar-default navbar-fixed-top nav-down navigation-bar" width="100%" style=" ">
   <div class="container-fluid " style="color: white;">
      <div class="navbar-header">
         <div class="button_container" id="toggle">
          <span class="top"></span>
          <span class="middle"></span>
          <span class="bottom"></span>
        </div>
         <!-- <a class="navbar-brand" href="{{URL::to('/')}}">Eigerlab Technologies</a>  -->
         <div class="logo">
              <a class="logo__link" href="{{URL::to('/')}}">
                <div class="logo__img-holder" >
                  <img class="logo__img" style="border-radius: 50%;" src="{{url('public/img/home/home-saas/eigerlab-36x36.png')}}" alt="Logo image">
                </div>
                <div class="logo__text-holder">
                  <h3 class="logo__text" style="text-align: center;">Eigerlab Technologies</h3>
                </div>
              </a>
            </div>
      </div>
     <!--  <?php
      // $fileData = file_get_contents('http://localhost/laravelwebsite/api/services');
      // $fileData = json_decode($fileData);
      //   dd($fileData);
      ?> -->
      <div class="getconbtnbx">
         
         <a href="{{URL::to('/contact')}}">
            <div class="button raised hoverable">
               <div class="anim"></div>
               <span>Contact Us!</span> 
            </div>
         </a>
      </div>
      <div id="navbar" class="navbar-collapse collapse js-navbar-collapse">
         <ul class="nav navbar-nav">
            <!-- <li class="hideview"><a href="{{URL::to('/contact')}}">Contact Us</a></li> -->
            <li class="fs28wd"><a href="{{URL::to('/about')}}">About Us</a></li>
            <li><a href="{{URL::to('/services')}}">Services Demo</a></li>

            <li class="dropdown mega-dropdown fs28wd">
               <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Services </a> <!--Services Menu Start Here--> 
               <ul class="dropdown-menu mega-dropdown-menu">
                  <div class="servicemenuwrap">
                     <div class="menuwrap">
                        <div class="row clearfix services-cols">
                           
                        </div>
                         </div>
                  </div>
               </ul>
             </li>

            <li class="dropdown mega-dropdown fs28wd">
               <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Technologies </a> <!--Services Menu Start Here--> 
               <ul class="dropdown-menu mega-dropdown-menu">
                  <div class="servicemenuwrap">
                     <div class="menuwrap">
                        <div class="row clearfix techonologies-cols">
                          
                        </div>
                     </div>
                  </div>
               </ul>
               <!--Services Menu End Here--> 
            </li>
            <!-- <li><a href="#">Technologies</a></li> -->
            <li><a href="{{URL::to('/portfolio')}}">Portfolio</a></li>
            <li><a href="{{URL::to('/blog')}}">Blog</a></li>
         </ul>
      </div>
   </div>
   <!-- <div class="overlaybg"></div> -->
   <!--Mobile Navigation Start Here-->
   <div class="overlay" id="overlay">
      <nav class="overlay-menu">
         <ul>
            <li><a href="{{URL::to('/contact')}}">Contact Us</a></li>
            <li><a href="{{URL::to('/about')}}">About Us</a></li>
            <li class="viewservicemenu">
               <a href="javascript:void(0);">Services <span class="fa fa-angle-down"></span></a> 
               <div class="servicemenuwrap">
                  <div class="menuwrap">
                     <ul>
                        <li><a href="#"><span class="icon iosdev"></span><span class="technm">iOS <i>Development</i></span></a></li>
                        <li><a href="#"><span class="icon androiddev"></span><span class="technm">Android <i>Development</i></span></a></li>
                        <li><a href="#"><span class="icon phpnodedev"></span><span class="technm">PHP/Node <i>JS Development</i></span></a></li>
                        <li><a href="#"><span class="icon reactdev"></span><span class="technm">React <i>Development</i></span></a></li>
                     </ul>
                  </div>
               </div>
            </li>
            <li style="clear:both"></li>
            <li><a href="#">Features</a></li>
            <!-- <li><a href="#">Blog</a></li> -->
            <li style="clear:both"></li>
            <li class="viewservicemenu">
               <a href="javascript:void(0);">Blog <span class="fa fa-angle-down"></span></a> 
               <div class="servicemenuwrap">
                  <div class="menuwrap">
                     <ul>
                        <li><a href="{{URL::to('/blog')}}"><span class="icon iosdev"></span><span class="technm">Blog <i></i></span></a></li>
                        <li><a href="{{URL::to('/blog')}}"><span class="icon androiddev"></span><span class="technm">Post <i></i></span></a></li>
                     </ul>
                  </div>
               </div>
            </li>
            <li><a href="{{URL::to('/pricing')}}">Pricing</a></li>
            <li><a href="{{URL::to('/contact')}}">Contact Us</a></li>

         </ul>
      </nav>
   </div>
   <!--Mobile Navigation End Here--> 
  </nav>
  <!-- <div class="container"> -->
		<div id="pagepiling">
		  
      <div class="section sec " style="background-color: red;">
        <!-- <div class="sectionheading">Powering World's Smartest Indoor Garden</div>
                <div class="portbtmbx clearfix" style="bottom: 0px;">
                    <div class="applogorw"><img class="ava" src="/images/portfolio/ava_logo.png" width=
                    "94" height="55" alt="AVA Logo" /></div>
                    <div class="appnamebx">
                        <div class="appname">AVA Smart Garden</div>
                        <div class="applinksrw">
                            <a href="#" target="_blank"><i class="fa fa-apple" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" data-toggle="modal" data-target=""><span class="youtube"></span></a>
                        </div>
                    </div>
                </div> -->
                 <div class="section-heading">1'st App Title</div>
      </div>
      <div class="section sec1" style="background-color: yellow;">
        <div class="section-heading">2'nd App Title</div>
      </div>
      <div class="section sec1" style="background-color: pink;">
        <div class="section-heading">3'rd App Title</div>
      </div>
      <div class="section sec1" style="background-color: blue;">
        <div class="section-heading">4'th App Title</div>
      </div>
      <div class="section sec1" style="background-color: red;">
        <div class="section-heading">5'th App Title</div>
      </div>
      <div class="section sec1" style="background-color: yellow;">
        <div class="section-heading">6'th App Title</div>
      </div>
      <div class="section sec1" style="background-color: pink;">
        <div class="section-heading">7'th App Title</div>
      </div>
      <div class="section sec1" style="background-color: blue;">
        <div class="section-heading">8'th App Title</div>
      </div>
		 
		</div>
<!--   </div> -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="{{URL::to('public/js/pagepiling/jquery.pagepiling.js')}}"></script>
		<script type="text/javascript">
			$(document).ready(function() {
	  		$('#pagepiling').pagepiling();
			});
		</script>
    <script>
    // get services data 

        var serviceArray = [];
        var technologyArray = [];

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/services',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                var url = "{{URL::to('/services')}}" + res.url;
                // console.log(url);
                serviceArray.push('<div class="col-sm-4"><ul><li><a href="'+ url +'"><span class="icon blcdev"><img src="'+ res.icon +'" class="img-icon"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
                })
                $('.services-cols').html(serviceArray);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });

        // get technologies data

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/technologies',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
                // response.map(res => {
                //     technologyArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
                // })
                response.map(res => {
                    var url = "{{URL::to('/technology')}}" + res.url;
                    technologyArray.push('<div class="col-sm-4"><ul><li><a href="'+ url +'"><span class="icon blcdev"><img src="'+ res.icon +'" class="img-icon"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
                })
                $('.techonologies-cols').html(technologyArray);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });
  </script>
	</body>
</html>