-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2021 at 09:30 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelwebsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_category_id` int(11) DEFAULT NULL,
  `blog_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blogger_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `blog_category_id`, `blog_title`, `blog_image`, `blog_description`, `blogger_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Android Dev Journey', 'blogs\\February2021\\X6bpLOtBWkkH4wTuEIh8.jpg', 'We kicked off the #AndroidDevJourney to give members of our community the opportunity to share their stories through our social platforms. Each Saturday from January through June we’ll feature a new developer on our Twitter account. We have received an overwhelming number of inspirational stories and hope you enjoy reading through the ones we’ve selected below.', 'Walmyr Carvalho', 1, '2021-02-06 08:32:05', '2021-02-06 08:32:05'),
(2, 2, 'Why Does Incentivized Advertising Increase Product Awareness?', 'blogs\\February2021\\OqzO98jek4MhosX8JzyD.jpg', 'There are countless ways to get users used to a new app. Incentive ad types are among the most effective in this regard. Using influencers to reach new users on social media; Types such as advertising through Google can also be quite effective. Award-winning advertisements are among the most prominent ad types in increasing product and brand awareness. Thanks to these ads, you reach new users. It is effortless to reach new users with a third-party mobile app that will increase your brand awareness. Companies like App Samurai, your access to a new audience of potential customers can also be beneficial.', 'Andrew', 1, '2021-02-06 09:12:00', '2021-02-06 09:12:00'),
(3, 4, 'How Artificial Intelligence Increases The Capability Of Android Apps?', 'blogs\\February2021\\alLoPAGDwIP7DY2VzwRz.png', 'Artificial intelligence is the most sought-after subject when it comes to innovation and research. AI is leaving its mark on almost all of the areas of concern like finance, medical, businesses and many more. Mobile app development is not an exception. Mobile app development has mushroomed over the past few years. It is gaining momentum rapidly and businesses today are looking forward to harness the power of mobile devices. Android smartphones have proved to be a milestone in the evolution of mankind.\r\nTaking a giant leap toward the future technology, Google acquired DeepMind, a well known artificial intelligence startup in 2014. The main motive behind this deal was to perform the artificial intelligence research. The results are overwhelming and we can witness this impact in android applications.', 'Sylvain', 1, '2021-02-06 09:14:29', '2021-02-06 09:14:29'),
(4, 6, 'How to Develop an App with Flutter', 'blogs\\February2021\\i18TMK4iDJUDDOXKL4go.png', 'Flutter, Flutter, Flutter. We can find this word all over the internet, somewhere in Twitter feed or recommended videos on Youtube. Flutter is a great tool from Google for creating cross-platform applications which – starting from the newest stable version – can be deployed to the web, desktop, and mobile.\r\nGoogle is promoting Flutter as a simple framework that allows us to create quality maintainable solutions. Easy, it is just the next cross-platform framework.\r\nFlutter launched as a project called Sky which at the beginning worked only on Android. Flutter’s goal is enabling developers to compile for every platform using its own graphic layer rendered by the Skia engine. Here’s a brief presentation of Flutter’s relatively short history.', 'Alexander Stolar', 1, '2021-02-06 09:19:02', '2021-02-06 09:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`id`, `category_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Android', 1, '2021-02-06 07:12:44', '2021-02-06 07:12:44'),
(2, 'App Marketing', 1, '2021-02-06 07:12:56', '2021-02-06 07:12:56'),
(3, 'App Trends', 1, '2021-02-06 07:13:13', '2021-02-06 07:13:13'),
(4, 'Artificial Intelligence', 1, '2021-02-06 07:14:36', '2021-02-06 07:14:36'),
(5, 'E-Commerce', 1, '2021-02-06 07:16:39', '2021-02-06 07:16:39'),
(6, 'Flutter', 1, '2021-02-06 07:16:48', '2021-02-06 07:16:48'),
(7, 'Hire Developer', 1, '2021-02-06 07:17:15', '2021-02-06 07:17:15'),
(9, 'Mobile App Development', 1, '2021-02-06 07:18:02', '2021-02-06 07:18:02'),
(10, 'Mobile App Testing', 1, '2021-02-06 07:18:13', '2021-02-06 07:18:13'),
(11, 'React Native', 1, '2021-02-06 07:18:34', '2021-02-06 07:18:34');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(10) UNSIGNED NOT NULL,
  `job_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_experience` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skills` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsibilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`id`, `job_title`, `job_experience`, `description`, `skills`, `responsibilities`, `job_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'iOS Developer', 'Exp: 2 to 4 years', 'We are looking for an iOS developer responsible for the development and maintenance of applications aimed at a range of iOS devices including mobile phones and tablet computers. Your primary focus will be development of iOS applications and their integration with back-end services. You will be working alongside other engineers and developers working on different layers of the infrastructure. Therefore, a commitment to collaborative problem solving, sophisticated design, and the creation of quality products is essential.', 'Proficient with Objective-C or Swift. Experience with iOS frameworks such as Core Data, Core Animation, etc. Experience with offline storage, threading, and performance tuning. Familiarity with RESTful APIs to connect iOS applications to back-end services. Knowledge of other web technologies and UI/UX standards. Understanding of Apple’s design principles and interface guidelines. Knowledge of low-level C-based libraries is preferred. Experience with performance and memory tuning with tools. Familiarity with cloud message APIs and push notifications. Knack for benchmarking and optimization. Proficient understanding of code versioning tools. Familiarity with continuous integration', 'Design and build applications for the iOS platform. Ensure the performance, quality, and responsiveness of applications. Collaborate with a team to define, design, and ship new features. Identify and correct bottlenecks and fix bugs. Help maintain code quality, organization, and automatization', '/job_openings?type=1', 1, '2021-02-01 07:41:04', '2021-02-02 08:17:27'),
(2, 'Android Developer', 'Exp: 2 to 4 years', NULL, NULL, NULL, '/job_openings?type=2', 1, '2021-02-01 07:42:03', '2021-02-01 08:31:56'),
(3, 'Sr. Android Developer', 'Exp: 4 to 6 years', NULL, NULL, NULL, '/job_openings?type=3', 1, '2021-02-01 07:42:34', '2021-02-01 08:32:06'),
(4, 'React Native Developer', 'Exp: 2 to 4 years', NULL, NULL, NULL, '/job_openings?type=4', 1, '2021-02-01 07:42:53', '2021-02-01 08:32:19'),
(5, 'Node.js developer', 'Exp: 2 to 4 years', NULL, NULL, NULL, '/job_openings?type=5', 1, '2021-02-01 07:43:15', '2021-02-01 08:32:30'),
(6, 'PHP Developer', 'Exp: 1 to 3 years', NULL, NULL, NULL, '/job_openings?type=6', 1, '2021-02-01 07:43:50', '2021-02-01 08:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2020-12-22 01:08:37', '2020-12-22 01:08:37'),
(2, NULL, 1, 'Category 2', 'category-2', '2020-12-22 01:08:37', '2020-12-22 01:08:37');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `client_name`, `client_logo`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Client-1', 'client\\January2021\\ieD4tPQIQTAptabgvtUA.png', 1, '2021-01-27 08:18:00', '2021-01-27 08:56:37'),
(2, 'Client-2', 'client\\January2021\\X8VnsLFHXdUsiEwfNaPB.png', 1, '2021-01-27 08:18:57', '2021-01-27 08:18:57'),
(3, 'Client-3', 'client\\January2021\\p1cvkx4IP8GaAEvpCLJb.png', 0, '2021-01-27 08:19:00', '2021-01-27 08:56:56'),
(4, 'Client-4', 'client\\January2021\\9d4NN2JBlYF4X3QXpg9o.png', 1, '2021-01-27 08:19:30', '2021-01-27 08:19:30'),
(5, 'Client-5', 'client\\January2021\\2afhZgC2hd8H03YMqS9U.png', 1, '2021-01-27 08:19:50', '2021-01-27 08:19:50'),
(6, 'Client-6', 'client\\January2021\\Bio3CYs83oXF2lyWGwPS.png', 1, '2021-01-27 08:20:09', '2021-01-27 08:20:09');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'name', 'text', 'Name', 0, 1, 1, 0, 0, 1, '{}', 2),
(58, 7, 'email', 'text', 'Email', 0, 1, 1, 0, 0, 1, '{}', 3),
(59, 7, 'mobile', 'text', 'Mobile', 0, 1, 1, 0, 0, 1, '{}', 4),
(60, 7, 'message', 'text_area', 'Message', 0, 1, 1, 0, 0, 1, '{}', 5),
(61, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, '{}', 6),
(62, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(63, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(64, 8, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(65, 8, 'designation', 'text', 'Designation', 0, 1, 1, 1, 1, 1, '{}', 3),
(66, 8, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(67, 8, 'social_accounts', 'text', 'Social Accounts', 0, 1, 1, 1, 1, 1, '{}', 6),
(68, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, '{}', 8),
(69, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(70, 8, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"ON\",\"off\":\"OFF\"}', 7),
(71, 8, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 5),
(72, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(73, 10, 'client_name', 'text', 'Client Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(74, 10, 'client_designation', 'text', 'Client Designation', 0, 1, 1, 1, 1, 1, '{}', 3),
(75, 10, 'client_image', 'image', 'Client Image', 0, 1, 1, 1, 1, 1, '{}', 5),
(76, 10, 'client_review', 'text_area', 'Client Review', 0, 1, 1, 1, 1, 1, '{}', 6),
(77, 10, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"ON\",\"off\":\"OFF\"}', 7),
(78, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(79, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(80, 10, 'client_company', 'text', 'Client Company', 0, 1, 1, 1, 1, 1, '{}', 4),
(81, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(82, 11, 'service_title', 'text', 'Service Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(83, 11, 'service_image', 'image', 'Service Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(84, 11, 'service_description', 'text_area', 'Service Description', 0, 1, 1, 1, 1, 1, '{}', 5),
(85, 11, 'service_icon', 'image', 'Service Icon', 0, 1, 1, 1, 1, 1, '{}', 6),
(86, 11, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"no\":\"NO\",\"yes\":\"YES\"}', 8),
(87, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, '{}', 9),
(88, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(89, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(91, 12, 'technology_icon', 'image', 'Technology Icon', 0, 1, 1, 1, 1, 1, '{}', 4),
(92, 12, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"ON\",\"off\":\"OFF\"}', 16),
(93, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, '{}', 17),
(94, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 18),
(95, 12, 'technology_title', 'text', 'Technology Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(96, 11, 'service_banner', 'image', 'Service Banner', 0, 1, 1, 1, 1, 1, '{}', 3),
(97, 11, 'url', 'text', 'Url', 0, 1, 1, 1, 1, 1, '{}', 7),
(98, 11, 'section3_image', 'image', 'Section3 Image', 0, 1, 1, 1, 1, 1, '{}', 11),
(99, 11, 'section3_title', 'text', 'Section3 Title', 0, 1, 1, 1, 1, 1, '{}', 12),
(100, 11, 'section3_description', 'text_area', 'Section3 Description', 0, 1, 1, 1, 1, 1, '{}', 13),
(101, 11, 'section4_image', 'image', 'Section4 Image', 0, 1, 1, 1, 1, 1, '{}', 15),
(102, 11, 'section4_title', 'text', 'Section4 Title', 0, 1, 1, 1, 1, 1, '{}', 16),
(103, 11, 'section4_description', 'text_area', 'Section4 Description', 0, 1, 1, 1, 1, 1, '{}', 17),
(107, 11, 'section3_points', 'text_area', 'Section3 Points', 0, 1, 1, 1, 1, 1, '{}', 14),
(108, 11, 'section4_points', 'text', 'Section4 Points', 0, 1, 1, 1, 1, 1, '{}', 18),
(109, 12, 'url', 'text', 'Url', 0, 1, 1, 1, 1, 1, '{}', 3),
(110, 12, 'technology_banner', 'image', 'Technology Banner', 0, 1, 1, 1, 1, 1, '{}', 5),
(111, 12, 'technology_image', 'image', 'Technology Image', 0, 1, 1, 1, 1, 1, '{}', 6),
(112, 12, 'technology_description', 'text_area', 'Technology Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(113, 12, 'section3_image', 'image', 'Section3 Image', 0, 1, 1, 1, 1, 1, '{}', 8),
(114, 12, 'section3_title', 'text', 'Section3 Title', 0, 1, 1, 1, 1, 1, '{}', 9),
(115, 12, 'section3_description', 'text_area', 'Section3 Description', 0, 1, 1, 1, 1, 1, '{}', 10),
(116, 12, 'section3_points', 'text_area', 'Section3 Points', 0, 1, 1, 1, 1, 1, '{}', 11),
(117, 12, 'section4_image', 'image', 'Section4 Image', 0, 1, 1, 1, 1, 1, '{}', 12),
(118, 12, 'section4_title', 'text', 'Section4 Title', 0, 1, 1, 1, 1, 1, '{}', 13),
(119, 12, 'section4_description', 'text_area', 'Section4 Description', 0, 1, 1, 1, 1, 1, '{}', 14),
(120, 12, 'section4_points', 'text_area', 'Section4 Points', 0, 1, 1, 1, 1, 1, '{}', 15),
(121, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(122, 13, 'client_name', 'text', 'Client Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(123, 13, 'client_logo', 'image', 'Client Logo', 0, 1, 1, 1, 1, 1, '{}', 3),
(124, 13, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"Show\",\"off\":\"Hide\"}', 4),
(125, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(126, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(127, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(128, 14, 'job_title', 'text', 'Job Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(129, 14, 'job_experience', 'text', 'Job Experience', 0, 1, 1, 1, 1, 1, '{}', 3),
(130, 14, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 4),
(131, 14, 'skills', 'text_area', 'Skills', 0, 1, 1, 1, 1, 1, '{}', 5),
(132, 14, 'responsibilities', 'text_area', 'Responsibilities', 0, 1, 1, 1, 1, 1, '{}', 6),
(133, 14, 'job_url', 'text', 'Job Url', 0, 1, 1, 1, 1, 1, '{}', 7),
(134, 14, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"Show\",\"off\":\"Hide\"}', 8),
(135, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, '{}', 9),
(136, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(137, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(138, 15, 'candidate_email', 'text', 'Candidate Email', 0, 1, 1, 1, 1, 1, '{}', 2),
(139, 15, 'candidate_resume', 'file', 'Candidate Resume', 0, 1, 1, 1, 1, 1, '{}', 3),
(140, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(141, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(142, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(143, 16, 'section1_banner', 'image', 'Section1 Banner', 0, 1, 1, 1, 1, 1, '{}', 2),
(144, 16, 'section1_heading', 'text', 'Section1 Heading', 0, 1, 1, 1, 1, 1, '{}', 3),
(145, 16, 'section1_quote', 'text_area', 'Section1 Quote', 0, 1, 1, 1, 1, 1, '{}', 4),
(146, 16, 'section2_image', 'image', 'Section2 Image', 0, 1, 1, 1, 1, 1, '{}', 5),
(147, 16, 'section2_heading', 'text', 'Section2 Heading', 0, 1, 1, 1, 1, 1, '{}', 6),
(148, 16, 'section2_tagline', 'text', 'Section2 Tagline', 0, 1, 1, 1, 1, 1, '{}', 7),
(149, 16, 'section2_description', 'text_area', 'Section2 Description', 0, 1, 1, 1, 1, 1, '{}', 8),
(150, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, '{}', 12),
(151, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(152, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(158, 17, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"Show\",\"off\":\"Hide\"}', 7),
(159, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, '{}', 8),
(160, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(161, 17, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(162, 17, 'image_name', 'text', 'Image Name', 0, 1, 1, 1, 1, 1, '{}', 6),
(164, 16, 'section4_tagline', 'text', 'Section4 Tagline', 0, 1, 1, 1, 1, 1, '{}', 11),
(165, 16, 'section4_heading', 'text', 'Section4 Heading', 0, 1, 1, 1, 1, 1, '{}', 10),
(170, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(172, 18, 'section3_number', 'number', 'Section3 Number', 0, 1, 1, 1, 1, 1, '{}', 3),
(173, 18, 'section3_tagline', 'text', 'Section3 Tagline', 0, 1, 1, 1, 1, 1, '{}', 4),
(174, 18, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"Show\",\"off\":\"Hide\"}', 5),
(175, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(176, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(177, 16, 'section3_heading', 'text', 'Section3 Heading', 0, 1, 1, 1, 1, 1, '{}', 9),
(178, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(179, 19, 'category_name', 'text', 'Category Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(180, 19, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"Show\",\"off\":\"Hide\"}', 3),
(181, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, '{}', 4),
(182, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(183, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(184, 21, 'blog_category_id', 'select_dropdown', 'Blog Category Id', 0, 1, 1, 1, 1, 1, '{\"relationship\":{\"key\":\"id\",\"label\":\"category_name\"}}', 2),
(185, 21, 'blog_title', 'text', 'Blog Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(186, 21, 'blog_image', 'image', 'Blog Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(187, 21, 'blog_description', 'text_area', 'Blog Description', 0, 1, 1, 1, 1, 1, '{}', 5),
(188, 21, 'blogger_name', 'text', 'Blogger Name', 0, 1, 1, 1, 1, 1, '{}', 6),
(189, 21, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"on\":\"Show\",\"off\":\"Hide\"}', 7),
(190, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, '{}', 8),
(191, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-12-22 01:08:20', '2020-12-22 01:08:20'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-12-22 01:08:20', '2020-12-22 01:08:20'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-12-22 01:08:20', '2020-12-22 01:08:20'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2020-12-22 01:08:35', '2020-12-22 01:08:35'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2020-12-22 01:08:37', '2020-12-22 01:08:37'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2020-12-22 01:08:39', '2020-12-22 01:08:39'),
(7, 'query', 'query', 'Query', 'Queries', NULL, 'App\\Query', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-29 03:51:53', '2020-12-29 03:51:53'),
(8, 'team_member', 'team-member', 'Team Member', 'Team Members', NULL, 'App\\TeamMember', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-01-03 23:52:15', '2021-01-06 04:29:27'),
(10, 'testimonial', 'testimonial', 'Testimonial', 'Testimonials', NULL, 'App\\Testimonial', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-01-07 07:55:47', '2021-01-08 00:31:02'),
(11, 'service_types', 'service-types', 'Service Type', 'Service Types', NULL, 'App\\ServiceType', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-01-19 23:40:24', '2021-01-25 05:40:45'),
(12, 'technology_type', 'technology-type', 'Technology Type', 'Technology Types', NULL, 'App\\TechnologyType', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-01-20 05:19:43', '2021-01-27 00:16:28'),
(13, 'client', 'client', 'Client', 'Clients', NULL, 'App\\Client', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-01-27 08:14:55', '2021-01-27 08:14:55'),
(14, 'career', 'career', 'Career', 'Careers', NULL, 'App\\Career', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-02-01 07:34:11', '2021-02-01 07:34:11'),
(15, 'job_resumes', 'job-resumes', 'Job Resume', 'Job Resumes', NULL, 'App\\JobResume', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-02-03 08:42:50', '2021-02-03 08:45:09'),
(16, 'home_page', 'home-page', 'Home Page', 'Home Pages', NULL, 'App\\HomePage', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-02-03 12:48:14', '2021-02-04 05:06:45'),
(17, 'home_page_section4_images', 'home-page-section4-images', 'Home Page Section4 Image', 'Home Page Section4 Images', NULL, 'App\\HomePageSection4Image', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-02-04 00:38:14', '2021-02-04 02:07:40'),
(18, 'home_page_section3', 'home-page-section3', 'Home Page Section3', 'Home Page Section3s', NULL, 'App\\HomePageSection3', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-02-04 05:04:38', '2021-02-04 05:05:38'),
(19, 'blog_category', 'blog-category', 'Blog Category', 'Blog Categories', NULL, 'App\\BlogCategory', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-02-06 07:11:57', '2021-02-06 07:11:57'),
(21, 'blogs', 'blogs', 'Blog', 'Blogs', NULL, 'App\\Blog', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-02-06 08:20:45', '2021-02-06 08:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_page`
--

CREATE TABLE `home_page` (
  `id` int(10) UNSIGNED NOT NULL,
  `section1_banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section1_heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section1_quote` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section2_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section2_heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section2_tagline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section2_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_tagline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `section3_heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_page`
--

INSERT INTO `home_page` (`id`, `section1_banner`, `section1_heading`, `section1_quote`, `section2_image`, `section2_heading`, `section2_tagline`, `section2_description`, `section4_heading`, `section4_tagline`, `created_at`, `updated_at`, `section3_heading`) VALUES
(1, 'home-page\\February2021\\06J11tksIMeoqgi31thF.png', 'We Build Next Generation App Solutions.', 'EigerlabTech is a One Stop Technology Solutions For all Your App Development Needs.', 'home-page\\February2021\\aIWCooe0JDV4Ih8W1zTo.png', NULL, 'Somethings about server analysis services.', 'Thanks to the commonwealth of Virginia, no matter where you live in the U.S., your documents can now be notarized online by a trusted notary.', 'SCREENSHOTS New', 'Design and Develop Full-Stack Mobile Apps using Native or Cross Platform Frameworks.', '2021-02-03 12:51:33', '2021-02-04 05:09:41', 'We are proud of our work');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_section3`
--

CREATE TABLE `home_page_section3` (
  `id` int(10) UNSIGNED NOT NULL,
  `section3_number` int(11) DEFAULT NULL,
  `section3_tagline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_page_section3`
--

INSERT INTO `home_page_section3` (`id`, `section3_number`, `section3_tagline`, `status`, `created_at`, `updated_at`) VALUES
(1, 1000, 'years of customer focused service', 1, '2021-02-04 05:15:00', '2021-02-04 05:42:43'),
(2, 193, 'countries where we have happy customers', 1, '2021-02-04 05:15:00', '2021-02-04 05:42:52'),
(3, 1000, 'clients across the globe', 1, '2021-02-04 05:15:00', '2021-02-04 05:43:08'),
(4, 1000, 'successful projects completed', 1, '2021-02-04 05:16:00', '2021-02-04 05:43:17');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_section4_images`
--

CREATE TABLE `home_page_section4_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_page_section4_images`
--

INSERT INTO `home_page_section4_images` (`id`, `image`, `status`, `created_at`, `updated_at`, `image_name`) VALUES
(1, 'home-page-section4-images\\February2021\\2xsyaHTKar8H5c73Hde5.png', 1, '2021-02-04 02:06:12', '2021-02-04 02:06:12', 'Image-1'),
(2, 'home-page-section4-images\\February2021\\Bw9Ep8XyzJIj1QJ5YUxU.png', 1, '2021-02-04 02:06:24', '2021-02-04 02:06:24', 'Image-2'),
(4, 'home-page-section4-images\\February2021\\HjXmzGcaOWclyLgk0EHq.png', 1, '2021-02-04 02:08:09', '2021-02-04 02:08:09', 'Image-4'),
(5, 'home-page-section4-images\\February2021\\TtSSzGi4xIiKDibKUeju.png', 1, '2021-02-04 02:08:22', '2021-02-04 02:08:22', 'Image-5'),
(7, 'home-page-section4-images\\February2021\\GxfKI5MnwadESevYpELL.png', 1, '2021-02-04 02:09:44', '2021-02-04 02:09:44', 'Image-6'),
(9, 'home-page-section4-images\\February2021\\hKseKZuYCFX8DHLyqZc6.png', 1, '2021-02-04 02:15:11', '2021-02-04 02:15:11', 'Image-6');

-- --------------------------------------------------------

--
-- Table structure for table `job_resumes`
--

CREATE TABLE `job_resumes` (
  `id` int(10) UNSIGNED NOT NULL,
  `candidate_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `candidate_resume` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-12-22 01:08:22', '2020-12-22 01:08:22');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-12-22 01:08:22', '2020-12-22 01:08:22', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-12-22 01:08:23', '2020-12-22 01:08:23', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-12-22 01:08:23', '2020-12-22 01:08:23', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-12-22 01:08:23', '2020-12-22 01:08:23', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-12-22 01:08:23', '2020-12-22 01:08:23', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-12-22 01:08:24', '2020-12-22 01:08:24', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-12-22 01:08:24', '2020-12-22 01:08:24', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-12-22 01:08:24', '2020-12-22 01:08:24', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-12-22 01:08:24', '2020-12-22 01:08:24', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-12-22 01:08:24', '2020-12-22 01:08:24', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 8, '2020-12-22 01:08:37', '2020-12-22 01:08:37', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2020-12-22 01:08:38', '2020-12-22 01:08:38', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2020-12-22 01:08:40', '2020-12-22 01:08:40', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-12-22 01:08:43', '2020-12-22 01:08:43', 'voyager.hooks', NULL),
(15, 1, 'Queries', '', '_self', NULL, NULL, NULL, 15, '2020-12-29 03:51:54', '2020-12-29 03:51:54', 'voyager.query.index', NULL),
(16, 1, 'Team Members', '', '_self', NULL, NULL, NULL, 16, '2021-01-03 23:52:16', '2021-01-03 23:52:16', 'voyager.team-member.index', NULL),
(18, 1, 'Testimonials', '', '_self', NULL, NULL, NULL, 17, '2021-01-07 07:55:47', '2021-01-07 07:55:47', 'voyager.testimonial.index', NULL),
(19, 1, 'Service Types', '', '_self', NULL, NULL, NULL, 18, '2021-01-19 23:40:25', '2021-01-19 23:40:25', 'voyager.service-types.index', NULL),
(20, 1, 'Technology Types', '', '_self', NULL, NULL, NULL, 19, '2021-01-20 05:19:43', '2021-01-20 05:19:43', 'voyager.technology-type.index', NULL),
(21, 1, 'Clients', '', '_self', NULL, NULL, NULL, 20, '2021-01-27 08:14:56', '2021-01-27 08:14:56', 'voyager.client.index', NULL),
(22, 1, 'Careers', '', '_self', NULL, NULL, NULL, 21, '2021-02-01 07:34:12', '2021-02-01 07:34:12', 'voyager.career.index', NULL),
(23, 1, 'Job Resumes', '', '_self', NULL, NULL, NULL, 22, '2021-02-03 08:42:51', '2021-02-03 08:42:51', 'voyager.job-resumes.index', NULL),
(24, 1, 'Home Pages', '', '_self', NULL, NULL, NULL, 23, '2021-02-03 12:48:15', '2021-02-03 12:48:15', 'voyager.home-page.index', NULL),
(25, 1, 'Home Page Section4 Images', '', '_self', NULL, NULL, NULL, 24, '2021-02-04 00:38:15', '2021-02-04 00:38:15', 'voyager.home-page-section4-images.index', NULL),
(26, 1, 'Home Page Section3s', '', '_self', NULL, NULL, NULL, 25, '2021-02-04 05:04:38', '2021-02-04 05:04:38', 'voyager.home-page-section3.index', NULL),
(27, 1, 'Blog Categories', '', '_self', NULL, NULL, NULL, 26, '2021-02-06 07:11:59', '2021-02-06 07:11:59', 'voyager.blog-category.index', NULL),
(28, 1, 'Blogs', '', '_self', NULL, NULL, NULL, 27, '2021-02-06 08:20:45', '2021-02-06 08:20:45', 'voyager.blogs.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2016_01_01_000000_create_pages_table', 2),
(25, '2016_01_01_000000_create_posts_table', 2),
(26, '2016_02_15_204651_create_categories_table', 2),
(27, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2020-12-22 01:08:41', '2020-12-22 01:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-12-22 01:08:24', '2020-12-22 01:08:24'),
(2, 'browse_bread', NULL, '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(3, 'browse_database', NULL, '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(4, 'browse_media', NULL, '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(5, 'browse_compass', NULL, '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(6, 'browse_menus', 'menus', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(7, 'read_menus', 'menus', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(8, 'edit_menus', 'menus', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(9, 'add_menus', 'menus', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(10, 'delete_menus', 'menus', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(11, 'browse_roles', 'roles', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(12, 'read_roles', 'roles', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(13, 'edit_roles', 'roles', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(14, 'add_roles', 'roles', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(15, 'delete_roles', 'roles', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(16, 'browse_users', 'users', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(17, 'read_users', 'users', '2020-12-22 01:08:25', '2020-12-22 01:08:25'),
(18, 'edit_users', 'users', '2020-12-22 01:08:26', '2020-12-22 01:08:26'),
(19, 'add_users', 'users', '2020-12-22 01:08:26', '2020-12-22 01:08:26'),
(20, 'delete_users', 'users', '2020-12-22 01:08:26', '2020-12-22 01:08:26'),
(21, 'browse_settings', 'settings', '2020-12-22 01:08:26', '2020-12-22 01:08:26'),
(22, 'read_settings', 'settings', '2020-12-22 01:08:26', '2020-12-22 01:08:26'),
(23, 'edit_settings', 'settings', '2020-12-22 01:08:26', '2020-12-22 01:08:26'),
(24, 'add_settings', 'settings', '2020-12-22 01:08:26', '2020-12-22 01:08:26'),
(25, 'delete_settings', 'settings', '2020-12-22 01:08:26', '2020-12-22 01:08:26'),
(26, 'browse_categories', 'categories', '2020-12-22 01:08:37', '2020-12-22 01:08:37'),
(27, 'read_categories', 'categories', '2020-12-22 01:08:37', '2020-12-22 01:08:37'),
(28, 'edit_categories', 'categories', '2020-12-22 01:08:37', '2020-12-22 01:08:37'),
(29, 'add_categories', 'categories', '2020-12-22 01:08:37', '2020-12-22 01:08:37'),
(30, 'delete_categories', 'categories', '2020-12-22 01:08:37', '2020-12-22 01:08:37'),
(31, 'browse_posts', 'posts', '2020-12-22 01:08:38', '2020-12-22 01:08:38'),
(32, 'read_posts', 'posts', '2020-12-22 01:08:38', '2020-12-22 01:08:38'),
(33, 'edit_posts', 'posts', '2020-12-22 01:08:39', '2020-12-22 01:08:39'),
(34, 'add_posts', 'posts', '2020-12-22 01:08:39', '2020-12-22 01:08:39'),
(35, 'delete_posts', 'posts', '2020-12-22 01:08:39', '2020-12-22 01:08:39'),
(36, 'browse_pages', 'pages', '2020-12-22 01:08:40', '2020-12-22 01:08:40'),
(37, 'read_pages', 'pages', '2020-12-22 01:08:40', '2020-12-22 01:08:40'),
(38, 'edit_pages', 'pages', '2020-12-22 01:08:40', '2020-12-22 01:08:40'),
(39, 'add_pages', 'pages', '2020-12-22 01:08:40', '2020-12-22 01:08:40'),
(40, 'delete_pages', 'pages', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(41, 'browse_hooks', NULL, '2020-12-22 01:08:43', '2020-12-22 01:08:43'),
(42, 'browse_query', 'query', '2020-12-29 03:51:53', '2020-12-29 03:51:53'),
(43, 'read_query', 'query', '2020-12-29 03:51:53', '2020-12-29 03:51:53'),
(44, 'edit_query', 'query', '2020-12-29 03:51:53', '2020-12-29 03:51:53'),
(45, 'add_query', 'query', '2020-12-29 03:51:53', '2020-12-29 03:51:53'),
(46, 'delete_query', 'query', '2020-12-29 03:51:53', '2020-12-29 03:51:53'),
(47, 'browse_team_member', 'team_member', '2021-01-03 23:52:16', '2021-01-03 23:52:16'),
(48, 'read_team_member', 'team_member', '2021-01-03 23:52:16', '2021-01-03 23:52:16'),
(49, 'edit_team_member', 'team_member', '2021-01-03 23:52:16', '2021-01-03 23:52:16'),
(50, 'add_team_member', 'team_member', '2021-01-03 23:52:16', '2021-01-03 23:52:16'),
(51, 'delete_team_member', 'team_member', '2021-01-03 23:52:16', '2021-01-03 23:52:16'),
(57, 'browse_testimonial', 'testimonial', '2021-01-07 07:55:47', '2021-01-07 07:55:47'),
(58, 'read_testimonial', 'testimonial', '2021-01-07 07:55:47', '2021-01-07 07:55:47'),
(59, 'edit_testimonial', 'testimonial', '2021-01-07 07:55:47', '2021-01-07 07:55:47'),
(60, 'add_testimonial', 'testimonial', '2021-01-07 07:55:47', '2021-01-07 07:55:47'),
(61, 'delete_testimonial', 'testimonial', '2021-01-07 07:55:47', '2021-01-07 07:55:47'),
(62, 'browse_service_types', 'service_types', '2021-01-19 23:40:24', '2021-01-19 23:40:24'),
(63, 'read_service_types', 'service_types', '2021-01-19 23:40:24', '2021-01-19 23:40:24'),
(64, 'edit_service_types', 'service_types', '2021-01-19 23:40:24', '2021-01-19 23:40:24'),
(65, 'add_service_types', 'service_types', '2021-01-19 23:40:24', '2021-01-19 23:40:24'),
(66, 'delete_service_types', 'service_types', '2021-01-19 23:40:24', '2021-01-19 23:40:24'),
(67, 'browse_technology_type', 'technology_type', '2021-01-20 05:19:43', '2021-01-20 05:19:43'),
(68, 'read_technology_type', 'technology_type', '2021-01-20 05:19:43', '2021-01-20 05:19:43'),
(69, 'edit_technology_type', 'technology_type', '2021-01-20 05:19:43', '2021-01-20 05:19:43'),
(70, 'add_technology_type', 'technology_type', '2021-01-20 05:19:43', '2021-01-20 05:19:43'),
(71, 'delete_technology_type', 'technology_type', '2021-01-20 05:19:43', '2021-01-20 05:19:43'),
(72, 'browse_client', 'client', '2021-01-27 08:14:56', '2021-01-27 08:14:56'),
(73, 'read_client', 'client', '2021-01-27 08:14:56', '2021-01-27 08:14:56'),
(74, 'edit_client', 'client', '2021-01-27 08:14:56', '2021-01-27 08:14:56'),
(75, 'add_client', 'client', '2021-01-27 08:14:56', '2021-01-27 08:14:56'),
(76, 'delete_client', 'client', '2021-01-27 08:14:56', '2021-01-27 08:14:56'),
(77, 'browse_career', 'career', '2021-02-01 07:34:12', '2021-02-01 07:34:12'),
(78, 'read_career', 'career', '2021-02-01 07:34:12', '2021-02-01 07:34:12'),
(79, 'edit_career', 'career', '2021-02-01 07:34:12', '2021-02-01 07:34:12'),
(80, 'add_career', 'career', '2021-02-01 07:34:12', '2021-02-01 07:34:12'),
(81, 'delete_career', 'career', '2021-02-01 07:34:12', '2021-02-01 07:34:12'),
(82, 'browse_job_resumes', 'job_resumes', '2021-02-03 08:42:50', '2021-02-03 08:42:50'),
(83, 'read_job_resumes', 'job_resumes', '2021-02-03 08:42:50', '2021-02-03 08:42:50'),
(84, 'edit_job_resumes', 'job_resumes', '2021-02-03 08:42:50', '2021-02-03 08:42:50'),
(85, 'add_job_resumes', 'job_resumes', '2021-02-03 08:42:50', '2021-02-03 08:42:50'),
(86, 'delete_job_resumes', 'job_resumes', '2021-02-03 08:42:50', '2021-02-03 08:42:50'),
(87, 'browse_home_page', 'home_page', '2021-02-03 12:48:14', '2021-02-03 12:48:14'),
(88, 'read_home_page', 'home_page', '2021-02-03 12:48:14', '2021-02-03 12:48:14'),
(89, 'edit_home_page', 'home_page', '2021-02-03 12:48:14', '2021-02-03 12:48:14'),
(90, 'add_home_page', 'home_page', '2021-02-03 12:48:14', '2021-02-03 12:48:14'),
(91, 'delete_home_page', 'home_page', '2021-02-03 12:48:14', '2021-02-03 12:48:14'),
(92, 'browse_home_page_section4_images', 'home_page_section4_images', '2021-02-04 00:38:15', '2021-02-04 00:38:15'),
(93, 'read_home_page_section4_images', 'home_page_section4_images', '2021-02-04 00:38:15', '2021-02-04 00:38:15'),
(94, 'edit_home_page_section4_images', 'home_page_section4_images', '2021-02-04 00:38:15', '2021-02-04 00:38:15'),
(95, 'add_home_page_section4_images', 'home_page_section4_images', '2021-02-04 00:38:15', '2021-02-04 00:38:15'),
(96, 'delete_home_page_section4_images', 'home_page_section4_images', '2021-02-04 00:38:15', '2021-02-04 00:38:15'),
(97, 'browse_home_page_section3', 'home_page_section3', '2021-02-04 05:04:38', '2021-02-04 05:04:38'),
(98, 'read_home_page_section3', 'home_page_section3', '2021-02-04 05:04:38', '2021-02-04 05:04:38'),
(99, 'edit_home_page_section3', 'home_page_section3', '2021-02-04 05:04:38', '2021-02-04 05:04:38'),
(100, 'add_home_page_section3', 'home_page_section3', '2021-02-04 05:04:38', '2021-02-04 05:04:38'),
(101, 'delete_home_page_section3', 'home_page_section3', '2021-02-04 05:04:38', '2021-02-04 05:04:38'),
(102, 'browse_blog_category', 'blog_category', '2021-02-06 07:11:57', '2021-02-06 07:11:57'),
(103, 'read_blog_category', 'blog_category', '2021-02-06 07:11:57', '2021-02-06 07:11:57'),
(104, 'edit_blog_category', 'blog_category', '2021-02-06 07:11:57', '2021-02-06 07:11:57'),
(105, 'add_blog_category', 'blog_category', '2021-02-06 07:11:57', '2021-02-06 07:11:57'),
(106, 'delete_blog_category', 'blog_category', '2021-02-06 07:11:57', '2021-02-06 07:11:57'),
(107, 'browse_blogs', 'blogs', '2021-02-06 08:20:45', '2021-02-06 08:20:45'),
(108, 'read_blogs', 'blogs', '2021-02-06 08:20:45', '2021-02-06 08:20:45'),
(109, 'edit_blogs', 'blogs', '2021-02-06 08:20:45', '2021-02-06 08:20:45'),
(110, 'add_blogs', 'blogs', '2021-02-06 08:20:45', '2021-02-06 08:20:45'),
(111, 'delete_blogs', 'blogs', '2021-02-06 08:20:45', '2021-02-06 08:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-12-22 01:08:39', '2020-12-22 01:08:39'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-12-22 01:08:39', '2020-12-22 01:08:39'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-12-22 01:08:39', '2020-12-22 01:08:39'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-12-22 01:08:39', '2020-12-22 01:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `query`
--

CREATE TABLE `query` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `query`
--

INSERT INTO `query` (`id`, `name`, `email`, `mobile`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Deepak Sharma', 'deepaksharma4557@gmail.com', '8989898989', 'Need a website to take my business online.', '2020-12-31 03:02:40', '2020-12-31 03:02:40'),
(2, 'Sapna Sharma', 'sapna@gmail.com', '7903603834', 'deepak is my friend.', '2020-12-31 03:08:05', '2020-12-31 03:08:05'),
(3, 'Deepak Sharma', 'mrityunjaykrjha338@gmail.com', '8989898989', 'szfsbhrthm', '2020-12-31 03:17:12', '2020-12-31 03:17:12'),
(4, 'Naman Kumar', 'naman@gmail.com', '8989898989', 'fbftnfm', '2020-12-31 03:18:38', '2020-12-31 03:18:38'),
(5, 'Deepak Sharma', 'admin@admin.com', '5454545401', 'fbdrth', '2020-12-31 03:19:21', '2020-12-31 03:19:21'),
(6, '', '', NULL, NULL, '2021-02-03 09:39:57', '2021-02-03 09:39:57'),
(7, '', '', NULL, NULL, '2021-02-03 09:40:03', '2021-02-03 09:40:03'),
(8, '', '', NULL, NULL, '2021-02-03 09:40:33', '2021-02-03 09:40:33'),
(9, '', 'mrityunjaykrjha338@gmail.com', NULL, NULL, '2021-02-03 09:50:30', '2021-02-03 09:50:30'),
(10, '', '', NULL, NULL, '2021-02-03 09:50:58', '2021-02-03 09:50:58'),
(11, '', '', NULL, NULL, '2021-02-03 09:51:25', '2021-02-03 09:51:25'),
(12, '', 'mrityunjaykrjha338@gmail.com', NULL, NULL, '2021-02-03 09:55:29', '2021-02-03 09:55:29'),
(13, '', 'admin@admin.com', NULL, NULL, '2021-02-03 11:05:25', '2021-02-03 11:05:25');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-12-22 01:08:24', '2020-12-22 01:08:24'),
(2, 'user', 'Normal User', '2020-12-22 01:08:24', '2020-12-22 01:08:24');

-- --------------------------------------------------------

--
-- Table structure for table `service_types`
--

CREATE TABLE `service_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section3_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section3_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section3_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section3_points` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_points` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_types`
--

INSERT INTO `service_types` (`id`, `service_title`, `service_icon`, `service_banner`, `service_image`, `service_description`, `url`, `section3_image`, `section3_title`, `section3_description`, `section4_image`, `section4_title`, `section4_description`, `section3_points`, `section4_points`, `status`, `created_at`, `updated_at`) VALUES
(1, 'UI/UX Design & Validation', 'service-types\\January2021\\97alLSsA5v5f4xujbcsF.png', NULL, NULL, NULL, '/ui-ux-development', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 00:38:38', '2021-01-23 09:23:40'),
(2, 'Mobile App Development', 'service-types\\January2021\\X0dUZKWLPgPeOpLjCURW.png', NULL, NULL, NULL, '/mobile-app-development', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 00:39:28', '2021-01-23 09:28:31'),
(3, 'Website Development', 'service-types\\January2021\\3oAQjwAVZHcddrBmnwyG.png', 'service-types\\January2021\\K8gzHIYUE06L5Zmq4QwW.jpg', 'service-types\\January2021\\ZA78H6U2epSIe7y09482.jpg', 'We use the latest technologies such as PHP, Web 2.0, HTML, Joomla, WordPress, Drupal, Magento and OSCommerce to create responsive and tailor-made web applications for your business. Our experienced team leverages the dynamism of PHP and combines it with database languages such as MySQL, HTML, CSS, JavaScript, PHP Code. We use frameworks such as ZEND, Codeigniter, Laravel, Yii, and CakePHP to create products that work fluidly across platforms and devices.', '/website-development', 'service-types\\January2021\\ejvfcloxEu9zkaf7PE0a.jpg', 'Web Development Services', 'We provide a wide range of services. At Eigerlab Technologies, we dig deep, understand our client’s objectives and unique business challenges to provide the best suited web app solution.', 'service-types\\January2021\\3CEVjMbzWdxKECiKAvhj.png', 'Web Development Life Cycle', NULL, 'Custom Web Application. Ecommerce Development. Portal development. Enterprise App Development. Website Development and Maintenance', 'Gathering Relevant Information. Planning – Sitemap and Wireframe. Design & Layout. Development. Testing, Review, and Launch. Maintenance and Updation', 1, '2021-01-20 00:39:55', '2021-01-25 06:01:20'),
(4, 'SaaS', 'service-types\\January2021\\Xg69hxlzh1lgCIXpZ4v8.png', NULL, NULL, NULL, '/software-as-a-service', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 00:40:14', '2021-01-23 09:27:49'),
(5, 'Chatbots', 'service-types\\January2021\\1jkkkFMRYS9yoS6Q7lSP.png', NULL, NULL, NULL, '/chatbots', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 00:40:49', '2021-01-23 09:26:35'),
(6, 'IOT', 'service-types\\January2021\\qVYoTdCTVy7RQ3va4Qoc.png', NULL, NULL, NULL, '/internet-of-things', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 00:41:10', '2021-01-23 09:26:14'),
(7, 'Augmented Reality', 'service-types\\January2021\\KYs62QvYfBck4TzxYYtX.png', NULL, NULL, NULL, '/augmented-reality', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 00:42:20', '2021-01-23 09:25:40'),
(8, 'Testing & Quality Assurance', 'service-types\\January2021\\9UGspJmjLbMcqUn6RJfe.png', NULL, NULL, NULL, '/quality-assurance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 00:42:51', '2021-01-23 09:25:15'),
(9, 'SEO Packages', 'service-types\\January2021\\BHIwV0NaOqYfNlmZfslr.png', NULL, NULL, NULL, '/seo-packages', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 00:43:14', '2021-01-23 09:24:29'),
(10, 'Maintenance & Support', 'service-types\\January2021\\TbOAqnyRATGmkZG52eVi.png', NULL, NULL, NULL, '/maintenance-support', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 00:43:39', '2021-01-23 09:24:12');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `team_member`
--

CREATE TABLE `team_member` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_accounts` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team_member`
--

INSERT INTO `team_member` (`id`, `name`, `designation`, `image`, `description`, `social_accounts`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Member 1', 'PHP Developer', 'team-member\\January2021\\laZeifmIS9Inh31cVRMx.jpg', NULL, 'https://www.facebook.com/,https://www.instagram.com/,https://in.linkedin.com/,https://www.twitter.com/LOGIN', 1, '2021-01-05 12:27:48', '2021-01-11 06:02:08'),
(2, 'Member 2', 'Project Head', 'team-member\\January2021\\zWcisSHeanRkp14OF4lV.jpg', NULL, 'https://in.linkedin.com/,https://www.twitter.com/LOGIN', 1, '2021-01-05 12:28:19', '2021-01-11 06:16:21'),
(3, 'Member 3', 'Java Developer', 'team-member\\January2021\\xJHsaorzf7aAKSUGx41C.jpg', NULL, 'https://www.facebook.com/,https://in.linkedin.com/,https://www.twitter.com/LOGIN', 1, '2021-01-05 12:28:36', '2021-01-11 06:16:37'),
(4, 'Member 4', 'UI/UX Developer', 'team-member\\January2021\\jjjbfSp2qE1juLVKrBla.jpg', NULL, 'https://in.linkedin.com/', 1, '2021-01-05 12:28:53', '2021-01-11 06:16:50'),
(5, 'Member 5', 'Senior Software Engineer', 'team-member\\January2021\\ONHoPhCGMvRovI0Y9yjD.jpg', NULL, 'https://in.linkedin.com/,https://www.twitter.com/LOGIN', 1, '2021-01-05 12:29:29', '2021-01-11 06:17:02'),
(6, 'Member 6', 'Software Developer', 'team-member\\January2021\\Z2pM9oPvbSByYoQByxba.jpg', NULL, 'https://www.facebook.com/,https://in.linkedin.com/,https://www.twitter.com/LOGIN', 1, '2021-01-05 12:29:50', '2021-01-11 06:17:15'),
(7, 'Member 7', 'CEO', 'team-member\\January2021\\yuuCsMLdZcvSvTYSgU3H.jpg', 'Ken Levine is serving as CEO from 2014 - present. Ken has over 20 years of startup and business leadership experience, primarily focused in technology and information security.', 'https://www.instagram.com/,https://www.facebook.com/,https://in.linkedin.com/,https://accounts.snapchat.com/', 1, '2021-01-06 06:11:21', '2021-01-11 06:17:26');

-- --------------------------------------------------------

--
-- Table structure for table `technology_type`
--

CREATE TABLE `technology_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `technology_title` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `technology_icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `technology_banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `technology_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `technology_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section3_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section3_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section3_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section3_points` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section4_points` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `technology_type`
--

INSERT INTO `technology_type` (`id`, `technology_title`, `technology_icon`, `url`, `technology_banner`, `technology_image`, `technology_description`, `section3_image`, `section3_title`, `section3_description`, `section3_points`, `section4_image`, `section4_title`, `section4_description`, `section4_points`, `status`, `created_at`, `updated_at`) VALUES
(1, 'PHP Development', 'technology-type\\January2021\\DKWJCMfDa25QY60FjvmC.png', '/php-development', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 05:33:00', '2021-01-27 00:00:04'),
(2, 'Node Js Development', 'technology-type\\January2021\\B2IYyopfdRIrCUzpzrnD.png', '/node-js-development', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 05:34:00', '2021-01-27 00:01:01'),
(3, 'React Development', 'technology-type\\January2021\\Lkne8ELEhJdhTg3ey5mq.png', '/react-development', 'technology-type\\January2021\\y1dWNTm1dxm6EDMNpmWb.png', 'technology-type\\January2021\\dvcwjZYEnf8S7clfmnYq.png', 'React.js Developers design and implement user interface components for JavaScript-based web and mobile applications using the React open source library ecosystem. These skilled front-end developers are involved in all stages of interface component design, from conception through to final testing.', 'technology-type\\January2021\\vKYu1EdMiaXs8P1OLfEP.png', 'React Development Services', 'React-JS development has become one of the hottest topics in the world of Web and App development for all the right reasons. They focus on designing, developing, and delivering robust web solutions, considering today’s business scenario. The development team is highly skilled and has delivered complex web applications across industries such as healthcare, gaming, logistics, etc. They are pioneers in whatever they do, and their client testimonials show that they are a great team to work with.', 'React web app development. SPA development. Migration to React. Ongoing support', 'technology-type\\January2021\\6yzaPK9Ppn4Iz0sIlu7w.png', 'React Key Features', 'React-JS development has become one of the hottest topics in the world of Web and App development for all the right reasons. They focus on designing, developing, and delivering robust web solutions, considering today’s business scenario. The development team is highly skilled and has delivered complex web applications across industries such as healthcare, gaming, logistics, etc. They are pioneers in whatever they do, and their client testimonials show that they are a great team to work with.', 'Create isomorphic and complex applications. One way data binding allows monitoring & managing of modifications. Supports the DOM model to manage & handle the never-ending need to constantly increasing & changing data. Hybrid of JavaScript & HTML or JSX that simplifies the codes & makes it readable', 1, '2021-01-20 05:34:00', '2021-01-27 01:02:42'),
(4, 'Ionic Development', 'technology-type\\January2021\\gDYveBbTNCajkYJHQL0r.png', '/ionic-development', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 05:35:00', '2021-01-27 00:00:48'),
(5, 'Flutter Development', 'technology-type\\January2021\\L0zmM5m5N86OhT5u4IaV.png', '/flutter-development', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 05:36:00', '2021-01-27 00:00:26'),
(6, 'Xamarin Development', 'technology-type\\January2021\\fu1zY29MBKBo1knuTtFg.png', '/xamarin-development', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-20 05:36:00', '2021-01-27 00:00:35');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_designation` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_company` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `client_name`, `client_designation`, `client_company`, `client_image`, `client_review`, `status`, `created_at`, `updated_at`) VALUES
(1, 'James Beid', 'Relationship Manager', 'Global Venture Software Pvt. Ltd.', 'testimonial\\January2021\\PWRpBcznmsAAJ5qCwyrn.png', 'When you innovate, you make mistakes. It is best to admit them quickly & get on with improving your other innovations.', 1, '2021-01-08 00:30:00', '2021-01-08 00:42:10'),
(2, 'John Wick', 'Co-Owner', 'Sunshine Software Pvt. Ltd.', 'testimonial\\January2021\\9xGVTTHIsXP7Fz2IFknu.png', 'A robust congratulations to the team at Eigerlab Technologies for a job well done.', 1, '2021-01-08 00:38:07', '2021-01-08 00:38:07'),
(3, 'Margot Whitney', 'Spokesperson', 'Leading System Software Pvt. Ltd.', 'testimonial\\January2021\\ldomeoJJsmwQLm07HTLI.jpeg', 'We thank for the wonderful job in helping us develop our program. Everyone was professional, excellent and hard working. Thanks to them, we were able to achieve our goal on time, and we look forward to continue working with them in the future.', 1, '2021-01-08 00:41:00', '2021-01-08 00:41:41'),
(4, 'Mark Colgan', 'CEO', 'Logistics Company', 'testimonial\\January2021\\pKNvjs44YupWH1crktGc.jpg', 'The wonderful team effort of Outsource2india helped me create new hopes for my industry. With their professionalism, prompt response and courteous service, I was able to design wonderful and innovative web applications that will break new ground in the logistics industry.', 1, '2021-01-08 00:46:15', '2021-01-08 00:46:15');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-12-22 01:08:41', '2020-12-22 01:08:41'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-12-22 01:08:42', '2020-12-22 01:08:42'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-12-22 01:08:42', '2020-12-22 01:08:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users\\December2020\\Iz22byqBrUIVHg1uc7QW.png', NULL, '$2y$10$jCKONVgJ5qZbK9NqiZYMHuCE4JIV8AmwG1cAbBSeM3NxFyVlULbv6', 'M7P1MwonkfPlzpWRKO8jJK2RfVWpKcTVOuHqLxNa2Qsebbg8yibyU7j34GM9', '{\"locale\":\"en\"}', '2020-12-22 01:08:37', '2020-12-22 01:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page`
--
ALTER TABLE `home_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_section3`
--
ALTER TABLE `home_page_section3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_section4_images`
--
ALTER TABLE `home_page_section4_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_resumes`
--
ALTER TABLE `job_resumes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `query`
--
ALTER TABLE `query`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `service_types`
--
ALTER TABLE `service_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `team_member`
--
ALTER TABLE `team_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `technology_type`
--
ALTER TABLE `technology_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_page`
--
ALTER TABLE `home_page`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page_section3`
--
ALTER TABLE `home_page_section3`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_page_section4_images`
--
ALTER TABLE `home_page_section4_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `job_resumes`
--
ALTER TABLE `job_resumes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `query`
--
ALTER TABLE `query`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_types`
--
ALTER TABLE `service_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `team_member`
--
ALTER TABLE `team_member`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `technology_type`
--
ALTER TABLE `technology_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
