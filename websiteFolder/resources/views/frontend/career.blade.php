@extends('frontend.master')
@section('content')
<style type="text/css">
	.para-1{
		font-size: 18px;
		text-align: center;
		color: #707070;
		margin:20px 0px 20px 0px;/* TOP; RIGHT; BOTTOM; LEFT */
		/*background-color: green;*/
    /*padding: 20px 20px 20px 20px; */
	}

	.jobopeningview{
		/*margin: */
	}

	.jobopeningview > h1{
		color: black;
		text-align: left;
		/*margin: 10px;*/
		padding-top: 50px;
		padding-left: 15px;
	}

	.designation_profile{
		font-size: 30px;
		text-align: left;
	}

	.jobopeningview .comeworkwith .create-border{
		
		padding-top: 30px;
		padding-left: 15px;
		background-color: white;
		
	}

	.job-experience{
		font-size: 15px;
    color: black;
	}

.designation_profile{
   /* font-size: 24px;
    color: #282828;
    font-weight: 600;*/
    -webkit-transition: all 400ms linear;
  transition: all 400ms linear;
}
.comeworkwith ul li:hover .designation_profile{
    padding-left: 20px;
    background-color: #E0E0E0;
}

.job-experience{
    -webkit-transition: all 400ms linear;
  transition: all 400ms linear;
}
.comeworkwith ul li:hover .job-experience{
    padding-left: 20px;
    background-color: #E0E0E0;
}
.tag-line{
    font-family: "Times New Roman", Times, serif;
                font-size:      20px;
                /*font-weight:    bold;*/
  }
</style>

<section class="career-heading" style="background-color: #FFFFFF; margin-top: 64px; width: 100%; height: auto;" >
	<div class="container">
		<div>
			<h2 style="padding: 0px 0px 0px 20px;  text-align: center; color: black;">Together At Eigerlab Technologies</h2>
		<p class="para-1">We are a team that practices design-led engineering, innovates iteratively, and builds delightful<br>
products with the potential to create a positive impact. Sounds like a team you want to join?</p>
		</div>
	</div>
	<!-- <div style="text-align: center;">
		<a id="jobopening-section" href="#jobopenings">
			<h3>Current Opening</h3>
		</a>
	</div> -->
	<div class="career-banner" id="career-banner" style="margin: 0px 0px 0px 0px;">
		<img style="width: 100%" src="{{URL::to('public/img/career/team-banner.png')}}">
	</div>
</section>


<!--Come Work With Us Start-->

            <div class="container comeworkwithuswrap jobopeningview" id="jobopenings" style="">
            	<!-- style="background-color: #f7f7f7;" -->
                <h1>More than a job…it’s an adventure in innovation</h1>
                <h4 style="text-align: left; padding-bottom: 50px; padding-left: 15px;">An adventurous journey awaits...</h4>
                <div class="comeworkwith">
                    <ul>
                    	<?php
              
                foreach($vacancy as $value){
                 // dd($value['job_url']);
                ?>
                        <li class="create-border" style="width: 100%; height: 125px; border-bottom: 1px solid #A8A8A8;">
                            <a href="{{URL::to('career'.'/'.$value['id'])}}">
                                <div class="designation_profile" >{{$value['job_title']}}</div>

                                <p class="job-experience" >{{$value['job_experience']}}</p>
                                <span class="arrowlist"></span>
                                
                              </a>
                        </li>
                        <?php }?>
                                            <!-- <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=2">
                                <div class="designation_profile">Sr iOS Developer </div>
                                <p class="job-experience">Exp: 4 to 6 years</p>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=3">
                                <div class="designation_profile">Android Developer </div>
                                <p class="job-experience">Exp: 2 to 4 years</p>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=4">
                                <div class="designation_profile">Sr. Android Developer </div> 
                                <p class="job-experience">Exp: 4 to 6 years</p>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li  class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=5">
                                <div class="designation_profile">React Native Developer </div>
                                <p class="job-experience">Exp: 2 to 4 years</p>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=6">
                                <div class="designation_profile">UI/UX Designer </div>
                                <p class="job-experience">Exp: 4 to 6 years</p>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=7">
                                <div class="designation_profile">QA Automation Engineer </div>
                                <p class="job-experience">Exp: 2 to 4 years</p>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=9">
                                <div class="designation_profile">Content Writer </div>
                                <p class="job-experience">Exp: 0 to 2 years</p>
                                <span class="arrowlist"></span>
                              </a>
                        </li> -->
                                            <!-- <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=10">
                                <div class="designation_profile">Project Manager <span class="job-experience">Exp: 4 to 6 years</span></div>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=11">
                                <div class="designation_profile">Web Architect <span class="job-experience">Exp: 6 to 8 years</span></div>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=12">
                                <div class="designation_profile">Intern <span class="job-experience">Exp: 0 to 0.5 years</span></div>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=13">
                                <div class="designation_profile">Business Development Executive <span class="job-experience">Exp: 0 to 0.5 years</span></div>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li class="create-border" style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=14">
                                <div class="designation_profile">Node.js developer <span class="job-experience">Exp: 2 to 4 years</span></div>
                                <span class="arrowlist"></span>
                              </a>
                        </li> -->
                                            <!-- <li style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=15">
                                <div class="designation_profile">Graphic Designing <span class="job-experience">Exp: 0 to 2 years</span></div>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=16">
                                <div class="designation_profile">Sales Executive <span class="job-experience">Exp: 0.5 to 1 years</span></div>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=17">
                                <div class="designation_profile">Sr. PHP Developer <span class="job-experience">Exp: 3 to 5 years</span></div>
                                <span class="arrowlist"></span>
                              </a>
                        </li>
                                            <li style="width: 100%; height: 100px;">
                            <a href="career/job_openings?type=18">
                                <div class="designation_profile">Accounts Manager <span class="job-experience">Exp: 1 to 2 years</span></div>
                                <span class="arrowlist"></span>
                              </a>
                        </li> -->
                                        </ul>
                </div>
            </div>

            <div class="container" style="background-color: #fbfbfb; width: 100%; height: auto; position: relative; margin-top: 60px;">
              <!-- <div class="col-md-12" style="font-size: 25px; position: relative;">Think you fit in?</div> -->
              <div class="tag-line col-md-12" style="position: relative; text-align: center; padding-top: 5px;">Not seen an open position, drop your Resume</div>
              <div class="row margin-lg-50t margin-lg-100b margin-md-80b margin-sm-50b margin-sm-30t">
      <div class="col-md-12">
        @if(Session::has('success'))
        <div class="col-sm-12">
          <div class="alert alert-success alert-dismissable">
            {{ session::get('success')}}
            <a class="close" data-dismiss="alert">&times;</a>
          </div>
        </div>
        @endif

        <div class="aheto-form aheto-form--default aheto-form--saas">
          <p id="head"></p>
          <form name="qform" class="wpcf7-form" method="post" action="{{URL::to('/submit-form')}}" enctype="multipart/form-data">
            @csrf
            <p>
              <span class="wpcf7-form-control-wrap Email">
                <input id="email" name="email"  type="text" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your E-Mail Address*">
                <span id="spanEmail"></span>
              </span>
              <span class="wpcf7-form-control-wrap Name" style="background-color: white;">
                <input id="resume" name="resume" type="file" size="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"aria-required="true" aria-invalid="false" placeholder="Attach Your Resume*">
                <span id="spanResume"></span>
              </span>
              <!-- <span class="wpcf7-form-control-wrap Phone">
                <input id="mobile" name="mobile"  type="text" required="mobile" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Your Mobile Number*">
                <span id="span3"></span>
              </span> -->
            </p>
            <p class="form-bth-holder">
              <input id="submit" type="submit" value="SEND MESSAGE" class="wpcf7-form-control wpcf7-submit ">
            </p>
          </form>
          <!-- <div id="succ_message"></div> -->
        </div>
      </div>
    </div>
      <!-- <div for="upload_file" class="control-label col-sm-6 fa fa-paperclip">Upload Resume</div>
     <div class="col-sm-6">
          <input class="form-control" type="file" name="upload_file" id="upload_file" style="width: 100%; height: auto; ">
     </div> -->

     <!-- <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Current Annual CTC (in ₹)<span>*</span></label>
                                    <input class="form-control number" name="current_ctc" type="text" placeholder="Type your current CTC" maxlength ="10" onkeypress="return isNumberKey(event)" required/>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Import Resume File <span>*</span></label>
                                    
                                    <div id="upload_prev">
                                        <div class="fileUpload">
                                            <span>Attach file <i class="fa fa-paperclip" aria-hidden="true"></i></span>
                                            <input id="uploadBtn" name="resume" type="file" class="upload" accept="application/msword,text/plain, application/pdf,.doc, .docx" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                            <div class="submitApplicationrw">
                              
                                <button type="submit" name= "submit" value = "submit" class="submitApplication" id ="submitApplication">Submit Application <div class="loadingview" id="loadData">
                                 <div class="lds-hourglass"></div>
                              </div></button>
                           </div>
                        </div>
                        </div> -->


     <!-- <div class="col-sm-12" style="position: relative;">
                                <div class="form-group">
                                    <label>Import Resume File <span>*</span></label>
                                    
                                    <div id="upload_prev">
                                        <div class="fileUpload">
                                            <span>Attach file <i class="fa fa-paperclip" aria-hidden="true"></i></span>
                                            <input id="uploadBtn" name="resume" type="file" class="upload" accept="application/msword,text/plain, application/pdf,.doc, .docx" required/>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
 
</div>

<script>
    $(document).ready(function() {
      $('#submit').click(function(e){
        // Initializing Variables With Form Element Values

        // var name = $('#name').val();
        var email = $('#email').val();
      //   
        var email_regex =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        // To Check Empty Form Fields.
        // if (name.length == 0) {
        // $('#head').html('<span class="somethingwrong" style="color:red; font-size:12px;">* All fields are mandatory *</span>').show(); // This Segment Displays The Validation Rule For All Fields
        // setTimeout(function(){ $('#head').fadeOut() }, 5000);
        // $("#name").focus();
        // return false;
        // }

        // Validating Email Field.
        if (email.length == 0) {
        $('#spanEmail').html('<span class="blankemail" style="color:red; font-size:12px;">* Email is required *</span>').show(); // This Segment Displays The Validation Rule For Name
        setTimeout(function(){ $('#spanEmail').fadeOut() }, 5000);
        $("#email").focus();
        return false;
        }

        else if (!email.match(email_regex)) {
        $('#spanEmail').html('<span class="emailwrong" style="color:red; font-size:12px;">* Please enter a valid email address *</span>').show(); // This Segment Displays The Validation Rule For Email
        setTimeout(function(){ $('#spanEmail').fadeOut() }, 5000);
        $("#email").focus();
        return false;
        }

        //validate file type

        

        // else {
        //   // $("#succ_message").html('<div class="thankstext"><a>Thanks For Contacting!!!</div>').show();
        //   // setTimeout(function(){ $('#succ_message').fadeOut() }, 5000);
        // alert("Thanks For Contacting with us!!!");
        // return true;
        // }

        });

      $("#resume").change(function () {
        var fileExtension = ['pdf', 'doc', 'docx'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
        }
    });
      // body...
    });
  </script>

  <!-- <script> 
        function fileValidation() {
            var fileInput =  
                document.getElementById('resume'); 
              
            var filePath = fileInput.value; 
          
            // Allowing file type 
            var allowedExtensions =  
                    /(\.pdf|\.docx|\.doc)$/i; 
              
            if (!allowedExtensions.exec(filePath)) { 
                alert('Please choose a valid file type'); 
                fileInput.value = ''; 
                return false; 
            }
            else{
              return true;
            }
            // else  
            // { 
              
            //     // Image preview 
            //     if (fileInput.files && fileInput.files[0]) { 
            //         var reader = new FileReader(); 
            //         reader.onload = function(e) { 
            //             document.getElementById( 
            //                 'imagePreview').innerHTML =  
            //                 '<img src="' + e.target.result 
            //                 + '"/>'; 
            //         }; 
                      
            //         reader.readAsDataURL(fileInput.files[0]); 
            //     } 
            // } 
        } 
    </script>  -->


            <script>
            	$(document).ready(function(){
  // Add smooth scrolling to all links
  $("#jobopening-section").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
            </script>
         <!--Come Work With Us End-->
@stop