<!DOCTYPE html>
<html>
<head>
  <!-- <header class="aheto-header aheto-header-9">
    <div class="aheto-header-9__nav-wrap">
      <div class="aheto-header-9__inner">
        <div class="aheto-header-9__line">
          <div class="aheto-header-9__logo">
            <div class="logo">
              <a class="logo__link" href="">
                <div class="logo__img-holder">
                  <img class="logo__img" src="{{URL::to('public/img/home/home-saas/eigerlab-36x36.png')}}" alt="Logo image">
                </div>
                <div class="logo__text-holder">
                  <h3 class="logo__text">Eigerlab</h3>
                </div>
              </a>
            </div>
          </div>
          <div class="aheto-header-9__menu js-menu js-menu-height">
            <ul class="main-menu padding-lg-5t padding-md-0t">
              <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item--mega-menu ">
                <a href="{{URL::to('/')}}">Home</a>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="{{URL::to('/about')}}">About</a>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                <a href="#">Features</a>
                <ul class="sub-menu menu-depth-1">
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/features')}}">Features saas</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/features_detals')}}">Features details saas</a></li>
                </ul>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                <a href="#">Blog</a>
                <ul class="sub-menu menu-depth-1">
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/blog')}}">Blog saas</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/post')}}">Post saas</a></li>
                </ul>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="{{URL::to('/pricing')}}">Pricing</a>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="{{URL::to('/contact')}}">Contact Us</a>
              </li>
            </ul>
            <div class="aheto-header-9__authentication">
              <div class="authentication">
                <button class="authentication__sign-in" type="button">Sign In</button>
                <button class="authentication__sign-up" type="button">Sign Up</button>
              </div>
            </div>
          </div>
          <div class="aheto-header-9__hamburger">
            <button class="hamburger hamburger--squeeze js-hamburger js-hamburger--body-over" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
          </div>
        </div>
      </div>
    </div>
  </header> -->
  <!-- ....................................Working header code from server.......................................... -->

  <meta charset="utf-8">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta name="format-detection" content="telephone=no">
  <link rel="icon" href="{{url('public/img/eigerlab-32x32.png')}}" sizes="32x32" />
  <link rel="icon" href="{{url('public/img/eigerlab-192x192.png')}}" sizes="192x192" />
  <link rel="apple-touch-icon-precomposed" href="{{url('public/img/eigerlab-180x180.png')}}" />
  <meta name="msapplication-TileImage" content="{{url('public/img/eigerlab-270x270.png')}}" />
  <title>Eigerlab - Home page</title>
  <!-- FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Karla:400,400i,700|Poppins:300,400,400i,600|Source+Sans+Pro:400,400i,600,700,900|Mukta:400,700,800|Oswald:400,600|Lato:400,400i,700|Roboto:300,400,500,700|Roboto+Slab:400,700|Playfair+Display:400,400i,700i,700|Catamaran:300,400,500,600,700,900|Merriweather:400,700|Montserrat:400,500,600|Nunito|Open+Sans:300,400,500,600|Caveat:400,700|Dancing+Script|Libre+Baskerville:400,700"
    rel="stylesheet">
  <!-- STYLES -->
  <!-- Main Style -->
  <link rel="stylesheet" id="style-main" href="{{url('public/css/styles-main.css?v=3')}}">
  <link rel="stylesheet" id="swiper-main" href="{{url('public/vendors/swiper/swiper.min.css')}}">
  <link rel="stylesheet" id="lity-main" href="{{url('public/vendors/lity/lity.min.css')}}">
  <link rel="stylesheet" id="mediaelementplayer" href="{{url('public/vendors/mediaelement/mediaelementplayer.min.css')}}">
  <link rel="stylesheet" id="range" href="{{url('public/vendors/range/jquery.range.css')}}">
  <link rel="stylesheet" id="lightgallery" href="{{url('public/vendors/lightgallery/lightgallery.min.css')}}">
  <link rel="stylesheet" id="style-link" href="{{url('public/css/styles-9.css?v=35')}}">

  <link rel="stylesheet" id="style-main-1" href="{{url('public/css/new-style.css')}}">
  <!-- SCRIPTS -->
  <!-- jQuery -->
  <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-migrate-1.3.0.js" integrity="sha256-/Gj+NlY1u/J2UGzM/B2QrWR01trK8ZZqrD5BdqQUsac=" crossorigin="anonymous"></script> -->
  <!-- Swiper -->
  <script src="{{url('public/vendors/swiper/swiper.min.js')}}"></script>
  <!-- Isotope -->
  <script src="{{url('public/vendors/isotope/isotope.pkgd.min.js')}}"></script>
  <!-- Images loaded library -->
  <script src="{{url('public/vendors/lazyload/imagesloaded.pkgd.min.js')}}"></script>
  <!-- MediaElement js library (only for Aheto HTML) -->
  <script src="{{url('public/vendors/mediaelement/mediaelement.min.js')}}"></script>
  <script src="{{url('public/vendors/mediaelement/mediaelement-and-player.min.js')}}"></script>
  <!-- Typed animation text -->
  <script src="{{url('public/vendors/typed/typed.min.js')}}"></script>
  <!-- Lity Lightbox -->
  <script src="{{url('public/vendors/lity/lity.min.js')}}"></script>

  <!--   Techugo links-->
  <link rel="icon" href="{{url('public/img/favicon-180x180.png')}}"  />
  <link rel="shortcut icon" href="{{url('public/img/favicon-180x180.png')}}"  />
  <!-- <link href="https:///assets/css/main.css" rel="stylesheet" type="text/css"> -->
  <link rel="stylesheet" id="style-main" href="{{url('public/css/new-style-1.css')}}">

  <link href="{{URL::to('public/css/navigationbarcss/home-page.css')}}" rel="stylesheet" type="text/css">

  <!-- <script src="https://www.techugo.com/assets/js/jquery-2.1.1.js"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script> -->
  <script src="{{URL::to('public/js/jquery-2.1.1.js')}}"></script>
  <script src="{{URL::to('public/js/owl.carousel.min.js')}}"></script>
  <script src="{{URL::to('public/js/script.js')}}"></script>

  <!-- <script src="{{URL::to('public/jquery/jquery.min.js')}}"></script> -->


  <!-- Magnific popup -->
  
</head>

<body data-anm=".anm" class="saas">
  <!-- <header class="aheto-header aheto-header-9"> -->
    <!--Header Section Start Here--> 
    <!-- <div id="wrapper"> -->
      <nav class="navbar navbar-default navbar-fixed-top nav-down" width="100%">
   <div class="container-fluid">
      <div class="navbar-header">
         <div class="button_container" id="toggle">
          <span class="top"></span>
          <span class="middle"></span>
          <span class="bottom"></span>
        </div>
         <!-- <a class="navbar-brand" href="{{URL::to('/')}}">Eigerlab Technologies</a>  -->
         <div class="logo">
              <a class="logo__link" href="{{URL::to('/')}}">
                <div class="logo__img-holder">
                  <img class="logo__img" src="{{url('public/img/home/home-saas/eigerlab-36x36.png')}}" alt="Logo image">
                </div>
                <div class="logo__text-holder">
                  <h3 class="logo__text" style="text-align: center;">Eigerlab Technologies</h3>
                </div>
              </a>
            </div>
      </div>
     <!--  <?php
      // $fileData = file_get_contents('http://localhost/laravelwebsite/api/services');
      // $fileData = json_decode($fileData);
      //   dd($fileData);
      ?> -->
      <div class="getconbtnbx">
         <!-- <div class="callusSalesOpenbox dropdown">
            <div class="callusSales hideslidecalls dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-phone" aria-hidden="true"></i> </div>
            <div class="dropdown-menu">
               <div class="callusrow">
                  <div class="callusrtype sales"></div>
                  <div class="callusrdetails">
                     <div class="callheading">Sales</div>
                     <div class="callnum"><a href="tel:966-713-4400">+91 966-713-4400</a></div>
                     <div class="emailsrw"><a href="mailto:sales@techugo.com">sales@techugo.com</a></div>
                     <div class="callheading">HR</div>
                     <div class="callnum"><a href="tel:950-444-5188">+91 950-444-5188</a></div>
                  </div>
               </div>
            </div>
         </div>
         <a class="whatsappicon" href="https://wa.me/919667134400" target="_blank"></a>  -->
         <a href="{{URL::to('/contact')}}">
            <div class="button raised hoverable">
               <div class="anim"></div>
               <span>Contact Us!</span> 
            </div>
         </a>
      </div>
      <div id="navbar" class="navbar-collapse collapse js-navbar-collapse">
         <ul class="nav navbar-nav">
            <!-- <li class="hideview"><a href="{{URL::to('/contact')}}">Contact Us</a></li> -->
            <li class="fs28wd"><a href="{{URL::to('/about')}}">About Us</a></li>
            <li><a href="{{URL::to('/services')}}">Services Demo</a></li>

            <li class="dropdown mega-dropdown fs28wd">
               <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Services </a> <!--Services Menu Start Here--> 
               <ul class="dropdown-menu mega-dropdown-menu">
                  <div class="servicemenuwrap">
                     <div class="menuwrap">
                        <div class="row clearfix services-cols">
                           <!-- <div class="col-sm-4">
                              <ul>
                                <li><a href="#"><span class="icon iosdev"></span><span class="technm">UI/UX Design & Validation</span></a></li>
                                 <li><a href="#"><span class="icon androiddev"></span><span class="technm">1234567890</span></a></li>
                                 
                                 <li><a href="#"><span class="icon ionicdev"></span><span class="technm">Ionic <i>Development</i></span></a></li>
                                 <li><a href="#"><span class="icon blcdev"></span><span class="technm">Website Development</span></a></li>
                                 <li><a href="#"><span class="icon uiux"></span><span class="technm">UI/UX Design & Validation</span></a></li>
                                 <li><a href="#"><span class="icon augmentedreality"></span><span class="technm">Augmented Reality</span></a></li>
                              </ul>
                           </div>
                           <div class="col-sm-4">
                              <ul>
                                 <li><a href="#"><span class="icon phpnodedev"></span><span class="technm">SaaS</span></a></li>
                                 <li><a href="#"><span class="icon testing"></span><span class="technm">Testing & QA</span></a></li>
                                 <li><a href="#"><span class="icon reactdev"></span><span class="technm">Chatbot</span></a></li>
                                 <li><a href="#"><span class="icon wearabledev"></span><span class="technm">IOT</span></a></li>
                                 <li><a href="#"><span class="icon iotdev"></span><span class="technm">Testing & Quality Assurance</span></a></li>
                              </ul>
                           </div>
                           <div class="col-sm-4">
                              <ul>
                                 
                                 
                                 <li><a href="#"><span class="icon aidev"></span><span class="technm">SEO Packages<i></i></span></a></li>
                                 <li><a href="#"><span class="icon chatbotsdev"></span><span class="technm">Maintenance & Support</span></a></li>
                              </ul>
                           </div> -->
                        </div>
                         </div>
                  </div>
               </ul>
             </li>

            <li class="dropdown mega-dropdown fs28wd">
               <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Technologies </a> <!--Services Menu Start Here--> 
               <ul class="dropdown-menu mega-dropdown-menu">
                  <div class="servicemenuwrap">
                     <div class="menuwrap">
                        <div class="row clearfix techonologies-cols">
                           <!-- <div class="col-sm-4">
                              <ul>
                                <li><a href="#"><span class="icon iosdev"></span><span class="technm">iOS <i>Development</i></span></a></li>
                                 <li><a href="#"><span class="icon androiddev"></span><span class="technm">Android <i>Development</i></span></a></li>
                                 
                                 <li><a href="#"><span class="icon ionicdev"></span><span class="technm">Ionic <i>Development</i></span></a></li>
                                 <li><a href="#"><span class="icon blcdev"></span><span class="technm">Blockchain <i>Development</i></span></a></li>
                                 <li><a href="#"><span class="icon uiux"></span><span class="technm">UI/UX <i> Design</i></span></a></li>
                              </ul>
                           </div>
                           <div class="col-sm-4">
                              <ul>
                                 <li><a href="#"><span class="icon phpnodedev"></span><span class="technm">PHP/Node <i>JS Development</i></span></a></li>
                                 <li><a href="#"><span class="icon testing"></span><span class="technm">Testing & QA</span></a></li>
                                 <li><a href="#"><span class="icon reactdev"></span><span class="technm">React <i>Development</i></span></a></li>
                                 <li><a href="#"><span class="icon wearabledev"></span><span class="technm">Wearables</span></a></li>
                                 <li><a href="#"><span class="icon vrdev"></span><span class="technm">VR Development</span></a></li>
                              </ul>
                           </div>
                           <div class="col-sm-4">
                              <ul>
                                 <li><a href="#"><span class="icon augmentedreality"></span><span class="technm">Augmented <i>Reality</i></span></a></li>
                                 <li><a href="#"><span class="icon iotdev"></span><span class="technm">IOT</span></a></li>
                                 <li><a href="#"><span class="icon aidev"></span><span class="technm">AI<i></i></span></a></li>
                                 <li><a href="#"><span class="icon chatbotsdev"></span><span class="technm">Chatbots<i></i></span></a></li>
                              </ul>
                           </div> -->
                        </div>
                        <!-- <div class="socialmediaiconsrow">
                           <ul class="social-network social-circle">
                              <li><a href="https://in.linkedin.com/company/eigerlabtech" class="icoLinkedin" title="LinkedIn" target="_blank" data-pjax-state=""><i class="fa fa-linkedin"></i></a></li>
                              <li><a href="https://twitter.com/eigerlabtech" class="icoTwitter" title="Twitter" target="_blank" data-pjax-state=""><i class="fa fa-twitter"></i></a></li>
                              <li><a href="https://www.facebook.com/Eigerlabtech/" class="icoFacebook" title="Facebook" target="_blank" data-pjax-state=""><i class="fa fa-facebook"></i></a></li>
                              <li><a href="https://www.instagram.com/eigerlabtech/" class="icoinstagram" title="Instagram" target="_blank" data-pjax-state=""><i class="fa fa-instagram"></i></a></li>
                              <li><a href="https://dribbble.com/techugo" class="icodribble" title="Dribbble" target="_blank" data-pjax-state=""><i class="fa fa-dribbble"></i></a></li>
                              
                              <li><a href="#" class="icoyoutube" title="Youtube" target="_blank" data-pjax-state=""><i class="fa fa-youtube"></i></a></li>
                              <li><a href="#" class="icobehance" title="Behance" target="_blank" data-pjax-state=""><i class="fa fa-behance"></i></a></li>
                              <li><a href="#" class="icopinterest" title="Pinterest" target="_blank" data-pjax-state=""><i class="fa fa-pinterest"></i></a></li>
                              <li><a href="#" class="emails" title="Email" data-pjax-state=""><i class="fa fa-envelope"></i></a></li>
                           </ul>
                        </div> -->
                     </div>
                  </div>
               </ul>
               <!--Services Menu End Here--> 
            </li>
            <!-- <li><a href="#">Portfolio</a></li> -->
            <!-- <li><a href="#">Career</a></li> -->
            <!-- <li><a href="#">TechTalks</a></li> -->

            <!-- <li class="dropdown mega-dropdown fs28wd">
               <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Services 2</a>
               <ul class="dropdown-menu mega-dropdown-menu">
                  <div class="servicemenuwrap">
                     <div class="menuwrap">
                        <div class="row clearfix">
                           <div class="col-sm-6">
                              <ul>
                                <li><a href="#"><span class="icon iosdev"></span><span class="technm">iOS <i>Development</i></span></a></li>
                                 <li><a href="#"><span class="icon androiddev"></span><span class="technm">Android <i>Development</i></span></a></li>
                              </ul>
                           </div>
                           <div class="col-sm-6">
                              <ul>
                                 <li><a href="#"><span class="icon phpnodedev"></span><span class="technm">PHP/Node <i>JS Development</i></span></a></li>
                                 
                                 <li><a href="#"><span class="icon reactdev"></span><span class="technm">React <i>Development</i></span></a></li>
                                
                              </ul>
                           </div>
                        </div>
                        <div class="socialmediaiconsrow">
                           <ul class="social-network social-circle">
                              <li><a href="https://in.linkedin.com/company/eigerlabtech" class="icoLinkedin" title="LinkedIn" target="_blank" data-pjax-state=""><i class="fa fa-linkedin"></i></a></li>
                              <li><a href="https://twitter.com/eigerlabtech" class="icoTwitter" title="Twitter" target="_blank" data-pjax-state=""><i class="fa fa-twitter"></i></a></li>
                              <li><a href="https://www.facebook.com/Eigerlabtech/" class="icoFacebook" title="Facebook" target="_blank" data-pjax-state=""><i class="fa fa-facebook"></i></a></li>
                              <li><a href="https://www.instagram.com/eigerlabtech/" class="icoinstagram" title="Instagram" target="_blank" data-pjax-state=""><i class="fa fa-instagram"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </ul>
            </li> -->

            
            <!-- <li><a href="#">Technologies</a></li> -->
            <li><a href="{{URL::to('/portfolio')}}">Portfolio</a></li>
            <li><a href="{{URL::to('/blog')}}">Blog</a></li>

            <!-- <li><a href="#">Blog</a></li> -->
           <!--  <li class="dropdown mega-dropdown fs28wd">
               <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Blog </a>  -->
               <!--Blog Menu Start Here--> 
               <!-- <ul class="dropdown-menu mega-dropdown-menu">
                  <div class="servicemenuwrap">
                     <div class="menuwrap">
                        <div class="row clearfix">
                           <div class="col-sm-">
                              <ul>
                                 <li><a href="{{URL::to('/blog')}}"><span class="icon androiddev"></span><span class="technm">Blog <i></i></span></a></li>
                                 <li><a href="{{URL::to('/post')}}"><span class="icon reactdev"></span><span class="technm">Post <i></i></span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </ul> -->
               <!--Blog Menu End Here--> 
            <!-- </li> -->
            <!-- <li><a href="#">Career</a></li> -->
            <!-- <li><a href="{{URL::to('/contact')}}">Contact Us</a></li> -->

         </ul>
      </div>
   </div>
   <!-- <div class="overlaybg"></div> -->
   <!--Mobile Navigation Start Here-->
   <div class="overlay" id="overlay">
      <nav class="overlay-menu">
         <ul>
            <li><a href="{{URL::to('/contact')}}">Contact Us</a></li>
            <li><a href="{{URL::to('/about')}}">About Us</a></li>
            <li class="viewservicemenu">
               <a href="javascript:void(0);">Services <span class="fa fa-angle-down"></span></a> 
               <div class="servicemenuwrap">
                  <div class="menuwrap">
                     <ul>
                        <li><a href="#"><span class="icon iosdev"></span><span class="technm">iOS <i>Development</i></span></a></li>
                        <li><a href="#"><span class="icon androiddev"></span><span class="technm">Android <i>Development</i></span></a></li>
                        <li><a href="#"><span class="icon phpnodedev"></span><span class="technm">PHP/Node <i>JS Development</i></span></a></li>
                        <li><a href="#"><span class="icon reactdev"></span><span class="technm">React <i>Development</i></span></a></li>
                        <!-- <li><a href="#"><span class="icon ionicdev"></span><span class="technm">Ionic <i>Development</i></span></a></li>
                        <li><a href="#"><span class="icon blcdev"></span><span class="technm">Blockchain <i>Development</i></span></a></li>
                        <li><a href="#"><span class="icon uiux"></span><span class="technm">UI/UX <i> Design</i></span></a></li>
                        
                        <li><a href="#"><span class="icon testing"></span><span class="technm">Testing & QA</span></a></li>
                        
                        <li><a href="#"><span class="icon wearabledev"></span><span class="technm">Wearables</span></a></li>
                        <li><a href="#"><span class="icon vrdev"></span><span class="technm">VR Development</span></a></li>
                        <li><a href="#"><span class="icon augmentedreality"></span><span class="technm">Augmented <i>Reality</i></span></a></li>
                        <li><a href="#"><span class="icon iotdev"></span><span class="technm">IOT</span></a></li>
                        <li><a href="#"><span class="icon aidev"></span><span class="technm">AI<i></i></span></a></li>
                        <li><a href="#"><span class="icon chatbotsdev"></span><span class="technm">Chatbots<i></i></span></a></li> -->
                     </ul>
                  </div>
               </div>
            </li>
            <li style="clear:both"></li>
            <!-- <li><a href="#">Portfolio</a></li> -->
            <!-- <li><a href="#">Career</a></li> -->
            <!-- <li><a href="#">Blog</a></li> -->
            <!-- <li><a href="{{URL::to('/pricing')}}">Pricing</a></li> -->
            <!-- <li><a href="{{URL::to('/contact')}}">Contact Us</a></li> -->

            <li><a href="#">Features</a></li>
            <!-- <li><a href="#">Blog</a></li> -->
            <li style="clear:both"></li>
            <li class="viewservicemenu">
               <a href="javascript:void(0);">Blog <span class="fa fa-angle-down"></span></a> 
               <div class="servicemenuwrap">
                  <div class="menuwrap">
                     <ul>
                        <li><a href="{{URL::to('/blog')}}"><span class="icon iosdev"></span><span class="technm">Blog <i></i></span></a></li>
                        <li><a href="{{URL::to('/blog')}}"><span class="icon androiddev"></span><span class="technm">Post <i></i></span></a></li>
                     </ul>
                  </div>
               </div>
            </li>
            <li><a href="{{URL::to('/pricing')}}">Pricing</a></li>
            <li><a href="{{URL::to('/contact')}}">Contact Us</a></li>

         </ul>
      </nav>
   </div>
   <!--Mobile Navigation End Here--> 
  </nav>
    <!-- </div> -->


<!--Header Section End Here-->
    <!-- <div class="aheto-header-9__nav-wrap">
      <div class="aheto-header-9__inner">
        <div class="aheto-header-9__line">
          <div class="aheto-header-9__logo">
            <div class="logo">
              <a class="logo__link" href="">
                <div class="logo__img-holder">
                  <img class="logo__img" src="{{url('public/img/home/home-saas/eigerlab-36x36.png')}}" alt="Logo image">
                </div>
                <div class="logo__text-holder">
                  <h3 class="logo__text">Eigerlab</h3>
                </div>
              </a>
            </div>
          </div>
          <div class="aheto-header-9__menu js-menu js-menu-height">
            <ul class="main-menu padding-lg-5t padding-md-0t">
              <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item--mega-menu ">
                <a href="{{URL::to('/')}}">Home</a>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="{{URL::to('/about')}}">About</a>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                <a href="#">Features</a>
                <ul class="sub-menu menu-depth-1">
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/features')}}">Features saas</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/features_detals')}}">Features details saas</a></li>
                </ul>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                <a href="#">Blog</a>
                <ul class="sub-menu menu-depth-1">
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/blog')}}">Blog saas</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/post')}}">Post saas</a></li>
                </ul>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="{{URL::to('/pricing')}}">Pricing</a>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="{{URL::to('/contact')}}">Contact Us</a>
              </li>
            </ul>
            <div class="aheto-header-9__authentication">
              <div class="authentication">
                <button class="authentication__sign-in" type="button">Sign In</button>
                <button class="authentication__sign-up" type="button">Sign Up</button>
              </div>
            </div>
          </div>
          <div class="aheto-header-9__hamburger">
            <button class="hamburger hamburger--squeeze js-hamburger js-hamburger--body-over" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
          </div>
        </div>
      </div>
    </div> -->
  <!-- </header> -->

  @yield('content')

  <!-- </footer> -->
  <footer class="aheto-footer aheto-footer-9">
    <div class="aheto-footer-9__main">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-lg-3">
            <div class="widget widget_aheto ">
              <div class="widget_aheto__logo">
                <img src="{{url('public/img/home/home-saas/eigerlab_image.png')}}" alt="footer">
              </div>
              <div class="widget_aheto__infos">
                <p class="widget_aheto__info widget_aheto__info--address">
                  <!-- 3rd floor, 81, Block A, Sector 4, Noida, Uttar Pradesh. -->
                  Office No – 09 , Top Floor, A – 81 Sector – 4 Noida, Uttar Pradesh.
                </p>
                <p class="widget_aheto__info widget_aheto__info--mail">
                  <a class="widget_aheto__link" href="info@eigerlabtech.com">info@eigerlabtech.com</a>
                </p>
                <p class="widget_aheto__info widget_aheto__info--tel">
                  <a class="widget_aheto__link" href="tel:(855)233-5385">(855) 233-5385</a>
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-3">
            <div class="widget widget_nav_menu widget_nav_menu_">
              <h2 class="widget-title">Services</h2>
              <div class="menu-main-container footerServices">
                <ul class="menu">
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Resources</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Podcast</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Help Center</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Terms &amp; Privacy</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Pricing</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Categories</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-3">
            <div class="widget widget_nav_menu widget_nav_menu_">
              <h2 class="widget-title">Technologies</h2>
              <div class="menu-main-container footerTechnologies">
                <ul class="menu">
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">FAQ</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Guides &amp; Tutorials</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Case Studies</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Webinars</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Tweet @ Us</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Download</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-3">
            <div class="widget widget_mc4wp_form_widget aheto_mc_3">
              <h2 class="widget-title">Get In Touch</h2>
              <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-223" method="post" data-id="223" data-name="Email">
                <div class="mc4wp-form-fields"><input type="email" name="EMAIL" placeholder="Your Email" required="">
                  <button type="submit">
                    <i class="icon ion-arrow-right-c"></i>
                  </button>
                </div>
                <label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></label><input type="hidden" name="_mc4wp_timestamp" value="1526996980"><input
                  type="hidden" name="_mc4wp_form_id" value="223"><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1">
                <div class="mc4wp-response"></div>
              </form>
              <!-- / MailChimp for WordPress Plugin -->
            </div>
            <!-- <div class="aht-socials ">
              <a class="aht-socials__link aht-btn--dark aht-btn--trans " href="https://in.linkedin.com/company/eigerlabtech">
                <i class="aht-socials__icon icon ion-social-linkedin"></i>
              </a>
              <a class="aht-socials__link aht-btn--dark aht-btn--trans " href="https://twitter.com/eigerlabtech">
                <i class="aht-socials__icon icon ion-social-twitter"></i>
              </a>
              <a class="aht-socials__link aht-btn--dark aht-btn--trans " href="https://www.facebook.com/Eigerlabtech/">
                <i class="aht-socials__icon icon ion-social-facebook"></i>
              </a>
              <a class="aht-socials__link aht-btn--dark aht-btn--trans " href="https://www.instagram.com/eigerlabtech/">
                <i class="aht-socials__icon icon ion-social-instagram"></i>
              </a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <hr style="border-bottom-style: inset;border-bottom-width: 4px;">
    <!-- <hr class="d-lg-block"> -->
    <div class="aheto-footer-9__bottom">
      <div class="container">
        <div class="row">
          
          <div class="col-md-6 col-lg-5">
            <div class="aheto-footer-menu margin-lg-20t margin-lg-30b margin-sm-0t">
              <nav class="menu-main-container">
                <ul class="main-menu">
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="{{URL::to('/about')}}">About Us</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="{{URL::to('/contact')}}">Contact Us</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="{{URL::to('/career')}}">Career</a></li>
                  <li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="#">Privacy</a></li>
                  
                </ul>
              </nav>
            </div>
          </div>
          <div class="col-md-6 col-lg-7" style="text-align: right;">
            <!-- <div class="aheto-footer-9__cr margin-lg-20t margin-lg-30b margin-sm-0b">
              <p class="aheto-footer-9__cr-text ">© <a href="{{URL::to('/')}}">Eigerlab Technologies</a>. All Right Reserved {{date('Y')}}.</p>
            </div> -->

            <ul class="social-network social-circle">
              <li><a href="https://in.linkedin.com/company/eigerlabtech" class="icoLinkedin" title="LinkedIn" target="_blank" data-pjax-state=""><i class="fa fa-linkedin"></i></a></li>
              <li><a href="https://twitter.com/eigerlabtech" class="icoTwitter" title="Twitter" target="_blank" data-pjax-state=""><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://www.facebook.com/Eigerlabtech/" class="icoFacebook" title="Facebook" target="_blank" data-pjax-state=""><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://www.instagram.com/eigerlabtech/" class="icoinstagram" title="Instagram" target="_blank" data-pjax-state=""><i class="fa fa-instagram"></i></a></li>
              <!-- <li><a href="https://dribbble.com/techugo" class="icodribble" title="Dribbble" target="_blank" data-pjax-state=""><i class="fa fa-dribbble"></i></a></li>
                              
              <li><a href="#" class="icoyoutube" title="Youtube" target="_blank" data-pjax-state=""><i class="fa fa-youtube"></i></a></li>
              <li><a href="#" class="icobehance" title="Behance" target="_blank" data-pjax-state=""><i class="fa fa-behance"></i></a></li>
              <li><a href="#" class="icopinterest" title="Pinterest" target="_blank" data-pjax-state=""><i class="fa fa-pinterest"></i></a></li>
              <li><a href="#" class="emails" title="Email" data-pjax-state=""><i class="fa fa-envelope"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <div class="site-search" id="search-box">
    <button class="close-btn js-close-search"><i class="fa fa-times" aria-hidden="true"></i></button>
    <div class="form-container">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <form role="search" method="get" class="search-form" action="http://ahetopro/" autocomplete="off">
              <div class="input-group">
                <input type="search" value="" name="s" class="search-field" placeholder="Enter Keyword" required="">
              </div>
            </form>
            <p class="search-description">Input your search keywords and press Enter.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--......Auto scroll Up-->
  <div class="btn btn-primary scroll-top" data-scroll="up" type="button" style="background-color: #000000; left: 1450px; padding-left: 19px; padding-top: 5px; border-radius: 50%; font-size: 38px;">
    <i class="fa fa-chevron-up"></i>
  </div>

  <!-- <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5cb2ba4ad6e05b735b42764e/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script> -->

  </body>
  <script>
    $(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }
    });

    $('.scroll-top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    });
  </script>
  <script>
    // get services data 

        var serviceArray = [];
        var technologyArray = [];

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/services',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                var url = "{{URL::to('/services')}}" + res.url;
                // console.log(url);
                serviceArray.push('<div class="col-sm-4"><ul><li><a href="'+ url +'"><span class="icon blcdev"><img src="'+ res.icon +'" class="img-icon"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
                })
                $('.services-cols').html(serviceArray);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });

        // get technologies data

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/technologies',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
                // response.map(res => {
                //     technologyArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
                // })
                response.map(res => {
                    var url = "{{URL::to('/technology')}}" + res.url;
                    technologyArray.push('<div class="col-sm-4"><ul><li><a href="'+ url +'"><span class="icon blcdev"><img src="'+ res.icon +'" class="img-icon"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
                })
                $('.techonologies-cols').html(technologyArray);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });
  </script>

  <script>
        // get services data for footer

        var footerServiceArray = [];
        var footerTechnologyArray = [];

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/services',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              // response.map(res => {
              //   serviceArray.push('<div class="col-sm-4"><ul><li><a href="#"><span class="icon blcdev"></span><span class="technm">'+ res.title +'</span></a></li></ul></div>');
              //   })
              response.map(res => {
                var url = "{{URL::to('/services')}}" + res.url;
                // console.log(url);
                footerServiceArray.push('<ul class="menu"><li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="'+ url +'">'+ res.title +'</a></li><li></li></ul>');
                })
                $('.footerServices').html(footerServiceArray);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });

        //get technologies data

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/technologies',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
                
                response.map(res => {
                    var url = "{{URL::to('/technology')}}" + res.url;
                    footerTechnologyArray.push('<ul class="menu"><li class="menu-item  menu-item-type-post_type menu-item-object-page"><a href="'+ url +'">'+ res.title +'</a></li><li></li></ul>');
                })
                $('.footerTechnologies').html(footerTechnologyArray);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });
  </script>
</html>