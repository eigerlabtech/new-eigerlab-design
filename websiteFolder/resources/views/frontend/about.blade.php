@extends('frontend.master')

<!-- @section('css')
<style type="text/css">
  .aheto-content-block__info .heading-about-us{
    font-size: 40px;

  }
</style>
@stop -->
@section('content')

<!-- <link rel="stylesheet" type="text/css" href="{{URL::to('public/css/about/bootstrap.min.css')}}">

<script type="text/javascript" src="{{URL::to('public/js/about/bootstrap.min.js')}}"></script> -->

<!-- <style>
* {box-sizing: border-box;}
body {font-family: Verdana, sans-serif;}
.mySlides {display: none;}
img {vertical-align: middle;}

 Slideshow container 
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

 Caption text 
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

 Number text (1/3 etc) 
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

 The dots/bullets/indicators 
.dot {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 4s ease;
}

.active {
  background-color: #717171;
}

 Fading animation 
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

 On smaller screens, decrease text size 
@media only screen and (max-width: 300px) {
  .text {font-size: 11px}
}
</style> -->

<style>
  body
  {overflow-x: hidden
    background-color: #343434}

.quote-slider
  {width: 100%
    height: 400px
    background-color: #0094de
    list-style: none
    display: block
    position: relative}
  .quote-slider > li
    {display: inline-block
        position: absolute
        top: 50%
        transform: translateY(-50%)
        right: -60%
        width: 60%
        font-family: 'Crimson Text', serif
        font-weight: 700
        font-size: 48px
        color: #fff
        text-shadow: 2px 2px 0px #0083cd
        font-style: italic
        text-align: center
        z-index: 5}
    .site
      {font-family: 'Oswald', sans-serif
            font-style: normal
            margin-top: 40px
            font-size: 36px}
  .arrow
    {position: absolute
        top: 0
        width: 70px
        color: #fff
        font-family: 'Oswald', sans-serif
        font-weight: 700
        font-size: 100px
        height: 100%
        line-height: 400px
        padding-left: 40px}
  .arrow.right
    {right: 0}
  .arrow.left
    {left: 0}
    
    
</style>



  <!-- <header class="aheto-header aheto-header-9">
    <div class="aheto-header-9__nav-wrap">
      <div class="aheto-header-9__inner">
        <div class="aheto-header-9__line">
          <div class="aheto-header-9__logo">
            <div class="logo">
              <a class="logo__link" href="">
                <div class="logo__img-holder">
                  <img class="logo__img" src="{{url('public/img/home/home-saas/logo-1.png')}}" alt="Logo image">
                </div>
                <div class="logo__text-holder">
                  <h3 class="logo__text">Eigerlab</h3>
                </div>
              </a>
            </div>
          </div>
          <div class="aheto-header-9__menu js-menu js-menu-height">
            <ul class="main-menu padding-lg-5t padding-md-0t">
              <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item--mega-menu ">
                <a href="{{URL::to('/')}}">Home</a>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="{{URL::to('/about')}}">About</a>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                <a href="#">Features</a>
                <ul class="sub-menu menu-depth-1">
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/features')}}">Features saas</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/features_detals')}}">Features details saas</a></li>
                </ul>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                <a href="#">Blog</a>
                <ul class="sub-menu menu-depth-1">
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/blog')}}">Blog saas</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{URL::to('/post')}}">Post saas</a></li>
                </ul>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="{{URL::to('/pricing')}}">Pricing</a>
              </li>
              <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="{{URL::to('/contact')}}">Contact Us</a>
              </li>
            </ul>
            <div class="aheto-header-9__authentication">
              <div class="authentication">
                <button class="authentication__sign-in" type="button">Sign In</button>
                <button class="authentication__sign-up" type="button">Sign Up</button>
              </div>
            </div>
          </div>
          <div class="aheto-header-9__hamburger">
            <button class="hamburger hamburger--squeeze js-hamburger js-hamburger--body-over" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
          </div>
        </div>
      </div>
    </div>
  </header> -->
  <section class="aheto-banner aheto-banner--height-450 aheto-banner--height-mob-200 aheto-banner--bg-position-btm" style="background-image: url('public/img/SAAS/banner_bg.png')">
    <div class="container d-flex h-100 justify-content-center align-items-center">
      <div class="row padding-lg-60t padding-sm-0t">
        <div class="aheto-heading t-center aheto-heading--t-white ">
          <h1 class="aheto-heading__title          t-light ">About Us</h1>
        </div>
      </div>
    </div>
  </section>

  <div class="container margin-lg-50t margin-lg-80b margin-md-70t margin-sm-50t margin-sm-20b">
    <div class="row row--flex row--v-center ">
      <div class="col-sm-12 col-md-6 col-xs-12 sm-t-center margin-xs-20b">
        <img src="{{url('public/img/SAAS/about_img.png')}}" alt="" class="w-100">
      </div>
      <div class="col-lg-5 offset-lg-1 col-md-6 col-sm-12 col-xs-12">
        <div>
          <div class="aheto-heading t-left sm-t-center aheto-heading">
            <!-- <h6 class="aheto-heading__subtitle t-bold text-color--grey f-14 let-spasing">ABOUT SAAS</h6> -->
            <h2 class="aheto-heading__title          t-light ">What We Are<!--  <span>analysis services.</span> --></h2>
          </div>
        </div>
        <div class="margin-lg-25t">
          <div class="aheto-heading t-left sm-t-center aheto-heading">
            <p class="aheto-heading__title  l-height--163" style="font-size: 20px;">Eigerlab Technologies is Mobile Application, website development, UX/UI and Quality Assurance firm. We use cutting edge software development methodology for various small & large enterprises across all verticals. Whether you are startup or an Enterprise, with a young creative and Innovation team we create product which are truly memorable and create such a digital experience which will exceed client expectations.</p>
            <p class="aheto-heading__title  l-height--163" style="font-size: 20px;">
              We have deep expertise our domain and we believe in providing end-to-end enterprise technology solutions. with our extremely innovative, dedicated team we develop robust & scalable solutions that helped our clients – overcome all of their technology and business related hurdles. We follow Agile methodology, It has been refined and optimized over a period of time and experience which we learned from industry to deliver product on time and in cost effective manner.
            </p>
          </div>
        </div>
        <!-- <div class="margin-lg-25t">
          <div class="aheto-heading t-left sm-t-center aheto-heading">
            <p class="aheto-heading__title       l-height--163    ">Thanks to the commonwealth of Virginia, no matter where you live in the U.S., your documents can now be notarized online by a trusted notary. Upload and annotate any document,
              verify your identity, and connect with an eNotary in minutes. All handled securely through the Notarize app. Once done, print or share your notarized document electronically.</p>
          </div>
        </div> -->
        <!-- <div class="margin-lg-45t margin-lg-40t margin-lg-30b sm-t-center">
          <a href="https://www.youtube.com/watch?time_continue=2&amp;v=YMurlapfouo" class="aheto-video-link js-mfp-video ">
            <span class="aheto-video-link__btn ">
              <i class="ion-arrow-right-b"></i>
            </span>
            <span class="aheto-video-link__label">watch the video</span>
          </a>
        </div> -->
      </div>
    </div>
  </div>
  <hr class="d-lg-block">

  <!-- <div class="container-fluid">
  <div class="slideshow-container">

<div class="mySlides fade">
  <div class="numbertext">1 / 3</div>
  <img src="{{URL::to('public/img/sedna/blog-img-01.jpg')}}" style="width:100%">
  <div class="text">Caption Text</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">2 / 3</div>
  <img src="{{URL::to('public/img/sedna/blog-img-02.jpg')}}" style="width:100%">
  <div class="text">Caption Two</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">3 / 3</div>
  <img src="{{URL::to('public/img/sedna/blog-img-03.jpg')}}" style="width:100%">
  <div class="text">Caption Three</div>
</div>

</div>
<br>

<div style="text-align:center">
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
</div>
</div> -->

<!-- <ul class="quote-slider">
  <li id='quote0' style='right: 20%'>
    "Working with them was literally terrible"
    <div class="site">Elon Musk</div>
  </li>
  <li id='quote1'>
    "They sucked and I hate them."
    <div class="site">Everyone Ever</div>
  </li>
  <li id='quote2'>
    "Frankly I don't understand why anyone would ever work with them"
    <div class="site">Arnold Schwarzenegger</div>
  </li>
  <li id='quote3'>
    "You have no friends. Go away."
    <div class="site">Bill "Le Master Troll" Gates</div>
  </li>
  <li id='quote4'>
    "No, Half Life 3 is never coming out."
    <div class="site">Our Lord and Savior, Gabe Newell</div>
  </li>
</ul>

<hr class="d-lg-block"> -->

  <section>
    <div class="container margin-lg-105t margin-md-75t margin-sm-50t">
      <div class="row">
        <div class="col-sm-6 offset-sm-3 retreat">
          <div class="aheto-heading t-center aheto-heading">
            <!-- <h6 class="aheto-heading__subtitle t-bold text-color--grey f-14 let-spasing">HOW IT WORKS</h6> -->
            <h1 class="aheto-heading__title          t-light ">Why choose <span class="spcl_text"> EigerlabTech?</span></h1>
            <!-- <h2 class="aheto-heading__title          t-light ">All you need is a webcam, Valid ID, and your document</h2> -->
          </div>
        </div>
      </div>
      <div class="row margin-lg-75t margin-md-40t margin-sm-20t margin-lg-100b margin-md-60b margin-sm-30b">
        <div class="col-sm-4">
          <div class="aheto-content-block aheto-content-block--saas t-center  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <!-- <i class="aheto-content-block__ico icon ion-ios-cloud-upload-outline">
                </i> -->
              </div>
              <div class="aheto-content-block__info">
                <img src="{{URL::to('public/img/saas/member_1.png')}}" alt="Mobile App development experts">
                <h5 class="heading-about-us" style="font-size: 20px;">Co-Founder with no Equity</h5>
                <!-- <p class="aheto-content-block__info-text ">Upload your document and prove<br> your identity.</p> -->
                <p class="aheto-content-block__info-text ">Our extensive interest and dedication in your app, as co-founders, is without equity. From the beginning, and through the app’s lifetime, we offer our assistance at every stage of your startup.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="aheto-content-block aheto-content-block--saas t-center  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <!-- <i class="aheto-content-block__ico icon ion-ios-camera-outline">
                </i> -->
              </div>
              <div class="aheto-content-block__info">
                <img src="{{URL::to('public/img/saas/member_1.png')}}" alt="design implementation">
                <h5 class="heading-about-us" style="font-size: 20px;">Cutting Edge Designs</h5>
                <!-- <p class="aheto-content-block__info-text ">Connect over live video<br> with a notary public.</p> -->
                <p class="aheto-content-block__info-text ">An app is as good as the design that is implemented, and its fate is decided by the user experience that thrives on a strong backend support. As evident through our past work, we focus on designs that meet market trends and your business expectations at every critical front.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="aheto-content-block aheto-content-block--saas t-center  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <!-- <i class="aheto-content-block__ico icon ion-ios-list-outline">
                </i> -->
              </div>
              <div class="aheto-content-block__info">
                <img src="{{URL::to('public/img/saas/member_1.png')}}" alt="Find a Growth Hacker for Your Startup">
                <h5 class="heading-about-us" style="font-size: 20px;">User Acquisition via Growth Hacking</h5>
                <!-- <p class="aheto-content-block__info-text ">Sign and Notarize<br> in five minutes flat!</p> -->
                <p class="aheto-content-block__info-text ">We work around the principle of ‘Growth First, Budget Second’ that has been curated with a lean approach to maximise profits through low cost yet innovative ideas, all suited to meet your business requirements.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row margin-lg-75t margin-md-40t margin-sm-20t margin-lg-100b margin-md-60b margin-sm-30b">
        <div class="col-sm-4">
          <div class="aheto-content-block aheto-content-block--saas t-center  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <!-- <i class="aheto-content-block__ico icon ion-ios-cloud-upload-outline">
                </i> -->
              </div>
              <div class="aheto-content-block__info">
                <img src="{{URL::to('public/img/saas/member_1.png')}}" alt="innovative ideas">
                <h5 class="heading-about-us" style="font-size: 20px;">Free Support for 3 Months</h5>
                <!-- <p class="aheto-content-block__info-text ">Upload your document and prove<br> your identity.</p> -->
                <p class="aheto-content-block__info-text ">Without any hidden costs or surprises, we help you with consistent assistance and guidance for 3 months. Our comprehensive approach coupled with your business outlook helps your business on its path to unparalleled success.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="aheto-content-block aheto-content-block--saas t-center  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <!-- <i class="aheto-content-block__ico icon ion-ios-camera-outline">
                </i> -->
              </div>
              <div class="aheto-content-block__info">
                <img src="{{URL::to('public/img/saas/member_1.png')}}" alt="App guidance and help">
                <h5 class="heading-about-us" style="font-size: 20px;">Flat Fees</h5>
                <!-- <p class="aheto-content-block__info-text ">Connect over live video<br> with a notary public.</p> -->
                <p class="aheto-content-block__info-text ">Each app is built with enough room to inculcate future prospects, and during the price lock down, we ensure that the app has been customised for a great business value. All the required changes are done without any additional payments.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="aheto-content-block aheto-content-block--saas t-center  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <!-- <i class="aheto-content-block__ico icon ion-ios-list-outline">
                </i> -->
              </div>
              <div class="aheto-content-block__info">
                <img src="{{URL::to('public/img/saas/member_1.png')}}" alt="Good Support for customer">
                <h5 class="heading-about-us" style="font-size: 20px;">24/7 Communication</h5>
                <!-- <p class="aheto-content-block__info-text ">Sign and Notarize<br> in five minutes flat!</p> -->
                <p class="aheto-content-block__info-text ">We love hearing from you and addressing your grievances, and you can reach out to us at any instant suited to your convenience through the mode of communication of your choice.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- <section style="background-color: #f3f9ff;" class="padding-lg-40b padding-sm-10b">
    <div class="container">
      <div class="row padding-lg-125t padding-md-75t padding-sm-50t margin-lg-60b margin-md-0b">
        <div class="col-md-12 margin-lg-70b margin-md-50b margin-sm-30b">
          <div class="aheto-heading t-center aheto-heading">
            <h6 class="aheto-heading__subtitle t-bold text-color--grey f-14 text-uppercase let-spasing">team members</h6>
            <h2 class="aheto-heading__title          t-light ">Get to know <span>amazing people</span></h2>
          </div>
        </div>
        <div class="col-sm-12 margin-md-30b">
          <div class="swiper swiper--saas ">
            <div class="swiper-container " data-speed="500" data-spaceBetween="30" data-slidesPerView="responsive" data-add-slides="4" data-lg-slides="4" data-md-slides="4" data-sm-slides="3" data-xs-slides="1">
              <div class="swiper-wrapper ">
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_1.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">Morgan Guadis</h5>
                      <p class="aheto-member__position ">Designer</p>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_2.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">John Senating</h5>
                      <p class="aheto-member__position ">Director</p>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_3.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">Sandi Hormez</h5>
                      <p class="aheto-member__position ">Director</p>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_4.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">Morgan Guadis</h5>
                      <p class="aheto-member__position ">Designer</p>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_1.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">Morgan Guadis</h5>
                      <p class="aheto-member__position ">Designer</p>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_2.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">John Senating</h5>
                      <p class="aheto-member__position ">Designer</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-button-prev">
            </div>
            <div class="swiper-button-next">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->




  <!-- <div class="about-counter-wrapp" style="background-color: #fff8f7;">
    <div class="container">
      <div class="row">
        <div class="col-md-8 offset-md-2 margin-lg-55b margin-md-35b">
          <div class="aheto-heading t-center ">
            <h2 class="aheto-heading__title          t-light ">Completely disintermediate excellent <br>skills with cient contents</h2>
          </div>
        </div>
      </div>
      <div class="row margin-lg-30t margin-sm-0t">
        <div class="col-sm-4 margin-sm-30b">
          <div class="aheto-counter aheto-counter--classic ">
            <i class="aheto-counter__icon icon ti-cloud-down"></i>
            <h6 class="aheto-counter__number  js-counter  ">1000</h6>
            <p class="aheto-counter__desc">You can find our code well organized and readable.</p>
          </div>
        </div>
        <div class="col-sm-4 margin-sm-30b">
          <div class="aheto-counter aheto-counter--classic ">
            <i class="aheto-counter__icon icon ti-bookmark"></i>
            <h6 class="aheto-counter__number  js-counter  ">6500</h6>
            <p class="aheto-counter__desc">You can find our code well organized and readable.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="aheto-counter aheto-counter--classic ">
            <i class="aheto-counter__icon icon ti-cup"></i>
            <h6 class="aheto-counter__number  js-counter  ">200</h6>
            <p class="aheto-counter__desc">You can find our code well organized and readable.</p>
          </div>
        </div>
      </div>
    </div>
  </div> -->

  <div class="about-counter-wrapp" style="background-color: #fff8f7;">
    <div class="container">
      <div class="row">
        <div class="col-md-8 offset-md-2 margin-lg-55b margin-md-35b">
           <?php
          foreach($section3_heading as $heading){
          // dd($heading);
            ?>
          <div class="aheto-heading t-center ">
            <h2 class="aheto-heading__title          t-light " style='font-family: "Times New Roman", Times, serif; font-size:      40px;'>{{$heading['section3_heading']}}</h2>
          </div>
          <?php }?>
        </div>
      </div>

      <div class="row margin-lg-30t margin-sm-0t achievements">
        <?php
          for($i=0; $i<count($achievements); $i++){
        ?>
        <div class="col-sm-3 margin-sm-30b">
          <div class="aheto-counter aheto-counter--classic ">
            <!-- <i class="aheto-counter__icon icon ti-cloud-down"></i> -->
            <h6 class="aheto-counter__number  js-counter  ">{{$achievements[$i]['section3_number']}}</h6>
            <p class="aheto-counter__desc">{{$achievements[$i]['section3_tagline']}}</p>
          </div>
        </div>
        <?php }?>
      </div>
    </div>
  </div>


  <!-- <section class="aheto-banner aheto-banner--height-520 aheto-banner--height-mob-400" style="background-image: url('{{url('public/img/SAAS/pricing_bg.png')}}');">
    <div class="container h-100 d-flex fl-column justify-content-center">
      <div class="row">
        <div class="col-md-12">
          <div class="aheto-heading t-center aheto-heading--t-white">
            <h5 class="aheto-heading__title          t-light f-18 ">Your landing page. Reinvigorated.</h5>
          </div>
        </div>
        <div class="col-md-12 margin-lg-40t margin-lg-40b">
          <div class="aheto-heading t-center aheto-heading--t-white">
            <h2 class="aheto-heading__title          t-light ">We’re always looking for talent</h2>
          </div>
        </div>
        <div class="col-md-12 margin-lg-40b">
          <div class="aheto-btn-container  t-center md-t-center">
            <a href="#" style="" class="aheto-btn    aheto-btn--alter  aheto-btn--shadow  ">
              JOIN US NOW
            </a>
          </div>
        </div>
        <div class="col-md-12 t-center margin-lg-0t">
          <a href="" class="aheto-link aheto-link--white aheto-btn--nobg">
            Explore the features
            <i class="aheto-link__icon ion-arrow-right-c"></i>
          </a>
        </div>
      </div>
    </div>
  </section> -->
  <!-- <section>
    <div class="container margin-lg-165t margin-lg-160b margin-md-45t margin-md-70b margin-sm-15t margin-sm-45b">
      <div class="row">
        <div class="swiper swiper--saas ">
          <div class="swiper-container " data-speed="500" data-centeredSlides="true" data-spaceBetween="30" data-slidesPerView="responsive" data-add-slides="1" data-lg-slides="1" data-md-slides="1" data-sm-slides="1" data-xs-slides="1">
            <div class="swiper-wrapper ">
              <div class="swiper-slide ">
                <div class="aheto-tm-slider text-center">
                  <h6 class="aheto-tm-slider__label t-bold text-color--grey f-14 text-uppercase let-spasing testemonial-quote padding-lg-55t padding-lg-40b">testimonials</h6>
                  <h3 class="aheto-tm-slider__text t-light">“When you innovate, you make mistakes.It is best to admit them quickly, &amp; get on with improving your other innovations.”</h3>
                  <img src="{{url('public/img/SAAS/testimonials_1.png')}}" alt="" class="aheto-tm-slider__img margin-lg-45t">
                  <h6 class="aheto-tm-slider__name t-medium margin-lg-25t">Sandi Hormez</h6>
                </div>
              </div>
              <div class="swiper-slide ">
                <div class="aheto-tm-slider text-center">
                  <h6 class="aheto-tm-slider__label t-bold text-color--grey f-14 text-uppercase let-spasing testemonial-quote padding-lg-55t padding-lg-40b">testimonials</h6>
                  <h3 class="aheto-tm-slider__text t-light">“When you innovate, you make mistakes.It is best to admit them quickly, &amp; get on with improving your other innovations.”</h3>
                  <img src="{{url('public/img/SAAS/testimonials_1.png')}}" alt="" class="aheto-tm-slider__img margin-lg-45t">
                  <h6 class="aheto-tm-slider__name t-medium margin-lg-25t">Sandi Hormez</h6>
                </div>
              </div>
              <div class="swiper-slide ">
                <div class="aheto-tm-slider text-center">
                  <h6 class="aheto-tm-slider__label t-bold text-color--grey f-14 text-uppercase let-spasing testemonial-quote padding-lg-55t padding-lg-40b">testimonials</h6>
                  <h3 class="aheto-tm-slider__text t-light">“When you innovate, you make mistakes.It is best to admit them quickly, &amp; get on with improving your other innovations.”</h3>
                  <img src="{{url('public/img/SAAS/testimonials_1.png')}}" alt="" class="aheto-tm-slider__img margin-lg-45t">
                  <h6 class="aheto-tm-slider__name t-medium margin-lg-25t">Sandi Hormez</h6>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-button-prev">
          </div>
          <div class="swiper-button-next">
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <!-- <div style="background-color: #f7f7f7;">

    <div class="container">

      <div class="row justify-content-center">
        <div class="padding-lg-25t padding-lg-20b">
          <p style="text-align: center;font-size: 50px;">Trusted Clients</p>
          <div class="aheto-clients aheto-clients--5-in-row aheto-clients--low-opacity">
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client1.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client2.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client3.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client4.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client6.png')}}" alt="Clients logo image">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->

  <div style="background-color: #f7f7f7;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="padding-lg-25t padding-lg-20b">
          <div class="aheto-clients aheto-clients--5-in-row aheto-clients--low-opacity">
            <?php
              foreach($clients_info as $client){
            // dd($client);
                
            ?>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{$client['logo']}}" alt="Clients logo image">
              </a>
            </div>
            <?php }?>

            <!-- <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client2.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client3.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client4.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client6.png')}}" alt="Clients logo image">
              </a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- <section class="aheto-banner aheto-banner--height-500 d-flex aheto-banner--promo" style="background-image: url('{{url('public/img/SAAS/banner_bg-3.png')}}')">
    <div class="container margin-lg-35t padding-lg-60b d-flex">
      <div class="row w-100 align-items-center">
        <div class="col-md-5 col-xs-12 md-t-center">
          <div class="aheto-heading t-left md-t-center aheto-heading--t-white">
            <h2 class="aheto-heading__title          t-light ">Download the Inbox app to get started.</h2>
          </div>
          <div class="aheto-banner__rating margin-lg-50t margin-sm-15t">
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
          </div>
          <div class="aheto-heading t-left md-t-center aheto-heading--t-white">
            <p class="aheto-heading__title          t-light ">Base on <b>2,000+ reviews</b></p>
          </div>
        </div>
        <div class="col-md-6 offset-md-1 col-xs-12 t-right md-t-center margin-lg-35t margin-sm-15t">
          <div class="aheto-banner__store margin-lg-25b">
            <a href="https://www.apple.com/lae/ios/app-store/" target="_blank"><img alt="" src="{{url('public/img/SAAS/app-store.png')}}"></a>
            <a href="https://play.google.com/" target="_blank"><img src="{{url('public/img/SAAS/google-play.png')}}" alt=""></a>
          </div>
          <div class="aheto-heading t-right md-t-center aheto-heading--t-white">
            <p class="aheto-heading__title          t-light ">Also available on the web at <b>inbox.google.com.</b></p>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  
  <!-- Magnific popup -->
  <script src="{{url('public/vendors/magnific/jquery.magnific-popup.min.js')}}"></script>
  <!-- anm -->
  <script src="{{url('public/vendors/animation/anm.min.js')}}"></script>
  <!-- Google maps -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARwCmK-LlGIH8Mv1ac4VyceMYUgg9vStM&amp;#038;&language=en"></script>
  <script src="{{url('public/vendors/googlemap/google-maps.js?v=1')}}"></script>
  <!-- FullCalendar -->
  <!-- Parallax -->
  <script src="{{url('public/vendors/parallax/parallax.min.js')}}"></script>
  <!-- asRange -->
  <script src="{{url('public/vendors/range/jquery.range-min.js')}}"></script>
  <!-- lightgallery -->
  <script src="{{url('public/vendors/lightgallery/lightgallery.min.js')}}"></script>
  <!-- Main script -->
  <script src="{{url('public/vendors/script.js?v=1')}}"></script>
  <script src="{{url('public/vendors/spectragram/spectragram.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      jQuery.fn.spectragram.accessData = {
        accessToken: '4058508404.1677ed0.f87c0182df0d4512a9e01def0c53adb7'
      }

      $('.instafeed').spectragram('getUserFeed', {
        size: 'big',
        max: 6
      });
    });
  </script>
  <!-- <script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script> -->


                                  <!-- WORKING SCRIPT FOR SLIDE SHOW OF IMAGES-->
<!-- <script>
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 2000); // Change image every 2 seconds
}
</script> -->
                                  <!-- WORKING SCRIPT END FOR SLIDE SHOW OF IMAGES -->
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>
@stop