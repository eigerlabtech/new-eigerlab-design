@extends('frontend.master')

@section('css')

<!-- <link href='https://fonts.googleapis.com/css?family=Raleway:400,200' rel='stylesheet' type='text/css' >
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> -->
@stop
@section('content')
  
  <!-- <link rel="stylesheet" href="{{URL::to('public/css/social_icon.css')}}"> -->
  <?php
          foreach($home_page_data as $homePage){
          // dd($homePage);
            ?>
  <!-- <section class="aheto-banner aheto-banner--full-height d-flex aheto-banner--saas d-flex" style="background-image: url('{{$homePage['section1_banner']}}')"> -->
    <section class="aheto-banner aheto-banner--full-height d-flex aheto-banner--saas d-flex" style="background-image: url('{{$homePage['section1_banner']}}')">
    
    <!-- <nav class="sociala" >
          <ul>
            <li><a href="https://www.facebook.com/Eigerlabtech/" target="_blank" >Facebook <i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/eigerlabtech" target="_blank">Twitter <i class="fa fa-twitter"></i></a></li>
            <li><a href="https://in.linkedin.com/company/eigerlabtech" target="_blank">Linkedin <i class="fa fa-linkedin"></i></a></li>
           <li><a href="https://www.instagram.com/eigerlabtech/" target="_blank">Instagram <i class="fa fa-instagram"></i></a></li>
          </ul>
  </nav> -->
    <div class="container padding-lg-90t padding-sm-15t">
      <div class="row">
        <div class="col-md-6 padding-lg-80t padding-md-50t padding-sm-5t">
          <div>
            <div class="aheto-heading t-left md-t-center aheto-heading--t-white">
              <h1 class="aheto-heading__title t-light " style=" color:DodgerBlue; font-family: Timesnweromon ">{{$homePage['section1_heading']}}</h1>
            </div>
          </div>
          <div class="margin-lg-20t">
            <div class="aheto-heading t-left md-t-center aheto-heading--t-white">
              <h5 class="aheto-heading__title   f-18 t-light " style=" color:DodgerBlue; font-size: 25px; font-family: Timesnweromon;">{{$homePage['section1_quote']}}</h5>
            </div>
          </div>
          <!-- <div class="margin-lg-55t margin-md-40t">
            <div class="aheto-btn-container  t-left md-t-center">
              <a href="#mega-dropdown" class="aheto-btn   aheto-btn--alter  aheto-btn--shadow  ">LET’S GET STARTED<i class="ion-arrow-right-c aheto-btn__icon--right"></i></a>
            </div>
          </div> -->
        </div>
        <!-- <div class="col-md-6 margin-md-50t margin-sm-0t">
          <img src="{{url('public/img/SAAS/banner_image.png')}}" class="aheto-banner__image aheto-banner__image--absolute" alt="Inbox">
        </div> -->
        
      </div>
    </div>
  </section>
  <?php }?>
  <!-- <div style="background-color: #f7f7f7;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="padding-lg-25t padding-lg-20b">
          <div class="aheto-clients aheto-clients--5-in-row aheto-clients--low-opacity">
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client1.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client2.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client3.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client4.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client6.png')}}" alt="Clients logo image">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->
  <?php
          foreach($home_page_data as $homePage){
          // dd($homePage);
            ?>
  <div class="container mx-widt margin-lg-85t margin-lg-40b margin-md-70t margin-sm-50t margin-sm-20b">
    <div class="row row--flex row--v-center ">
      <div class="col-sm-12 col-md-6 col-xs-12 sm-t-center margin-xs-20b">
        <img src="{{$homePage['section2_image']}}" alt="" class="w-100">
      </div>
      
      <div class="col-lg-5 offset-lg-1 col-md-6 col-sm-12 col-xs-12">
        <div>
          <div class="aheto-heading t-left sm-t-center aheto-heading">
            <!-- <h6 class="aheto-heading__subtitle t-bold text-color--grey f-14 let-spasing">ABOUT SAAS</h6> -->
            <h2 class="aheto-heading__title          t-light ">{{$homePage['section2_tagline']}}</h2>
          </div>
        </div>
        <div class="margin-lg-25t">
          <div class="aheto-heading t-left sm-t-center aheto-heading">
            <p class="aheto-heading__title       l-height--163    ">{{$homePage['section2_description']}}</p>
          </div>
        </div>
        <!-- <div class="margin-lg-45t margin-lg-40t margin-lg-30b sm-t-center">
          <a href="https://www.youtube.com/watch?time_continue=2&amp;v=YMurlapfouo" class="aheto-video-link js-mfp-video ">
            <span class="aheto-video-link__btn ">
              <i class="ion-arrow-right-b"></i>
            </span>
            <span class="aheto-video-link__label">watch the video</span>
          </a>
        </div> -->
      </div>
      
    </div>
  </div>
  <?php }?>
  <!-- <hr class="d-lg-block"> -->
  
  <!-- <div class="container margin-lg-115t margin-lg-120b margin-md-75t margin-md-80b margin-sm-50t margin-sm-50b">
    <div class="row justify-content-center">
      <div class="col-md-9 col-lg-7">
        <div class="aheto-heading t-center aheto-heading">
          <h6 class="aheto-heading__subtitle t-bold text-color--grey f-14 let-spasing">FEATURES</h6>
          <h2 class="aheto-heading__title          t-light ">Bundles, because some messages just <span>belong together</span></h2>
        </div>
      </div>
      <img src="{{url('public/img/SAAS/banner_image-2.png')}}" alt="" class="aheto-banner__image mx-width img-100hw">
      <div class="row margin-lg-35t margin-sm-20t margin-lg-20b padding-sm-30b no-scroll w-100">
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-mobile">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Responsive</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">Your landing page displays smoothly on any device: desktop, tablet or mobile.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-settings">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Customizable</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">You can easily read, edit, and write your own code, or change everything.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-package">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">UI Elements</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">There is a bunch of useful and necessary elements for developing your website.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-bolt">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Clean Code</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">You can find our code well organized, commented and readable.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-layout-tab">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Documented</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">As you can see in the source code, we provided a comprehensive documentation.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 margin-lg-60b margin-md-30b margin-sm-10b">
          <div class="aheto-content-block aheto-content-block--feature t-left  ">
            <div class="aheto-content-block__descr transition-none">
              <div class="aheto-content-block__title-holder">
                <i class="aheto-content-block__ico icon ti-harddrive">
                </i>
                <h5 class="aheto-content-block__title margin-lg-40t margin-sm-25t margin-lg-20b ">Free Updates</h5>
              </div>
              <div class="aheto-content-block__info">
                <p class="aheto-content-block__info-text ">When you purchase this template, you'll freely receive future updates.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="aheto-btn-container  t-center md-t-center">
        <a href="#" class="aheto-btn   aheto-btn--alternative  aheto-btn--shadow  ">SEE MORE FEATURES</a>
      </div>
    </div>
  </div> -->
 
  <div class="about-counter-wrapp" style="background-color: #fff8f7;">
    <div class="container">
      <div class="row">
        <div class="col-md-8 offset-md-2 margin-lg-55b margin-md-35b">
           <?php
          foreach($home_page_data as $homePage){
          // dd($homePage);
            ?>
          <div class="aheto-heading t-center ">
            <h2 class="aheto-heading__title          t-light " style='font-family: "Times New Roman", Times, serif; font-size:      40px;'>{{$homePage['section3_heading']}}</h2>
          </div>
          <?php }?>
        </div>
      </div>

      <div class="row margin-lg-30t margin-sm-0t achievements">
        <?php
              //for($i=0;$i<count($reviews);$i++){ 
                for($i=0; $i<count($achievements); $i++){
                  

                ?>
        <div class="col-sm-3 margin-sm-30b">
          <div class="aheto-counter aheto-counter--classic ">
            <!-- <i class="aheto-counter__icon icon ti-cloud-down"></i> -->
            <h6 class="aheto-counter__number  js-counter  ">{{$achievements[$i]['section3_number']}}</h6>
            <p class="aheto-counter__desc">{{$achievements[$i]['section3_tagline']}}</p>
          </div>
        </div>
        <?php }?>

        <!-- <div class="col-sm-3 margin-sm-30b">
          <div class="aheto-counter aheto-counter--classic ">
            <i class="aheto-counter__icon icon ti-bookmark"></i>
            <h6 class="aheto-counter__number  js-counter  ">6500</h6>
            <p class="aheto-counter__desc">You can find our code well organized and readable.</p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="aheto-counter aheto-counter--classic ">
            <i class="aheto-counter__icon icon ti-cup"></i>
            <h6 class="aheto-counter__number  js-counter  ">200</h6>
            <p class="aheto-counter__desc">You can find our code well organized and readable.</p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="aheto-counter aheto-counter--classic ">
            <i class="aheto-counter__icon icon ti-cup"></i>
            <h6 class="aheto-counter__number  js-counter  ">200</h6>
            <p class="aheto-counter__desc">You can find our code well organized and readable.</p>
          </div>
        </div> -->
      </div>
    </div>
  </div>
  
  <!-- <section class="aheto-banner aheto-banner--height-600 d-flex" style="background-image: url('public/img/SAAS/banner_bg-2.png')">
    <div class="container d-flex">
      <div class="row w-100 align-items-center padding-sm-30t padding-sm-30b">
        <div class="col-lg-6 col-md-7 col-sm-12">
          <div class="aheto-heading t-left sm-t-center aheto-heading--t-white">
            <h6 class="aheto-heading__subtitle t-bold text-color--grey f-14 text-uppercase let-spasing">check out our video</h6>
            <h2 class="aheto-heading__title   f-60       t-light ">See the Highlights<br> at a glance</h2>
          </div>
        </div>
        <div class="col-lg-6 col-md-5 col-sm-12 t-right sm-t-center margin-sm-0t d-flex aheto-video-link__btn--pos">
          <a href="https://www.youtube.com/watch?time_continue=2&amp;v=YMurlapfouo" class="aheto-video-link js-mfp-video aheto-video-link--border">
            <span class="aheto-video-link__btn aheto-video-link__btn--circle aheto-video-link__btn--big">
              <i class="ion-arrow-right-b"></i>
            </span>
          </a>
        </div>
      </div>
    </div>
  </section> -->
  <?php
          foreach($home_page_data as $homePage){
          // dd($homePage);
            ?>
  <div class="container margin-lg-120t margin-md-80t margin-sm-50t">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <div class="aheto-heading t-center aheto-heading">
          <h6 class="aheto-heading__subtitle t-bold text-color--grey f-14 text-uppercase let-spasing">{{$homePage['section4_heading']}}</h6>
          <!-- <h2 class="aheto-heading__title          t-light "><spna>{{$homePage['section4_tagline']}}</span></h2> -->

            <h2 class="aheto-heading__title          t-light "><span>{{$homePage['section4_tagline']}}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <?php }?>
  
                
  <div class="container-fluid margin-lg-70t margin-md-10t margin-lg-110b margin-md-70b margin-sm-45b padding-lg-90t no-scroll">
    <div class="row row--flex row--h-center">
      <div class="col-12">
        <div class="aheto-image-slider aheto-image-slider--mobile-view">
          <div class="swiper  ">
            <div class="swiper-container " data-speed="500" data-centeredSlides="true" data-spaceBetween="30" data-slidesPerView="responsive" data-add-slides="5" data-lg-slides="5" data-md-slides="4" data-sm-slides="4" data-xs-slides="4"
              data-initialSlide="2">
              <div class="swiper-wrapper ">
                <?php
              //for($i=0;$i<count($reviews);$i++){ 
                for($i=0; $i<count($section4_image); $i++){
                  

                ?>
                <div class=" swiper-slide ">
                  <img  src="{{$section4_image[$i]['image']}}" alt="{{$section4_image[$i]['image_name']}}">
                </div>
                <?php }?>
                <!-- <div class="swiper-slide ">
                  <img src="{{url('public/img/SAAS/slide_3.png')}}" alt="">
                </div>
                <div class="swiper-slide ">
                  <img src="{{url('public/img/SAAS/slide_1.png')}}" alt="">
                </div>
                <div class="swiper-slide ">
                  <img src="{{url('public/img/SAAS/slide_4.png')}}" alt="">
                </div>
                <div class="swiper-slide ">
                  <img src="{{url('public/img/SAAS/slide_5.png')}}" alt="">
                </div> -->
              </div>
              <div class="swiper-pagination demo"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  <!-- <section style="background: URL::to('public/img/saas/pricing_bg.png') no-repeat center top;">
    <div class="container">
      <div class="row">
        <div class="col-md-12 margin-lg-120t margin-lg-70b margin-md-80t margin-md-50b margin-sm-50t margin-sm-30b">
          <div class="aheto-heading t-center aheto-heading--t-white">
            <h6 class="aheto-heading__subtitle t-bold f-14 text-uppercase let-spasing">pricing table</h6>
            <h2 class="aheto-heading__title          t-light ">Find the plan that’s right for you</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row row--flex row--h-center">
        <div class="aheto-pricing-wrapper aheto-pricing-wrapper--home-saas">
          <div class="aheto-pricing aheto-pricing--home-saas ">
            <div class="aheto-pricing__content">
              <div class="aheto-pricing__header">
                <h6 class="aheto-pricing__title ">Individual</h6>
              </div>
              <h1 class="aheto-pricing__cost-value">Free</h1>
              <ul class="aheto-pricing__details t-left ">
                <li class="aheto-pricing__detail aheto-pricing__detail--not-labeled">
                  <p><span class="green  padding-10r"><i class="fa fa-check" aria-hidden="true"></i></span>1 Social Profile per Platform</p>
                </li>
                <li class="aheto-pricing__detail aheto-pricing__detail--not-labeled">
                  <p><span class="green  padding-10r"><i class="fa fa-check" aria-hidden="true"></i></span>10 Scheduled Posts per Profile</p>
                </li>
                <li class="aheto-pricing__detail aheto-pricing__detail--not-labeled">
                  <p><span class="green  padding-10r"><i class="fa fa-check" aria-hidden="true"></i></span>10 Scheduled Posts per Profile</p>
                </li>
              </ul>
            </div>
            <a href="#" class="aheto-btn aheto-pricing__btn aheto-btn--shadow ">GET STARTED</a>
          </div>
          <div class="aheto-pricing aheto-pricing--home-saas ">
            <div class="aheto-pricing__content">
              <div class="aheto-pricing__header">
                <h6 class="aheto-pricing__title ">awesome</h6>
              </div>
              <h1 class="aheto-pricing__cost-value">$30.00</h1>
              <ul class="aheto-pricing__details t-left ">
                <li class="aheto-pricing__detail aheto-pricing__detail--not-labeled">
                  <p><span class="green  padding-10r"><i class="fa fa-check" aria-hidden="true"></i></span>1 Social Profile per Platform</p>
                </li>
                <li class="aheto-pricing__detail aheto-pricing__detail--not-labeled">
                  <p><span class="green  padding-10r"><i class="fa fa-check" aria-hidden="true"></i></span>10 Scheduled Posts per Profile</p>
                </li>
                <li class="aheto-pricing__detail aheto-pricing__detail--not-labeled">
                  <p><span class="green  padding-10r"><i class="fa fa-check" aria-hidden="true"></i></span>10 Scheduled Posts per Profile</p>
                </li>
              </ul>
            </div>
            <a href="#" class="aheto-btn aheto-pricing__btn aheto-btn--shadow ">GET STARTED</a>
          </div>
          <div class="aheto-pricing aheto-pricing--home-saas ">
            <div class="aheto-pricing__content">
              <div class="aheto-pricing__header">
                <h6 class="aheto-pricing__title ">business</h6>
              </div>
              <h1 class="aheto-pricing__cost-value">$90.00</h1>
              <ul class="aheto-pricing__details t-left ">
                <li class="aheto-pricing__detail aheto-pricing__detail--not-labeled">
                  <p><span class="green  padding-10r"><i class="fa fa-check" aria-hidden="true"></i></span>1 Social Profile per Platform</p>
                </li>
                <li class="aheto-pricing__detail aheto-pricing__detail--not-labeled">
                  <p><span class="green  padding-10r"><i class="fa fa-check" aria-hidden="true"></i></span>10 Scheduled Posts per Profile</p>
                </li>
                <li class="aheto-pricing__detail aheto-pricing__detail--not-labeled">
                  <p><span class="green  padding-10r"><i class="fa fa-check" aria-hidden="true"></i></span>10 Scheduled Posts per Profile</p>
                </li>
              </ul>
            </div>
            <a href="#" class="aheto-btn aheto-pricing__btn aheto-btn--shadow ">GET STARTED</a>
          </div>
        </div>
        <div class="col-sm-12 margin-lg-60t margin-md-40t">
          <div class="aheto-heading t-center aheto-heading ">
            <h6 class="aheto-heading__title       l-height--163 text-color--grey    ">Want to see the full details for each plan? Check out our <span>plans and pricing.</span></h6>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <section id="target">
    <div class="container margin-lg-20t margin-lg-110b margin-md-45t margin-md-70b margin-sm-15t margin-sm-45b">
      <div class="row">
        <div class="swiper swiper--saas ">
          <div class="swiper-container " data-speed="500" data-centeredSlides="true" data-spaceBetween="30" data-slidesPerView="responsive" data-add-slides="1" data-lg-slides="1" data-md-slides="1" data-sm-slides="1" data-xs-slides="1">

            <div class="swiper-wrapper ">
              <?php
              //for($i=0;$i<count($reviews);$i++){ 
                foreach($reviews as $review){
                  //dd($reviews);
                ?>
              <div class="swiper-slide ">
                <div class="aheto-tm-slider text-center">
                  <h6 class="aheto-tm-slider__label t-bold text-color--grey f-14 text-uppercase let-spasing testemonial-quote padding-lg-55t padding-lg-40b">testimonials</h6>
                  <h3 class="aheto-tm-slider__text t-light">“{{ $review['client_review'] }}”</h3>
                  <img src="{{ $review['client_image'] }}" alt="" class="aheto-tm-slider__img margin-lg-45t">
                  <h5 class="aheto-tm-slider__name t-medium margin-lg-8t">{{ $review['client_name'] }}</h5>
                  <h5 class="aheto-tm-slider__desigation t-medium margin-lg-5t"><i><strong>{{ $review['client_designation'] }}</strong></i></h5>
                  <!-- <h5 class="aheto-tm-slider__company_name t-medium margin-lg-0t"><strong><i>{{ $review['client_company'] }}</i></strong></h5> -->
                </div>
              </div>
             <?php }?>

              <!-- <div class="swiper-slide ">
                <div class="aheto-tm-slider text-center">
                  <h6 class="aheto-tm-slider__label t-bold text-color--grey f-14 text-uppercase let-spasing testemonial-quote padding-lg-55t padding-lg-40b">testimonials</h6>
                  <h3 class="aheto-tm-slider__text t-light">“When you innovate, you make mistakes.It is best to admit them quickly, &amp; get on with improving your other innovations.”</h3>
                  <img src="{{url('public/img/SAAS/testimonials_1.png')}}" alt="" class="aheto-tm-slider__img margin-lg-45t">
                  <h6 class="aheto-tm-slider__name t-medium margin-lg-25t">Sandi Hormez</h6>
                </div>
              </div>
              <div class="swiper-slide ">
                <div class="aheto-tm-slider text-center">
                  <h6 class="aheto-tm-slider__label t-bold text-color--grey f-14 text-uppercase let-spasing testemonial-quote padding-lg-55t padding-lg-40b">testimonials</h6>
                  <h3 class="aheto-tm-slider__text t-light">“When you innovate, you make mistakes.It is best to admit them quickly, &amp; get on with improving your other innovations.”</h3>
                  <img src="{{url('public/img/SAAS/testimonials_1.png')}}" alt="" class="aheto-tm-slider__img margin-lg-45t">
                  <h6 class="aheto-tm-slider__name t-medium margin-lg-25t">Sandi Hormez</h6>
                </div>
              </div> -->
            </div>
          </div>
          <div class="swiper-button-prev">
          </div>
          <div class="swiper-button-next">
          </div>
        </div>
      </div>
    </div>
  </section>

  
  <div style="background-color: #f7f7f7;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="padding-lg-25t padding-lg-20b">
          <div class="aheto-clients aheto-clients--5-in-row aheto-clients--low-opacity">
            <?php
              foreach($clients_info as $client){
            // dd($client);
                
            ?>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{$client['logo']}}" alt="Clients logo image">
              </a>
            </div>
            <?php }?>

            <!-- <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client2.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client3.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client4.png')}}" alt="Clients logo image">
              </a>
            </div>
            <div class="aheto-clients__holder">
              <a href="#" class="aheto-clients__link">
                <img class="aheto-clients__img" src="{{url('public/img/home/client6.png')}}" alt="Clients logo image">
              </a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <section style="background-color: #f3f9ff;" class="padding-lg-40b padding-sm-10b">
    <div class="container">
      <div class="row padding-lg-125t padding-md-75t padding-sm-50t margin-lg-60b margin-md-0b">
        <div class="col-md-12 margin-lg-70b margin-md-50b margin-sm-30b">
          <div class="aheto-heading t-center aheto-heading">
            <h6 class="aheto-heading__subtitle t-bold text-color--grey f-14 text-uppercase let-spasing">team members</h6>
            <h2 class="aheto-heading__title          t-light ">Get to know <span>amazing people</span></h2>
          </div>
        </div>
        <div class="col-sm-12 margin-md-30b">
          <div class="swiper swiper--saas ">
            <div class="swiper-container " data-speed="500" data-spaceBetween="30" data-slidesPerView="responsive" data-add-slides="4" data-lg-slides="4" data-md-slides="4" data-sm-slides="3" data-xs-slides="1">
              <div class="swiper-wrapper ">
                <?php 
                // dd($data);
                //for($i=0;$i<count($teamMembers);$i++)

                foreach($teamMembers as $key=>$value){
              ?>
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" style="border-radius: 50%; border: 1px solid dodgerblue" src="{{ $value['image'] }}" alt="Team member">
                      <div class="aheto-member__contact">
                        <?php
                        //dd($value['social_accounts']);
                        for($i=0;$i<count($value['social_accounts']);$i++){
                          ?>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="{{$value['social_accounts'][$i]}}" target="_blank">
                          <?php
                        for($i=0;$i<count($value['icons_name']);$i++){
                          ?>
                          <i class="aheto-member__icon icon ion-social-{{$value['icons_name'][$i]}}">&nbsp;&nbsp;&nbsp;</i>
                          <?php }?>
                        </a>
                        <?php }?>
                        <!-- <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a> -->
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">{{ $value['name'] }}</h5>
                      <p class="aheto-member__position ">{{ $value['designation'] }}</p>
                      <p class="aheto-member__position ">{{ $value['description'] }}</p>
                      <!-- <p></p> -->
                    </div>
                  </div>
                </div>
              <?php }?>
                <!-- <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_2.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">John Senating</h5>
                      <p class="aheto-member__position ">Director</p>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_3.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">Sandi Hormez</h5>
                      <p class="aheto-member__position ">Director</p>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_4.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">Morgan Guadis</h5>
                      <p class="aheto-member__position ">Designer</p>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_1.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">Morgan Guadis</h5>
                      <p class="aheto-member__position ">Designer</p>
                    </div>
                  </div>
                </div> -->
                <!-- <div class="swiper-slide ">
                  <div class="aheto-member aheto-member--saas  t-center">
                    <div class="aheto-member__img-holder">
                      <img class="aheto-member__img" src="{{url('public/img/SAAS/member_2.png')}}" alt="Team member">
                      <div class="aheto-member__contact">
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-facebook"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-twitter"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-skype"></i>
                        </a>
                        <a class="aheto-member__link aheto-btn--white aheto-btn--trans t-center" href="#">
                          <i class="aheto-member__icon icon ion-social-youtube"></i>
                        </a>
                      </div>
                    </div>
                    <div class="aheto-member__text">
                      <h5 class="aheto-member__name">John Senating</h5>
                      <p class="aheto-member__position ">Designer</p>
                    </div>
                  </div>
                </div> -->
              </div>
            </div>
            <div class="swiper-button-prev">
            </div>
            <div class="swiper-button-next">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- <section class="aheto-banner aheto-banner--height-500 d-flex aheto-banner--promo" style="background-image: url('public/img/SAAS/banner_bg-3.png')">
    <div class="container margin-lg-35t padding-lg-60b d-flex">
      <div class="row w-100 align-items-center">
        <div class="col-md-5 col-xs-12 md-t-center">
          <div class="aheto-heading t-left md-t-center aheto-heading--t-white">
            <h2 class="aheto-heading__title          t-light ">Download the Inbox app to get started.</h2>
          </div>
          <div class="aheto-banner__rating margin-lg-50t margin-sm-15t">
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
            <i class="ion-ios-star"></i>
          </div>
          <div class="aheto-heading t-left md-t-center aheto-heading--t-white">
            <p class="aheto-heading__title          t-light ">Base on <b>2,000+ reviews</b></p>
          </div>
        </div>
        <div class="col-md-6 offset-md-1 col-xs-12 t-right md-t-center margin-lg-35t margin-sm-15t">
          <div class="aheto-banner__store margin-lg-25b">
            <a href="https://www.apple.com/lae/ios/app-store/" target="_blank"><img alt="" src="{{url('public/img/SAAS/app-store.png')}}"></a>
            <a href="https://play.google.com/" target="_blank"><img src="{{url('public/img/SAAS/google-play.png')}}" alt=""></a>
          </div>
          <div class="aheto-heading t-right md-t-center aheto-heading--t-white">
            <p class="aheto-heading__title          t-light ">Also available on the web at <b>inbox.google.com.</b></p>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  

  <!-- Magnific popup -->
  <script src="{{url('public/vendors/magnific/jquery.magnific-popup.min.js')}}"></script>
  <!-- anm -->
  <script src="{{url('public/vendors/animation/anm.min.js')}}"></script>
  <!-- Google maps -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARwCmK-LlGIH8Mv1ac4VyceMYUgg9vStM&amp;#038;&language=en"></script>
  <script src="{{url('public/vendors/googlemap/google-maps.js?v=1')}}"></script>
  <!-- FullCalendar -->
  <!-- Parallax -->
  <script src="{{url('public/vendors/parallax/parallax.min.js')}}"></script>
  <!-- asRange -->
  <script src="{{url('public/vendors/range/jquery.range-min.js')}}"></script>
  <!-- lightgallery -->
  <script src="{{url('public/vendors/lightgallery/lightgallery.min.js')}}"></script>
  <!-- Main script -->
  <script src="{{url('public/vendors/script.js?v=1')}}"></script>
  <script src="{{url('public/vendors/spectragram/spectragram.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      jQuery.fn.spectragram.accessData = {
        accessToken: '4058508404.1677ed0.f87c0182df0d4512a9e01def0c53adb7'
      }

      $('.instafeed').spectragram('getUserFeed', {
        size: 'big',
        max: 6
      });
    });
  </script>

  <!-- <script>
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script> -->



  <!-- <script>
    var achievements = [];
        

        $.ajax({
            url: '{{env("LARAVEL_API_BASE_PATH")}}' + '/',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
              
              response.map(res => {
                
                achievements.push('<div class="col-sm-3 margin-sm-30b"><div class="aheto-counter aheto-counter--classic "><h6 class="aheto-counter__number  js-counter  ">'+ res.section3_number +'</h6><p class="aheto-counter__desc">'+ res.section3_tagline +'</p></div></div>');
                })
                $('.achievements').html(achievements);
            },
            error : function(request, status, err){
                console.log(err)
            }
        });
  </script> -->


  <!-- <script type="text/javascript">
    function updateTime(){
    var currentTime = new Date()
    var date = currentTime.getDate()
    var month = currentTime.getMonth()
    var year = currentTime.getFullYear()
    var hours = currentTime.getHours()
    var minutes = currentTime.getMinutes()
    var seconds = currentTime.getSeconds()
    
    if (minutes < 10){
        minutes = "0" + minutes
    }
    if (seconds < 10){
        seconds = "0" + seconds
    }
    months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
    days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    var t_str = date + " " + months[month]+ " " + year + " " + hours + ":" + minutes + ":" + seconds + " ";
    if(hours > 11){
        t_str += "PM";
    } else {
        t_str += "AM";
    }
    document.getElementById('date-and-time').innerHTML = t_str;
    }
    setInterval(updateTime, 1000);
  </script> -->
@stop