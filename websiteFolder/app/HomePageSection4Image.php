<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class HomePageSection4Image extends Model
{
    protected $table = 'home_page_section4_images';
    protected $perPage = 30;
}
