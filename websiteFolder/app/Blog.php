<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BlogCategory;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $perTable = 30;
    protected $fillable = ['blog_category_id','blog_title','blogger_name','blog_image','blog_description'];

    public function blogCategoryId(){
     	return $this->belongsTo(BlogCategory::class);
     }
}
