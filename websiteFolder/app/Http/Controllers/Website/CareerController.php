<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Career;
use Session;
use App\JobResume;

class CareerController extends Controller
{
    public function index(){
    	$returnArray = array();
    	$vacancies = Career::select('id','job_title','job_experience','job_url')->orderBy('id', 'asc')->where('status','=','1')->get()->toArray();
    	$count = count($vacancies);
    	// dd($count);
    	// dd($vacancies);
   //  	foreach ($vacancies as $value) {
   //  		$arr = array();
   //  		$arr['id'] = $value['id'];
   //  		$arr['job_title'] = $value['job_title'];
			// $arr['id'] = $value['id'];    		
   //  	}
        // session::flush('succes', 'your resume submitted successfully');
    	return view('frontend.career',['vacancy' => $vacancies, 'count' => $count]);
    }


    public function vacancyDetail(Request $request, $type){

    	$returnArray = array();
    	// dd($type);
    	$job_data = Career::select('job_title','job_experience','description','skills','responsibilities')->where('id','=', $type)->first();
    	// dd($job_data);
    	$returnArray['job_title'] = $job_data->job_title;
        $returnArray['job_experience'] = $job_data->job_experience;
    	$returnArray['description'] = $job_data->description;
    	$returnArray['skills'] = explode('.',$job_data->skills);
    	$returnArray['responsibilities'] = explode('.',$job_data->responsibilities);
        // dd($returnArray);
    	return view('frontend.vacancy-description')->with('details', $returnArray);
    	// dd($returnArray);
    }

    public function submitResume(Request $request){

        
        // $resume = $request->file('resume')
        // dd($canddate_email);
        // $request->validate([
        //     'file' => 'required|mimes:pdf,xlx,csv|max:2048',
        // ]);
        // $resume = $request->file('resume');
        // // dd($resume);
        // $fileName = time().'.'.$request->file('resume')->extension();
        // // dd($fileName);


        $email = strtolower($request->email);
        $data = JobResume::where('candidate_email','=',$email)->first();
        if(!$data){
            $job_resume = new JobResume();
            $job_resume->candidate_email = $email;
            $resume=time().".".$request->file('resume')->extension();
            $path = public_path('resumes');
            $request->file('resume')->move($path,$resume);
            $job_resume->candidate_resume=$resume;
            $job_resume->save();
            if($job_resume->save()){
                Session::flush('succes', 'your resume submitted successfully');
                // return view('frontend.career');
                return back();
            }
        }
        else{
            // $previous_resume_path = public_path('resume')."/".$data->candidate_resume;
            // dd($previous_resume_path);
            // unlink($previous_resume_path);
            // $resume=time().".".$request->file('resume')->extension();
            // $path = public_path('resumes');
            // $request->file('resume')->move($path,$resume);
            // $data->candidate_resume=$resume;
            // $data->save();
        }

        // $data = JobResume::select('id','candidate_email','candidate_resume')->get();
        // // dd($data);
        // $array = array();
        // foreach ($data as $value) {
        //     $arr = array();
        //     $arr['id'] = $value['id'];
        //     $arr['candidate_email'] = $value['candidate_email'];
        //     $arr['candidate_resume'] = asset('public/resumes/'.$value['candidate_resume']);
        //     array_push($array, $arr);
        // }
        // dd($array);
    }
}
