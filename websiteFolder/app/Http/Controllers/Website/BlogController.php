<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlogCategory;
use App\Blog;

class BlogController extends Controller
{
    public function index(Request $request){
    	if($request->isMethod('GET')){
    		// $value = 'deepak';
    		// dd($value);
    		return view('frontend.blog');
    	}
    }

    public function blogCategories(){

    	$categories = BlogCategory::where('status','=','1')->orderBy('id','asc')->get()->toArray();
    	// dd($cat)
    	return $categories;
    }

    public function allBlogs(Request $request){
    	$returnArray = array();
    	$all_blogs = Blog::where('status','=','1')->get()->toArray();
    	foreach($all_blogs as $value){
    		$arr = array();
    		$arr['id'] = $value['id'];
    		$arr['blog_category_id'] = $value['blog_category_id'];
    		$arr['blog_title'] = $value['blog_title'];
    		$arr['blog_image'] = asset('public/storage/'.$value['blog_image']);
    		$arr['blog_description'] = $value['blog_description'];
    		$arr['blogger_name'] = $value['blogger_name'];
    		$date_time = explode(" ",$value['created_at']);
    		$arr['created_at'] = $date_time[0];
    		array_push($returnArray, $arr);
    	}
    	// dd($returnArray);
    	return $returnArray;
    }

    public function getBlogCategoryData(Request $request, $categoryId){

    	$returnArray = array();

    	$category_data = Blog::where('blog_category_id','=',$categoryId)->get()->toArray();
    	// $category_data = Blog::select('blogs.id as id','blog_category.category_name as category_name','blogs.blog_title','blogs.blog_image','blogs.blog_description','blogs.blogger_name','blogs.created_at')->join('blog_category','blog_category.id','=','blogs.blog_category_id')->where('blog_category_id','=', $categoryId)->get()->toArray();
    	//dd($category_data);
    	foreach ($category_data as $value) {
    		$arr = array();
    		$arr['id'] = $value['id'];
    		$arr['blog_category_id'] = $value['blog_category_id'];
    		$arr['blog_title'] = $value['blog_title'];
    		$arr['blog_image'] = asset('public/storage/'.$value['blog_image']);
    		$arr['blog_description'] = $value['blog_description'];
    		$arr['blogger_name'] = $value['blogger_name'];
    		$date_time = explode(" ",$value['created_at']);
    		$arr['created_at'] = $date_time[0];
    		array_push($returnArray, $arr);
    	}
    	return view('frontend.blog',['blog_category_data' => $returnArray]);
    }
}
