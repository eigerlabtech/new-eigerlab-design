<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TechnologyType extends Model
{
    protected $table = 'technology_type';
    protected $perPage = 30;
}
