<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class JobResume extends Model
{
    protected $table = 'job_resumes';
    protected $perTable = 30;
}
