<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Query;
use App\TeamMember;
use App\Testimonial;
use App\ServiceType;
use App\Client;
use App\HomePage;
use App\HomePageSection4Image;
use App\HomePageSection3;

class webController extends Controller
{
	public function submitQuery(Request $request){

		if($request->isMethod('GET')){

			return view('frontend.contact');
		}

		else{
			// dd($request->_token);
			$name = ucwords($request->name);
			$email = strtolower($request->email);
			$mobile = $request->mobile;
			$message = $request->message;

			$queryData = new Query();
			$queryData->name = $name;
			$queryData->email = $email;
			$queryData->mobile = $mobile;
			$queryData->message = $message;
			$queryData->save();
			if($queryData->save()){
				// return back()->with('success','Your query submitted successfully');
				//$request->session()->flash('success', 'Your query submitted successfully');
				//return redirect('/')->with('error', 'Error!');
				// \Session::flesh('message', 'query submitted successfully');
				return back();
			}
			else{
				// \Session::flesh('message', 'Something went wrong! Try again later');
				return back();
				//return back()->with('error', 'Something went wrong!');
				//$request->session()->flash('warning', 'Something went wrong!');
			}

		}
	}

	public function index(Request $request){

		// $returnArray = array();
		$teamMembers = TeamMember::select('name','designation','image','description','social_accounts')->where('status','=','1')->get()->toArray();
		// dd($teamMembers);
		//$count = count($teamMembers);
		// dd($count);
		if(!empty($teamMembers)){
			$memberArray = array();
			foreach($teamMembers as $team){
				$arr = array();
				$arr['name'] = $team['name'];
				$arr['designation'] = $team['designation'];
				$arr['image'] = asset('public/storage/'.$team['image']);
				$arr['description'] = (!empty($team['description'])) ? $team['description'] : "";
				$arr['social_accounts'] = explode(',', $team['social_accounts']);
				$arr['icons_name'] = array();
				foreach($arr['social_accounts'] as $key => $icon){
					$social_icons = array();
					$social_icons = explode('.',$icon);
					array_push($arr['icons_name'], $social_icons[1]);
					//$arr['social_accounts_icons'] = $social_icons['icons'][1];
					// print_r($arr['icon']);
					// die();
				}
				//dd($icons);
					// print_r($arr);
					// die();
					array_push($memberArray, $arr);
			}
			//dd($memberArray);
			//return response()->json($returnArray);
			// print_r($returnArray[0]['icons_name']);
			// die();
			$review_data = webController::clientReviews();
			$servicesData = webController::serviceTypes();
			$client_data = webController::getClients();
			$homePageData = webController::homePageData();
			$section3_achievements = webController::getAchievements();
			// dd($section3_achievements);
			$section4_images = webController::getSection4Images();
			

			//dd($client_data);
			// return view('frontend.home')->with('teamMembers',$memberArray)->with('reviews', $review_data)->with('services', $servicesData)->with('clients_info', $client_data);
			return view('frontend.home',['teamMembers' => $memberArray,'reviews' => $review_data, 'services' => $servicesData, 'clients_info' => $client_data, 'home_page_data' => $homePageData, 'achievements' => $section3_achievements,'section4_image' => $section4_images]);
		}
		
		// $arr = array();
		// foreach ($teamMembers as $value) {
		// 	$arr1 = array();
		// 	$arr1['name'] = $value['name'];
		// 	$arr1['designation'] = $value['designation'];
		// 	$arr1['image'] = asset('public/storage/'.$value['image']);
		// 	$arr1['social_accounts'] = explode(',',$value['social_accounts']);
		// }
		else{
			return response()->json(['mess'=>'Something went wrong','data'=>'']);
		}
	}

	private function clientReviews(){

		$client_reviews = Testimonial::select('client_name','client_designation','client_company','client_image','client_review')->where('status','=','1')->get();
		$arr = array();
		foreach($client_reviews as $data){
			$arr1 = array();
			$arr1['client_name'] = $data['client_name'];
			$arr1['client_designation'] = $data['client_designation'];
			$arr1['client_company'] = $data['client_company'];
			$arr1['client_image'] = asset('public/storage/'.$data['client_image']);
			$arr1['client_review'] = $data['client_review'];
			array_push($arr, $arr1);
		}
		return $arr;
		// if(!empty($client_review)){
		// 	$review_array = array();
		// 	foreach($client_reviews as $review)
		// }
	}

	private function serviceTypes(){
		$returnArray = array();
		$servicesData = ServiceType::select('service_title as title', 'service_icon as icon')->get()->toArray();
		foreach($servicesData as $service){
			$arr = array();
			$arr['title'] = $service['title'];
			$arr['icon'] = asset('public/storage/'.$service['icon']);
			array_push($returnArray, $arr);
		}
		// dd($returnArray);
		return $returnArray;
	}

	private function getClients(){

		$returnArray = array();
		$client_data = Client::select('id','client_name as name', 'client_logo as logo')->where('status','=','1')->orderBy('id','asc')->get()->toArray();
		foreach($client_data as $client){
			$arr = array();
			$arr['id'] = $client['id'];
			$arr['name'] = $client['name'];
			$arr['logo'] = asset('public/storage/'.$client['logo']);
			array_push($returnArray, $arr);
		}
		// dd($returnArray);
		return $returnArray;
	}

	private function homePageData(){

		$returnArray = array();
		$data = HomePage::select('id','section1_banner','section1_heading','section1_quote','section2_image','section2_tagline','section2_description','section3_heading','section4_heading','section4_tagline')->orderBy('id','asc')->get()->toArray();
		// dd($data);
		foreach($data as $value){
			$arr = array();
			$arr['id'] = $value['id'];
			$arr['section1_banner'] = asset('public/storage/'.$value['section1_banner']);
			$arr['section1_heading'] = $value['section1_heading'];
			$arr['section1_quote'] = $value['section1_quote'];
			$arr['section2_image'] = asset('public/storage/'.$value['section2_image']);
			$arr['section2_tagline'] = $value['section2_tagline'];
			$arr['section2_description'] = $value['section2_description'];
			$arr['section3_heading'] = $value['section3_heading'];
			$arr['section4_heading'] = $value['section4_heading'];
			$arr['section4_tagline'] = $value['section4_tagline'];
			// $arr['id'] = $value['id'];
			// $arr['name'] = $value['name'];
			// $arr['logo'] = asset('public/storage/'.$value['logo']);
			array_push($returnArray, $arr);
		}
		// dd($returnArray);
		 return $returnArray;
	}

	private function getSection4Images(){

		$returnArray = array();
		$data = HomePageSection4Image::select('id','image','image_name')->where('status','=','1')->orderBy('id','asc')->get()->toArray();
		// dd($data);
		foreach($data as $value){
			$arr = array();
			$arr['id'] = $value['id'];
			$arr['image'] = asset('public/storage/'.$value['image']);
			$arr['image_name'] = $value['image_name'];
			array_push($returnArray, $arr);
		}
		// dd($returnArray);
		 return $returnArray;
	}

	public function aboutPageClinetData(){
		
		$clients_data = webController::getClients();
		$section3_achievements = webController::getAchievements();
		$section3_heading = HomePage::select('section3_heading')->get()->toArray();
		// dd($section3_heading);
		return view('frontend.about', ['clients_info' => $clients_data, 'achievements' => $section3_achievements, 'section3_heading' => $section3_heading]);
	}

	private function getAchievements(){

		$achievements = HomePageSection3::select('id','section3_number','section3_tagline')->where('status','=','1')->orderBy('id','asc')->get()->toArray();
		// dd($achievements);
		return $achievements;
	}
}
