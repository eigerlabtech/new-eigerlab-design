<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlogCategory;
use App\Blog;

class BlogController extends Controller
{
    public function index(Request $request){
    	if($request->isMethod('GET')){
			$allCategory = self::blogCategories();
			$randomBlogs = self::RandomBlogs(0);
    		return view('frontend.blog')->with('category',$allCategory)->with('randomBlog',$randomBlogs);
    	}
    }

    public function blogCategories(){

    	$categories = BlogCategory::where('status','=','1')->orderBy('id','asc')->get()->toArray();
    	return $categories;
    }

	public function categoryById($id){
    	$categories = BlogCategory::find($id)->first()->toArray();
    	return $categories;
    }

    public function allBlogs(Request $request){
    	$returnArray = array();
    	$all_blogs = Blog::where('status','=','1')->get()->toArray();
    	foreach($all_blogs as $value){
    		$arr = array();
    		$arr['id'] = $value['id'];
    		$arr['blog_category_id'] = $value['blog_category_id'];
    		$arr['blog_title'] = $value['blog_title'];
    		$arr['blog_image'] = asset('public/storage/'.$value['blog_image']);
    		$arr['blog_description'] = $value['blog_description'];
    		$arr['blogger_name'] = $value['blogger_name'];
    		$date_time = explode(" ",$value['created_at']);
    		$arr['created_at'] = $date_time[0];
    		array_push($returnArray, $arr);
    	}
    	// dd($returnArray);
    	return $returnArray;
    }

    public function getBlogCategoryData(Request $request, $categoryId){

    	$returnArray = array();
        // dd($categoryId);
    	$category_data = Blog::where('blog_category_id','=',$categoryId)->get()->toArray();
    	// $category_data = Blog::select('blogs.id as id','blog_category.category_name as category_name','blogs.blog_title','blogs.blog_image','blogs.blog_description','blogs.blogger_name','blogs.created_at')->join('blog_category','blog_category.id','=','blogs.blog_category_id')->where('blog_category_id','=', $categoryId)->get()->toArray();
    	//dd($category_data);
    	foreach ($category_data as $value) {
    		$arr = array();
    		$arr['id'] = $value['id'];
    		$arr['blog_category_id'] = $value['blog_category_id'];
    		$arr['blog_title'] = $value['blog_title'];
    		$arr['blog_image'] = asset('public/storage/'.$value['blog_image']);
    		$arr['blog_description'] = $value['blog_description'];
    		$arr['blogger_name'] = $value['blogger_name'];
    		$date_time = explode(" ",$value['created_at']);
    		$arr['created_at'] = $date_time[0];
    		array_push($returnArray, $arr);
    	}
    	return $returnArray;
     	//dd($returnArray);
    // 	return view('frontend.blog',['blog_category_data' => $returnArray]);
    }
   

	public function RandomBlogs($page){
		$returnArray = array();
        $next = $page * 5;
    	$all_blogs = Blog::where('status','=','1')->offset($next)->limit(5)->get()->toArray();
		 
		foreach($all_blogs as $blog){
			$arr = array();
			$blog['id'] = base64_encode(strval($blog['id']));
			$blog['blog_image'] = asset('public/storage/'.$blog['blog_image']);
			$blog['category_name'] = self::categoryById($blog['blog_category_id'])['category_name'];
            $date_time = explode(" ",$blog['created_at']);
            $blog['created_at'] = $date_time[0];
			array_push($returnArray, $blog);
		}
		
		return $returnArray;
	}

	public function DetailBlog($id){
		$returnArray = array();
       $blogId = explode('_',$id);
	   $blogId = $blogId[1];
	   $blogId = base64_decode($blogId);
	   $blog = Blog::find($blogId)->toArray();
		$blog['id'] = base64_encode(strval($blog['id']));
		$blog['blog_image'] = asset('public/storage/'.$blog['blog_image']);
		$blog['category_name'] = self::categoryById($blog['blog_category_id'])['category_name'];
        $date_time = explode(" ",$blog['created_at']);
        $blog['created_at'] = $date_time[0];
	   $allCategory = self::blogCategories();
	   $randomBlogs = self::RandomBlogs(0);
	   return view('frontend.detail_blog',['blog_data' => $blog,'category' => $allCategory,'randomBlog'=>$randomBlogs]);
	}
}
