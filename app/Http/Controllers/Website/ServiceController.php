<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ServiceType;
use App\TechnologyType;
use App\Client;

class ServiceController extends Controller
{	
	public function serviceTypes(){
		$returnArray = array();
		$servicesData = ServiceType::select('id','service_title as title', 'service_icon as icon', 'url')->orderBy('id','asc')->get()->toArray();
		foreach($servicesData as $service){
			$arr = array();
			$arr['id'] = $service['id'];
			$arr['title'] = $service['title'];
			$arr['icon'] = asset('public/storage/'.$service['icon']);
			$arr['url'] = $service['url'];
			array_push($returnArray, $arr);
		}
		 //dd($returnArray);
		 return $returnArray;
		// return view('frontend.home');
	}


	public function technologyTypes(){
		$returnArray = array();
		$technologyData = TechnologyType::select('id','technology_title as title', 'technology_icon as icon','url')->where('status','=','1')->orderBy('id','asc')->get()->toArray();
		foreach($technologyData as $technology){
			$arr = array();
			$arr['id'] = $technology['id'];
			$arr['title'] = $technology['title'];
			$arr['icon'] = asset('public/storage/'.$technology['icon']);
			$arr['url'] =$technology['url'];
			array_push($returnArray, $arr);
		}
		 // dd($returnArray);
		  return $returnArray;
		// return view('frontend.home');
	}

	public function getServiceData(Request $request, $type){

		$returnData = array();
		$url  = "/".$type;
		// dd($url);
		$service_data = ServiceType::where('url', $url)->get()->toArray();
		// dd($service_data);
		foreach ($service_data as $value) {
			$arr = array();
			$arr['id'] = $value['id'];
			$arr['service_title'] = $value['service_title'];
			$arr['service_image'] = asset('public/storage/'.$value['service_image']);
			$arr['service_description'] = $value['service_description'];
			$arr['service_icon'] = asset('public/storage/'.$value['service_icon']);
			$arr['service_banner'] = asset('public/storage/'.$value['service_banner']);
			$arr['url'] = $value['url'];

			$arr['section3_image'] = asset('public/storage/'.$value['section3_image']);
			$arr['section3_title'] = $value['section3_title'];
			$arr['section3_description'] = $value['section3_description'];
			
			$arr['section4_image'] = asset('public/storage/'.$value['section4_image']);
			$arr['section4_title'] = $value['section4_title'];
			$arr['section4_description'] = $value['section4_description'];
			$arr['section3_points'] = explode('.',$value['section3_points']);
			$arr['section4_points'] = explode('.',$value['section4_points']);
			array_push($returnData, $arr);
		}
		//dd($returnData);
		$client_data = serviceController::getClients();
		return view('frontend.services', ['service_data' => $returnData, 'clients_info' => $client_data]);
	}

	public function getTechnologyData(Request $request, $type){

		$returnData = array();
		$url  = "/".$type;
		// dd($url);
		$technology_data = TechnologyType::where('url', $url)->get()->toArray();
		// dd($service_data);
		foreach ($technology_data as $value) {
			$arr = array();
			$arr['id'] = $value['id'];
			$arr['technology_title'] = $value['technology_title'];
			$arr['technology_image'] = asset('public/storage/'.$value['technology_image']);
			$arr['technology_description'] = $value['technology_description'];
			$arr['technology_icon'] = asset('public/storage/'.$value['technology_icon']);
			$arr['technology_banner'] = asset('public/storage/'.$value['technology_banner']);
			$arr['url'] = $value['url'];

			$arr['section3_image'] = asset('public/storage/'.$value['section3_image']);
			$arr['section3_title'] = $value['section3_title'];
			$arr['section3_description'] = $value['section3_description'];
			
			$arr['section4_image'] = asset('public/storage/'.$value['section4_image']);
			$arr['section4_title'] = $value['section4_title'];
			$arr['section4_description'] = $value['section4_description'];
			$arr['section3_points'] = explode('.',$value['section3_points']);
			$arr['section4_points'] = explode('.',$value['section4_points']);
			array_push($returnData, $arr);
		}
		//dd($returnData);
		$client_data = ServiceController::getClients();
		return view('frontend.technologies',['technology_data' => $returnData, 'clients_info' => $client_data]);
	}

	private function getClients(){
		$returnArray = array();
		$client_data = Client::select('id','client_name as name', 'client_logo as logo')->where('status','=','1')->orderBy('id','asc')->get()->toArray();
		foreach($client_data as $client){
			$arr = array();
			$arr['id'] = $client['id'];
			$arr['name'] = $client['name'];
			$arr['logo'] = asset('public/storage/'.$client['logo']);
			array_push($returnArray, $arr);
		}
		return $returnArray;
	}
}
