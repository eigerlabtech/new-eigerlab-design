<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BlogCategory extends Model
{
    protected $table = 'blog_category';
    protected $perPage = 30;
    protected $fillable = ['category_name'];
}
