<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class HomePageSection3 extends Model
{
    protected $table = 'home_page_section3';
    protected $perPage = 30;
}
