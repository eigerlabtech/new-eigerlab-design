<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Website\webController@index');
Route::get('/services/{type}', 'Website\ServiceController@getServiceData');
Route::get('/technology/{type}', 'Website\ServiceController@getTechnologyData');

//.........................Blog Controller.......................//

Route::get('/blog-category/{categoryName}','Website\BlogController@getBlogCategoryData');







//..................................................................//

// Route::get('/','Website\ServiceController@serviceTypes');

// Route::get('/about', function () {
//     return view('frontend.about');
// });

Route::get('/about', 'Website\webController@aboutPageClinetData');

Route::get('/services', function () {
    return view('frontend.services');
});



Route::get('/features', function () {
    return view('frontend.features');
});
Route::get('/features_detals', function () {
    return view('frontend.features-details');
});
Route::get('/blog', 'Website\BlogController@index');
Route::get('/post', function () {
    return view('frontend.post');
});
Route::get('/pricing', function () {
    return view('frontend.pricing');
});
Route::get('/contact', function () {
    return view('frontend.contact');
});

Route::get('/portfolio', function () {
    return view('frontend.portfolio');
});

Route::get('/techugo-home', function () {
    return view('frontend.techugo_home');
});

Route::get('/career', 'Website\careerController@index');
Route::get('/career/{type}', 'Website\careerController@vacancyDetail');

Route::get('/team-members','Website\webController@teamMembersDetails');

Route::post('/submit-form', 'Website\CareerController@submitResume');

// Route::get('/imageChange', function () {
//     return view('frontend.imageChange_scroll');
// });

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
