<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Website\webController@index');

Route::get('/about', 'Website\webController@aboutPageData');

Route::get('/services', function () {
    return view('frontend.services2');
});

Route::get('/services/{type}', 'Website\ServiceController@getServiceData');
Route::get('/technology/{type}', 'Website\ServiceController@getTechnologyData');

// Route::get('/features', function () {
//     return view('frontend.features-saas');
// });
// Route::get('/features_detals', function () {
//     return view('frontend.features-details-saas');
// });


// Route::get('/blog', function () {
//     return view('frontend.blog');
// });

//Route::get('/blog', 'Website\BlogController@index');
Route::get('/blog', 'Website\BlogController@index');
Route::get('blog/{id}', 'Website\BlogController@DetailBlog');
// Route::get('blog-category/{blogCategoryId}', 'Website\BlogController@getBlogCategoryData');

Route::get('/post', function () {
    return view('frontend.post-saas');
});
Route::get('/pricing', function () {
    return view('frontend.pricing-saas');
});
Route::get('/contact', function () {
    return view('frontend.contact-saas');
});

Route::get('/portfolio', function () {
    return view('frontend.portfolio');
});

Route::get('/portfolio-casestudy', function () {
    return view('frontend.portfolio-casestudy');
});

// Route::get('/career', function () {
//     return view('frontend.career');
// });

Route::get('/career', 'Website\CareerController@index');
Route::get('/career/{type}', 'Website\CareerController@vacancyDetail');
Route::post('/submit-form', 'Website\webController@submitQuery');
Route::post('/submit-resume', 'Website\CareerController@submitResume');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
